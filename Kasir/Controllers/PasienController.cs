﻿using System;
using System.Collections.Generic;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Kasir.Controllers
{
    public class PasienController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<KASIR_GetListPasienSiapPulang_Result> proses = s.KASIR_GetListPasienSiapPulang();
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("TglReg >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("TglReg <= @0", DateTime.Parse(filter[13]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_Result.Status_Pulang)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_Result.SectionPerawatan)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_Result.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_Result.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_Result.JenisPasien)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_Result.SisaResep)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_Result.SisaPenunjang)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_Result.SisaRJ)}.Contains(@0)", filter[7]);
                    if (filter[15] != "ALL")
                    {
                        var tipepelayanan = (filter[15] == "1") ? true : false;
                        proses = proses.Where(x => x.RawatInap == tipepelayanan);
                    }
                    if (filter[16] != "ALL")
                    {
                        var JenisPasien = filter[16];
                        proses = proses.Where(x => x.JenisPasien.Contains(JenisPasien));
                    }
                    proses = proses.OrderBy("NoReg ASC");
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                    foreach (var x in m)
                    {
                        var nilai = s.Kasir_GetNilaiInvoice(x.NoReg);
                        if(nilai == null)
                        {
                            x.nilai = "0";
                        }
                        else
                        {
                            decimal nilaiInvoice = Convert.ToDecimal(nilai.FirstOrDefault());
                            x.nilai = IConverter.ToMoney(nilaiInvoice);
                        }
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }

}