﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kasir.Controllers
{
    [Authorize(Roles = "Kasir")]
    public class ReportController : Controller
    {

        private string ServerPath = "~/CrystalReports/Kwitansi";

        #region === REPORT KE PASIEN

        public ActionResult PrintInvoiceRI(string noinvoice, int tampilkanYangKosong)
        {
            try
            {

                string reportname = $"SIM_Rpt_Invoice_RI_NEEW";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports/DEV"), reportname + ".rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_RekapInvoicePasien_WEB", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"SIM_Rpt_RekapInvoicePasien_WEB;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand("Kasir_TipeBayar", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Kasir_TipeBayar;1"].SetDataSource(ds[i].Tables[0]);

                    //i++;
                    //cmd.Add(new SqlCommand($"select * from sReport", conn));
                    //cmd[i].CommandType = CommandType.Text;
                    //da.Add(new SqlDataAdapter(cmd[i]));
                    //ds.Add(new DataSet());
                    //da[i].Fill(ds[i], "DataTable1");
                    //rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #region ===== C E T A K  I N V O I C E
        public ActionResult CetakInvoiceRJ(string noinvoice, int tampilkanYangKosong)
        {
            try
            {

                string reportname = $"SIM_Rpt_Invoice_RJ";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports/Kwitansi"), reportname + ".rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_RekapInvoicePasien_WEB", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"SIM_Rpt_RekapInvoicePasien_WEB;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand("Kasir_TipeBayar", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Kasir_TipeBayar;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult CetakInvoiceRI(string noinvoice, int tampilkanYangKosong)
        {
            try
            {

                string reportname = $"SIM_Rpt_Invoice_RI";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports/Kwitansi"), reportname + ".rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_RekapInvoicePasien_WEB", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"SIM_Rpt_RekapInvoicePasien_WEB;1"].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand("Kasir_TipeBayar", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Kasir_TipeBayar;1"].SetDataSource(ds[i].Tables[0]);

                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region ===== C E T A K  I N V O I C E
        public ActionResult CetakInvoiceBPJSRJ(string noinvoice, int tampilkanYangKosong)
        {
            try
            {
                string reportname = $"SIM_Rpt_Invoice_BPJS_RJ";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname + ".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_RekapInvoicePasien_WEB_BPJS", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult CetakInvoiceBPJSRI(string noinvoice, int tampilkanYangKosong)
        {
            try
            {
                string reportname = $"SIM_Rpt_Invoice_BPJS_RI";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname + ".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_RekapInvoicePasien_WEB_BPJS", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region ===== C E T A K  I N V O I C E  C O B
        public ActionResult CetakInvoiceCOB(string noinvoice, int tampilkanYangKosong)
        {
            try
            {
                string reportname = $"SIM_Rpt_Invoice_Cob_RI";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname + ".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_RekapInvoicePasien_WEB_COB ", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region ===== C E T A K  K W I T A N S I

        public ActionResult CetakKwitansi(string noinvoice)
        {
            try
            {
                string reportname = $"SIM_Rpt_Kwintansi";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname+ ".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_Kwitansi_WEB", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region ===== C E T A K  R I N C I A N  O B A T

        public ActionResult CetakRincianObat(string noreg)
        {
            try
            {
                string reportname = $"SIM_Rpt_PemakaianObat_All_WEB";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname+".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand(reportname, conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult CetakRincianObatCOB(string noreg)
        {
            try
            {
                string reportname = $"SIM_Rpt_PemakaianObat_All_WEB";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), "SIM_Rpt_PemakaianObat_All_WEB_COB" + ".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_PemakaianObat_All_WEB_COB", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region ===== C E T A K  R I N C I A N  B H P

        public ActionResult CetakRincianBHP(string noreg)
        {
            try
            {
                string reportname = $"SIM_Rpt_RincianBHP_WEB";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname+".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand(reportname, conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion

        #region ===== C E T A K  

        public ActionResult CetakInvoiceAmbulance(string noinvoice, int tampilkanYangKosong)
        {
            try
            {
                string reportname = $"SIM_Rpt_Invoice_Ambulance";

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname + ".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand("SIM_Rpt_RekapInvoicePasien_WEB_Ambulance", conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoInvoice", noinvoice));
                    cmd[i].Parameters.Add(new SqlParameter("@TampilkanYangKosong", tampilkanYangKosong));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion


        #endregion

        #region ===== A L L  K W I T A N S I

        public ActionResult CetakPDF(string reportname, string nobukti)
        {
            try
            {

                var sp_name = reportname;
                if (reportname == "KASIR_Bukti_KasKeluar" || reportname == "KASIR_Bukti_KasMasuk" || reportname == "KASIR_Bukti_SetoranBank")
                {
                    sp_name = "KASIR_KNIKwitansi";
                }

                var param = "@NoBukti";
                if (reportname == "KASIR_BuktiObatBebas" || reportname == "KASIR_ReportBuktiTransaksi")
                {
                    param = "@No_Bukti";
                }

                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname+".rpt"));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand(sp_name, conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter(param, nobukti));

                    if(reportname == "KASIR_ReportBuktiTransaksi") cmd[i].Parameters.Add(new SqlParameter("@ReportType", ""));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult AA()
        {
            try
            {
                var reportname = "SIM_Rpt_Kwintansi_Deposit";
                var nobukti = "210113-PBO-0002";
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath(ServerPath), reportname));

                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<DataSet>();
                    var i = 0;
                    cmd.Add(new SqlCommand(reportname, conn));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    cmd[i].Parameters.Add(new SqlParameter("@NoBukti", nobukti));

                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Database.Tables[0].SetDataSource(ds[i].Tables[0]);

                    i++;
                    cmd.Add(new SqlCommand($"select * from sReport", conn));
                    cmd[i].CommandType = CommandType.Text;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new DataSet());
                    da[i].Fill(ds[i], "DataTable1");
                    rd.Subreports[0].Database.Tables[0].SetDataSource(ds[i].Tables[0]);
                };

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");

            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        #endregion
    }
}