﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Kasir.Controllers
{
    [Authorize(Roles = "Kasir")]
    public class LookUpController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== L I S T  R E G I S T R A S I

        [HttpPost]
        public string ListRegistrasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    //var sectionID = "";
                    //if (Request.Cookies["SectionID"] != null)
                    //{
                    //    if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                    //    {
                    //        sectionID = Request.Cookies["SectionID"].Value;
                    //    }
                    //}

                    IQueryable<Kasir_GetBillingPasien> proses = s.Kasir_GetBillingPasien;
                    if (filter[14] == "True")
                    {
                        proses = proses.Where(x=> x.ProsesPayment == true);
                    }

                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_GetBillingPasien.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_GetBillingPasien.NRM)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_GetBillingPasien.NamaPasien)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_GetBillingPasien.Nama_Customer)}.Contains(@0)", filter[3]);

                    //if (sectionID == "SEC080" && !string.IsNullOrEmpty(sectionID))
                    //{
                    //    proses = proses.Where(x => x.TipePelayanan == "RI");
                    //}

                    //if (sectionID == "SEC079" && !string.IsNullOrEmpty(sectionID))
                    //{
                    //    proses = proses.Where(x => x.TipePelayanan != "RI");
                    //}
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<RegistrasiViewModel>(x));
                    foreach (var x in m)

                    {
                        var nilai = s.Kasir_GetNilaiInvoice(x.NoReg);
                        if (nilai == null)
                        {
                            x.nilai = "0";
                        }
                        else
                        {
                            decimal nilaiInvoice = Convert.ToDecimal(nilai.FirstOrDefault());
                            x.nilai = IConverter.ToMoney(nilaiInvoice);
                        }
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListRegistrasiBilling(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<KASIR_GetListPasienSiapPulang_ListBilling_Result> proses = s.KASIR_GetListPasienSiapPulang_ListBilling();

                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_ListBilling_Result.Status_Pulang)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_ListBilling_Result.SectionPerawatan)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_ListBilling_Result.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_ListBilling_Result.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_ListBilling_Result.JenisPasien)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_ListBilling_Result.SisaResep)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_ListBilling_Result.SisaPenunjang)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(KASIR_GetListPasienSiapPulang_ListBilling_Result.SisaRJ)}.Contains(@0)", filter[7]);
                    if (filter[14] != "")
                    {
                        if (filter[14] == "True")
                        {
                            proses = proses.Where(x => x.ProsesPayment == true);
                        }
                    }
                    if (filter[15] != "ALL")
                    {
                        var tipepelayanan = (filter[15] == "1") ? true : false;
                        proses = proses.Where(x => x.RawatInap == tipepelayanan);
                    }
                    if (filter[16] != "ALL")
                    {
                        var JenisPasien = filter[16];
                        proses = proses.Where(x => x.JenisPasien.Contains(JenisPasien));
                    }
                    proses = proses.OrderBy("NoReg ASC");
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                    foreach (var x in m)
                    {
                        var nilai = s.Kasir_GetNilaiInvoice(x.NoReg);
                        if (nilai == null)
                        {
                            x.nilai = "0";
                        }
                        else
                        {
                            decimal nilaiInvoice = Convert.ToDecimal(nilai.FirstOrDefault());
                            x.nilai = IConverter.ToMoney(nilaiInvoice);
                        }
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== L I S T  R E G I S T R A S I  R E P O R T

        [HttpPost]
        public string ListRegistrasiReport(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {

                    IQueryable<VW_Registrasi> proses = s.VW_Registrasi.Where(x => x.RawatInap == true && x.Batal == false && x.StatusBayar == "Sudah Bayar");
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(VW_Registrasi.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                    {
                        proses = proses.Where("TglReg = @0", DateTime.Parse(filter[1]));
                    }
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(VW_Registrasi.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(VW_Registrasi.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(VW_Registrasi.NoKartu)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6]))
                    {
                        string umur = filter[6];
                        proses = proses.Where(x => x.UmurThn.ToString().Contains(umur));
                    }
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(VW_Registrasi.Nama_Customer)}.Contains(@0)", filter[7]);
                    if (!string.IsNullOrEmpty(filter[8])) proses = proses.Where($"{nameof(VW_Registrasi.Alamat)}.Contains(@0)", filter[8]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<RegistrasiReportViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  I N A C B G

        [HttpPost]
        public string ListINACBG(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmTarifINA> proses = s.SIMmTarifINA;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(SIMmTarifINA.Kode)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(SIMmTarifINA.Deskripsi)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<INACBGViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  D I S C O U N T

        [HttpPost]
        public string ListDiscount(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_Discount_NEW_Result> proses = s.Kasir_Discount_NEW(filter[11], filter[12], filter[13], filter[14]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_Discount_NEW_Result.NamaDiscount)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_Discount_NEW_Result.NamaDokter)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_Discount_NEW_Result.Jasa)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_Discount_NEW_Result.Kelas)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4]))
                    {
                        string nilai = filter[4];
                        proses = proses.Where(x => x.NilaiDiscont.ToString().Contains(nilai));
                    }
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_Discount_NEW_Result.TipeDiscount)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Kasir_Discount_NEW_Result.KomponenName)}.Contains(@0)", filter[6]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DiscountViewModel>(x));
                    foreach (var x in m)
                    {
                        x.NilaiDiscont_View = ((decimal)(x.NilaiDiscont == null ? 0 : x.NilaiDiscont)).ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  O B A T  B E B A S

        [HttpPost]
        public string ListObatBebas(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_GetObatBebas> proses = s.Kasir_GetObatBebas.Where(x => x.ClosePayment == false);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_GetObatBebas.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_GetObatBebas.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_GetObatBebas.Total)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ObatBebasViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Total_View = x.Total.Value.ToMoney();

                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  M E R C H A N

        [HttpPost]
        public string ListMerchan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_ListMerchan> proses = s.Kasir_ListMerchan;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListMerchan.ID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_ListMerchan.NamaBank)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<MerchanViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  I N V O I C E  O U T S T A N D I N G

        [HttpPost]
        public string ListInvoiceOutstanding(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_GetPasienOutstanding> proses = s.Kasir_GetPasienOutstanding;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_GetPasienOutstanding.NoInvoice)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                    {
                        proses = proses.Where("Tanggal = @0", DateTime.Parse(filter[1]));
                    }
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_GetPasienOutstanding.NoReg)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_GetPasienOutstanding.NRM)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_GetPasienOutstanding.NamaPasien)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_GetPasienOutstanding.JenisKerjasama)}.Contains(@0)", filter[5]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<InvoiceOutStandingViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.NilaiAwal_View = x.NilaiAwal.ToMoney();
                        x.NilaiAkumulasi_View = x.NilaiAkumulasi.Value.ToMoney();
                        x.NilaiKewajiban_View = x.NilaiKewajiban.Value.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  I N V O I C E  L O S T

        [HttpPost]
        public string ListInvoiceLost(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_GetPasienLost> proses = s.Kasir_GetPasienLost;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_GetPasienLost.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                    {
                        proses = proses.Where("TglReg = @0", DateTime.Parse(filter[1]));
                    }
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_GetPasienLost.NoReg)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_GetPasienLost.NRM)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_GetPasienLost.NamaPasien)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_GetPasienLost.JenisKerjasama)}.Contains(@0)", filter[5]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<InvoiceLostViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.NilaiAwal_View = x.NilaiAwal.ToMoney();
                        x.NilaiAkumulasi_View = x.NilaiAkumulasi.Value.ToMoney();
                        x.NilaiKewajiban_View = x.NilaiKewajiban.Value.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== S E L E C T  D O K T E R
        [HttpPost]
        public string SelectListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mDokter> proses = s.mDokter.Where(x => x.Active != false);
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.DokterID.Contains(param) || x.NamaDOkter.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new StandardViewModel()
                    {
                        Kode = x.DokterID,
                        Nama = x.NamaDOkter
                    });

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E L E C T  S E C T I O N
        [HttpPost]
        public string SelectListSection(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.StatusAktif != false);
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.SectionID.Contains(param) || x.SectionName.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new StandardViewModel()
                    {
                        Kode = x.SectionID,
                        Nama = x.SectionName
                    });

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E L E C T  K A M A R
        [HttpPost]
        public string SelectListKamar(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mKamarDetail> proses = s.mKamarDetail.Where(x => x.Aktif != false);
                    var param = filter[0];
                    if (!string.IsNullOrEmpty(param)) proses = proses.Where(x => x.NoKamar.Contains(param));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => new StandardViewModel()
                    {
                        Kode = x.NoKamar,
                        Nama = x.NoKamar
                    });

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== GET DATA REGISTRASI
        [HttpGet]
        public string GetDataRegistrasi(string id)
        {
            var m = new Kasir_GetBillingPasien();
            var result = new RegistrasiViewModel();
            var nama_perusahaan = "";
            using (var s = new SIMEntities())
            {
                m = s.Kasir_GetBillingPasien.FirstOrDefault(x => x.NoReg == id);
                var reg = s.VW_Registrasi.FirstOrDefault(x => x.NoReg == m.NoReg);
                if (reg != null) nama_perusahaan = reg.Nama_Customer;
            }
            if (m != null)
            {
                result = IConverter.Cast<RegistrasiViewModel>(m);
                result.Deposit_View = m.Deposit == null ? "0" : ((decimal)m.Deposit).ToMoney();
                result.Nama_Customer = nama_perusahaan;
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result,
            });
        }
        #endregion

        #region ==== GET DATA INACBG
        [HttpGet]
        public string GetDataINACBG(string id, string kelas)
        {
            SIMmTarifINA m;
            using (var s = new SIMEntities())
            {
                m = s.SIMmTarifINA.FirstOrDefault(x => x.Kode == id);
                if (m == null) throw new Exception("Data tidak ditemukan");
            }
            var result = IConverter.Cast<INACBGViewModel>(m);
            result.TarifBPJS = kelas == "11" ? m.Tarif.ToMoney() : kelas == "13" ? m.TarifKelas2.Value.ToMoney() : kelas == "3" ? m.tarifKelas3.Value.ToMoney() : "";
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result,
            });
        }
        #endregion

        #region ==== GET DATA PASIEN CLOSING
        [HttpGet]
        public string GetDataPasienCLosing()
        {
            var result = new ListDetail<SIMtrRJ>();
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.DistinctBy(x => x.RegNo).Where(x => x.Audit == true && x.Batal == false).ToList();
                if (m.Count() > 0)
                {
                    foreach (var d in m)
                    {
                        var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.StatusBayar != "Sudah Bayar" && x.NoReg == d.RegNo);
                        if (reg != null)
                        {
                            d.NamaPasien = reg.NamaPasien_Reg;
                            result.Add(false, d);
                        }
                    }
                }
            }

            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result,
            });
        }
        #endregion

        #region ===== L I S T  A K U N

        [HttpPost]
        public string ListRekening(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mst_akun> proses = s.mst_akun;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mst_akun.Akun_ID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mst_akun.Akun_No)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(mst_akun.Akun_Name)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<AkunViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  T I P E  T R A N S A K S I 

        [HttpPost]
        public string ListTipeTransaksi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var tipe = filter[10];
                    IQueryable<KASIR_mTypeTransaksiKasir> proses = s.KASIR_mTypeTransaksiKasir.Where(x => x.Tipe == tipe);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(KASIR_mTypeTransaksiKasir.NamaType)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(KASIR_mTypeTransaksiKasir.NamaType)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<KASIR_mTypeTransaksiKasir>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  T I P E  P E M B A Y A R A N

        [HttpPost]
        public string ListTipePembayaran(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<KASIR_mTypeBayarKasir> proses = s.KASIR_mTypeBayarKasir;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(KASIR_mTypeBayarKasir.NamaJenisBayar)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(KASIR_mTypeBayarKasir.NamaJenisBayar)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<KASIR_mTypeBayarKasir>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== S E L E C T 2
        [HttpPost]
        public string listSelect2Akun(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mst_akun> proses = s.mst_akun;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mst_akun.Akun_ID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mst_akun.Akun_Name)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(mst_akun.Aktif)}=@0", true);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<mst_akun>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region L I S T
        [HttpPost]
        public string ListRegAsal(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_GetListAsalNoReg_SplitBill> proses = s.Kasir_GetListAsalNoReg_SplitBill;

                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_GetListAsalNoReg_SplitBill.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                    {
                        var tglkeluar = DateTime.Parse(filter[1]);
                        proses = proses.Where(x => x.tglKeluar == tglkeluar);
                    }
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_GetListAsalNoReg_SplitBill.NoBukti)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_GetListAsalNoReg_SplitBill.NRM)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_GetListAsalNoReg_SplitBill.NamaPasien)}.Contains(@0)", filter[4]);

                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<RegAsalViewModel>(x));
                    foreach (var x in m)
                    {
                        x.tglKeluar_View = x.tglKeluar.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region L I S T
        [HttpPost]
        public string ListRegTujuan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMtrRegistrasi> proses = s.SIMtrRegistrasi.Where(x => x.Batal == false && x.StatusBayar == "Belum");

                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(SIMtrRegistrasi.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(SIMtrRegistrasi.NRM)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(SIMtrRegistrasi.NamaPasien_Reg)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3]))
                    {
                        var tglreg = DateTime.Parse(filter[3]);
                        proses = proses.Where(x => x.TglReg == tglreg);
                    }

                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => new PasienViewModel()
                    {
                        NoReg = x.NoReg,
                        NamaPasien = x.NamaPasien_Reg,
                        NRM = x.NRM,
                        TglReg_View = x.TglReg.ToString("dd/MM/yyyy")
                    });
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion


        #region ===== L I S T  M E R C H A N

        [HttpPost]
        public string ListCustomer(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mCustomer> proses = s.mCustomer.Where(x => x.Active == true);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mCustomer.Kode_Customer)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mCustomer.Nama_Customer)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<mCustomer>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

    }
}