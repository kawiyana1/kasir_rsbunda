﻿using System;
using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;


namespace Kasir.Controllers
{
    [Authorize(Roles = "Kasir")]
    public class PembayaranObatBebasController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoNumber_Kasir_ObatBebas().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_ListPembayaranObatBebas> proses = s.Kasir_ListPembayaranObatBebas;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListPembayaranObatBebas.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_ListPembayaranObatBebas.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_ListPembayaranObatBebas.TipePasien)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3]))
                    {
                        string nilai = filter[3];
                        proses = proses.Where(x => x.NilaiTransaksi.ToString().Contains(nilai));
                    }
                    //var sectionID = "";
                    //if (Request.Cookies["SectionID"] != null)
                    //{
                    //    if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                    //    {
                    //        sectionID = Request.Cookies["SectionID"].Value;
                    //    }
                    //}
                    //if (!string.IsNullOrEmpty(sectionID)) proses = proses.Where($"{nameof(Kasir_ListPembayaranObatBebas.SectionID)}.Contains(@0)", sectionID);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PembayaranObatBebasViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Jam_View = x.Jam.ToString("HH:mm");
                        x.NilaiTransaksi_View = x.NilaiTransaksi.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var model = new PembayaranObatBebasInsertViewModel();
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new PembayaranObatBebasInsertViewModel();
                TryUpdateModel(item);

                var nobukti = "";
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        if (item.Total.ToDecimal() != item.NilaiTransaksi_View.ToDecimal())
                        {
                            return JsonHelper.JsonMsgInfo("Total dan nilai transaksi harus sama");
                        }

                        if (!string.IsNullOrEmpty(item.NilaiPembayaranCC_View) && item.NilaiPembayaranCC_View != "0")
                        {
                            if(item.IDBank == null || item.NamaBank == null || item.IDBank == "" || item.NamaBank == "") { 
                                return JsonHelper.JsonMsgInfo("Jika kartu kredit maka merchan bank harus di isi.");
                            }
                        }
                        var m = IConverter.Cast<SIMtrPembayaranObatBebas>(item);
                        m.NilaiPembayaranBebanRS = item.NilaiPembayaranBebanRS_View.ToDecimal();
                        m.NilaiPembayaranCC = item.NilaiPembayaranCC_View.ToDecimal();
                        m.NilaiPembayaranKredit = item.NilaiPembayaranKredit_View.ToDecimal();
                        m.NilaiPembayaranTunai = item.NilaiPembayaranTunai_View.ToDecimal();
                        m.NilaiPembayaranTunaiBPD = item.NilaiPembayaranTunaiBPD_View.ToDecimal();
                        m.NilaiTransaksi = item.NilaiTransaksi_View.ToDecimal();
                        m.Batal = false;
                        m.Audit = false;
                        if (Request.Cookies["SectionID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                            {
                                m.SectionID = Request.Cookies["SectionID"].Value;
                            }
                        }

                        if (Request.Cookies["Shift"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["Shift"].Value))
                            {
                                m.Shift = Request.Cookies["Shift"].Value;
                            }
                        }
                        nobukti = item.NoBukti;
                        m.UserIDWeb = User.Identity.GetUserId();
                        s.SIMtrPembayaranObatBebas.Add(m);
                        result = new ResultSS(s.SaveChanges());
                        if (result.IsSuccess == true)
                        {
                            var bill = s.BILLFarmasi.FirstOrDefault(x => x.NoBukti == m.NoBuktiFarmasi);
                            bill.ClosePayment = true;
                        }

                        s.SaveChanges();
                            
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrPembayaranObatBebas Create {nobukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            var item = new PembayaranObatBebasInsertViewModel(); 
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrPembayaranObatBebas.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    var obatbebas = s.Kasir_GetObatBebas.FirstOrDefault(x => x.NoBukti == m.NoBuktiFarmasi);
                    if (!string.IsNullOrEmpty(m.IDBank))
                    {
                        var merchan = s.SIMmMerchan.FirstOrDefault(x => x.ID == m.IDBank);
                        item.NamaBank = merchan.NamaBank;
                    }
                    item = IConverter.Cast<PembayaranObatBebasInsertViewModel>(m);
                    item.NilaiTransaksi_View = m.NilaiTransaksi.ToMoney();
                    item.NilaiPembayaranTunaiBPD_View = m.NilaiPembayaranTunaiBPD.Value.ToMoney();
                    item.NilaiPembayaranBebanRS_View = m.NilaiPembayaranBebanRS.Value.ToMoney();
                    item.NilaiPembayaranCC_View = m.NilaiPembayaranCC.Value.ToMoney();
                    item.NilaiPembayaranKredit_View = m.NilaiPembayaranKredit.Value.ToMoney();
                    m.NilaiPembayaranTunaiBPD = item.NilaiPembayaranTunaiBPD_View.ToDecimal();
                    item.NilaiPembayaranTunai_View = m.NilaiPembayaranTunai.Value.ToMoney();
                    item.NamaPasien = obatbebas.NamaPasien;
                    item.DetailObat = obatbebas.DeskripsiObat;
                    item.TipePasien = obatbebas.TipePasien;
                    item.TglFarmasi = obatbebas.Tanggal.ToString("dd/MM/yyyy");
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new PembayaranObatBebasInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        if (item.Total.ToDecimal() != item.NilaiTransaksi_View.ToDecimal())
                        {
                            return JsonHelper.JsonMsgInfo("Total dan nilai transaksi harus sama");
                        }

                        var model = s.SIMtrPembayaranObatBebas.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        if (!string.IsNullOrEmpty(item.NilaiPembayaranCC_View) && item.NilaiPembayaranCC_View != "0")
                        {
                            if(item.IDBank == null || item.NamaBank == null || item.IDBank == "" || item.NamaBank == "") { 
                                return JsonHelper.JsonMsgInfo("Jika kartu kredot maka merchan bank harus di isi.");
                            }
                        }
                      
                        TryUpdateModel(model);
                        model.NilaiPembayaranTunaiBPD = item.NilaiPembayaranTunaiBPD_View.ToDecimal();
                        model.NilaiPembayaranBebanRS = item.NilaiPembayaranBebanRS_View.ToDecimal();
                        model.NilaiPembayaranCC = item.NilaiPembayaranCC_View.ToDecimal();
                        model.NilaiPembayaranKredit = item.NilaiPembayaranKredit_View.ToDecimal();
                        model.NilaiPembayaranTunai = item.NilaiPembayaranTunai_View.ToDecimal();
                        model.UserIDWeb = User.Identity.GetUserId();
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrPembayaranObatBebas Edit {model.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id)
        {
            try
            {
                ResultSS result;

                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrPembayaranObatBebas.FirstOrDefault(x=> x.NoBukti == id);
                    m.Batal = true;
                    var bill = s.BILLFarmasi.FirstOrDefault(x => x.NoBukti == m.NoBuktiFarmasi);
                    bill.ClosePayment = false;
                    result = new ResultSS(1, s.SaveChanges());
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"SIMtrPembayaranObatBebas Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}