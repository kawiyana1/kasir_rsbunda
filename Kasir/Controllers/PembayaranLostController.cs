﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;


namespace Kasir.Controllers
{
    [Authorize(Roles = "Kasir")]
    public class PembayaranLostController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoNumber_Kasir_Oustanding().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_ListOutstanding> proses = s.Kasir_ListOutstanding.Where(x => x.Jenis.ToUpper() == "LOST");
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListOutstanding.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_ListOutstanding.NoReg)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_ListOutstanding.NRM)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_ListOutstanding.NamaPasien)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_ListOutstanding.JenisKerjasama)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6]))
                    {
                        string nilai = filter[6];
                        proses = proses.Where(x => x.NilaiPembayaran.ToString().Contains(nilai));
                    }
                    var sectionID = "";
                    if (Request.Cookies["SectionID"] != null)
                    {
                        if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                        {
                            sectionID = Request.Cookies["SectionID"].Value;
                        }
                    }
                    if (!string.IsNullOrEmpty(sectionID)) proses = proses.Where($"{nameof(Kasir_ListOutstanding.SectionID)}.Contains(@0)", sectionID);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PembayaranLostViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.NilaiPembayaran_View = x.NilaiPembayaran.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var model = new PembayaranLostInsertViewModel();
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new PembayaranLostInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {

                        var m = IConverter.Cast<SIMtrPembayaranOutStanding>(item);
                        m.NilaiPembayaran = item.NilaiPembayaran_View.ToDecimal();
                        m.NilaiAwal = item.NilaiAwal_View.ToDecimal();
                        m.NilaiAkumulaiPembayaran = item.NilaiAkumulaiPembayaran_View.ToDecimal();
                        m.AddCharge_Persen = (double)item.AddCharge_Persen_View.ToDecimal();
                        m.AddCharge = m.NilaiPembayaran * (decimal)m.AddCharge_Persen / 100;
                        m.Jenis = "Lost";
                        m.Batal = false;
                        m.Audit = false;
                        if (Request.Cookies["Shift"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["Shift"].Value))
                            {
                                m.Shift = Request.Cookies["Shift"].Value;
                            }
                        }
                        if (Request.Cookies["SectionID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                            {
                                m.SectionID = Request.Cookies["SectionID"].Value;
                            }
                        }
                        m.UserIDWeb = User.Identity.GetUserId();
                        s.SIMtrPembayaranOutStanding.Add(m);

                        result = new ResultSS(s.SaveChanges());
                        if (result.IsSuccess == true)
                        {
                            if (m.NilaiAwal == m.NilaiPembayaran)
                            {
                                var kasir = s.SIMtrKasir.FirstOrDefault(x => x.NoBukti == m.NoInvoice);
                                if (kasir != null)
                                {
                                    kasir.Lunas = true;
                                    kasir.TglLunas = DateTime.Now;
                                }
                                s.SaveChanges();
                            }
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrPembayaranOutStanding Lost Create {m.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id)
        {
            try
            {
                ResultSS result;

                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrPembayaranOutStanding.FirstOrDefault(x => x.NoBukti == id);
                    m.Batal = true;
                    var kasir = s.SIMtrKasir.FirstOrDefault(x => x.NoBukti == m.NoInvoice);
                    kasir.Lunas = false;
                    result = new ResultSS(1, s.SaveChanges());
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"SIMtrPembayaranOutStanding Lost Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            PembayaranLostInsertViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrPembayaranOutStanding.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<PembayaranLostInsertViewModel>(m);
                    item.AddCharge_Persen_View = ((decimal)m.AddCharge_Persen).ToMoney();
                    item.AddCharge_View = m.AddCharge.Value.ToMoney();
                    item.NilaiAkumulaiPembayaran_View = m.NilaiAkumulaiPembayaran.ToMoney();
                    item.NilaiAwal_View = m.NilaiAwal.ToMoney();
                    item.NilaiPembayaran_View = m.NilaiPembayaran.ToMoney();
                    item.Sisa = (m.NilaiAwal - m.NilaiPembayaran).ToMoney();
                    var kasir = s.SIMtrKasir.FirstOrDefault(x => x.NoBukti == m.NoInvoice);
                    if (kasir != null)
                    {
                        item.TanggalInvoice = kasir.Tanggal.ToString("dd/MM/yyyy");
                    }
                    var reg = s.VW_Registrasi.FirstOrDefault(x => x.NoReg == m.NoReg);
                    if (reg != null)
                    {
                        item.NRM = reg.NRM;
                        item.NamaPasien = reg.NamaPasien;
                        item.TipePasien = reg.JenisKerjasama;
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion
    }
}