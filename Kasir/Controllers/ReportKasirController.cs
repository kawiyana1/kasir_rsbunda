﻿using Newtonsoft.Json;
using Kasir.Helper;
using Kasir.Entities;
using Kasir.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Kasir.Controllers
{
    public class ReportKasirController : Controller
    {
        private string serverpath = "~/CrystalReports/";
        private string category = "Kasir";

        #region ==== H REPORT
        public ActionResult Index(string dir = "Kasir")
        {
            category = dir;
            var hreport = new HReport(Server, serverpath);
            var r = new ReportHelperViewModel()
            {
                Name = category,
                Reports = hreport.List(category),
                UserID = User.Identity.GetUserId(),
                UserName = User.Identity.GetUserName(),
                SectionID = Request.Cookies["SectionID"].Value,
                SectionName = Request.Cookies["SectionName"].Value,
            };
            return View(r);
        }

        public ActionResult ExportPDF(string filename, string param)
        {
            try
            {
                string server = "";
                string database = "";
                string userid = "";
                string pass = "";
                foreach (var x in ConfigurationManager.ConnectionStrings["SIMConnectionString"].ConnectionString.Split(';'))
                {
                    if (x.Trim().ToUpper().IndexOf("SERVER") == 0)
                        server = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("DATABASE") == 0)
                        database = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("USER ID") == 0)
                        userid = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("PASSWORD") == 0)
                        pass = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                }

                var hreport = new HReport(Server, serverpath);
                var parameters = JsonConvert.DeserializeObject<List<HReportParameter>>(param);
                var stream = hreport.ExportPDF(server, database, userid, pass, category, filename, parameters);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult ExportExcel(string dir, string filename, string param)
        {
            try
            {
                //var category = dir;
                string server = "";
                string database = "";
                string userid = "";
                string pass = "";
                foreach (var x in ConfigurationManager.ConnectionStrings["SIMConnectionString"].ConnectionString.Split(';'))
                {
                    if (x.Trim().ToUpper().IndexOf("SERVER") == 0)
                        server = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("DATABASE") == 0)
                        database = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("USER ID") == 0)
                        userid = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                    else if (x.Trim().ToUpper().IndexOf("PASSWORD") == 0)
                        pass = x.Split('=').Length > 0 ? x.Split('=')[1].Trim() : "";
                }

                var hreport = new HReport(Server, serverpath);
                var parameters = JsonConvert.DeserializeObject<List<HReportParameter>>(param);
                var stream = hreport.ExportExcel(server, database, userid, pass, category, filename, parameters);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                return File(stream, "application/vnd.ms-excel");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}