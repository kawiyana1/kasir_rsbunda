﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Kasir.Controllers
{
    [Authorize(Roles = "Kasir")]
    public class PembayaranDepositController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoNumber_Kasir_Deposit().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_ListDeposit> proses = s.Kasir_ListDeposit;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListDeposit.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_ListDeposit.NOReg)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_ListDeposit.NRM)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_ListDeposit.NamaPasien)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Kasir_ListDeposit.Alamat)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7]))
                    {
                        string nilai = filter[7];
                        proses = proses.Where(x => x.NilaiDeposit.ToString().Contains(nilai));
                    }
                    if (!string.IsNullOrEmpty(filter[3]))
                    {
                        proses = proses.Where("TglReg = @0", DateTime.Parse(filter[3]));
                    }
                    var sectionID = "";
                    if (Request.Cookies["SectionID"] != null)
                    {
                        if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                        {
                            sectionID = Request.Cookies["SectionID"].Value;
                        }
                    }
                    if (!string.IsNullOrEmpty(sectionID)) proses = proses.Where($"{nameof(Kasir_ListDeposit.SectionID)}.Contains(@0)", sectionID);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PembayaranDepositViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        x.NilaiDeposit_View = x.NilaiDeposit.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var model = new PembayaranDepositInsertViewModel();
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new PembayaranDepositInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        
                        var m = IConverter.Cast<SIMtrDeposit>(item);
                        m.NilaiDeposit = item.NilaiDeposit_View.ToDecimal();
                        m.AddCharge_Persen = (double)item.AddCharge_Persen_View.ToDecimal();
                        m.AddCharge = m.NilaiDeposit * (decimal)m.AddCharge_Persen / 100;
                        m.Batal = false;
                        m.Audit = false;
                        if (Request.Cookies["Shift"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["Shift"].Value))
                            {
                                m.Shift = Request.Cookies["Shift"].Value;
                            }
                        }
                        if (Request.Cookies["SectionID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                            {
                                m.SectionID = Request.Cookies["SectionID"].Value;
                            }
                        }
                        m.UserIDWeb = User.Identity.GetUserId();
                        s.SIMtrDeposit.Add(m);
                        result = new ResultSS(s.SaveChanges());
                        
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrDeposit Create {m.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            var item = new PembayaranDepositInsertViewModel();
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrDeposit.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                   
                    item = IConverter.Cast<PembayaranDepositInsertViewModel>(m);
                    if (!string.IsNullOrEmpty(m.IDBank))
                    {
                        var merchan = s.SIMmMerchan.FirstOrDefault(x => x.ID == m.IDBank);
                        if (merchan != null)
                        {
                            item.NamaBank = merchan.NamaBank;
                        }
                    }
                    item.NilaiDeposit_View = m.NilaiDeposit.ToMoney();
                    item.AddCharge_Persen_View = ((decimal)m.AddCharge_Persen).ToMoney();

                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new PembayaranDepositInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        var model = s.SIMtrDeposit.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        TryUpdateModel(model);
                        model.NilaiDeposit = item.NilaiDeposit_View.ToDecimal();
                        model.AddCharge_Persen = (double)item.AddCharge_Persen_View.ToDecimal();
                        model.AddCharge = model.NilaiDeposit * (decimal)model.AddCharge_Persen / 100;
                        model.UserIDWeb = User.Identity.GetUserId();
                        model.Keterangan = item.Keterangan;
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrDeposit Edit {model.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id)
        {
            try
            {
                ResultSS result;

                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrDeposit.FirstOrDefault(x => x.NoBukti == id);
                    m.Batal = true;
                    result = new ResultSS(1, s.SaveChanges());
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"SIMtrDeposit Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}