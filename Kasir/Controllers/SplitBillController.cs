﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
namespace Kasir.Controllers
{
    [Authorize(Roles = "Kasir")]
    public class SplitBillController : Controller
    {
        // GET: SplitBill
        public ActionResult Index()
        {
            return View();
        }

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<VW_Kasir_SplitBill_GetList> proses = s.VW_Kasir_SplitBill_GetList;

                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]).AddDays(1));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(VW_Kasir_SplitBill_GetList.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(VW_Kasir_SplitBill_GetList.Tanggal)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(VW_Kasir_SplitBill_GetList.NoReg_Asal)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(VW_Kasir_SplitBill_GetList.NRMPasienAsal)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(VW_Kasir_SplitBill_GetList.NamaPasienAsal)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(VW_Kasir_SplitBill_GetList.NoReg_Tujuan)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(VW_Kasir_SplitBill_GetList.NRMPasienTujuan)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(VW_Kasir_SplitBill_GetList.NamaPasienTujuan)}.Contains(@0)", filter[7]);

                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<SplitBillViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal?.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new SplitBillInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Validation
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<SplitBillDetailViewModel>();
                                item.Detail_List.RemoveAll(x => x.Remove);
                                #endregion
                                var nobukti = s.AutoNumber_SplitBill().FirstOrDefault();
                                var m = new SIMtrSplitBill()
                                {
                                    NoReg_Asal = item.NoRegAsal,
                                    NoReg_Tujuan = item.NoRegTujuan,
                                    Tanggal = DateTime.Now,
                                    UserIDWeb = User.Identity.GetUserId(),
                                    NoBukti = nobukti,
                                };

                                s.SIMtrSplitBill.Add(m);
                                foreach (var y in item.Detail_List)
                                {
                                    if (y.Model.Check == true)
                                    {
                                        s.Kasir_Update_SplitBill(item.NoRegAsal, item.NoRegTujuan, y.Model.NoBukti);
                                    }
                                    s.SIMtrSplitBill_Detail.Add(new SIMtrSplitBill_Detail
                                    {
                                        NoBukti = nobukti,
                                        NoBukti_Transaksi = y.Model.NoBukti,
                                        Pindahkan = y.Model.Check,
                                        Tgl_Transaksi = DateTime.Now
                                    });
                                }
                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"SIMtrSplitBill Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
                }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string GetDataSplit(string noreg)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var d = s.Kasir_GetSplitBill(noreg).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = d.ConvertAll(x => new
                        {
                            SectionName = x.SectionName,
                            NoBukti = x.NoBukti,
                            Total = x.Total
                        })
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #region ===== D E T A I L

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            SplitBillInsertViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.VW_Kasir_SplitBill_GetList.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    item = new SplitBillInsertViewModel() 
                    {
                        Detail_List = new ListDetail<SplitBillDetailViewModel>(),
                        NamaPasienAsal = m.NamaPasienAsal,
                        NamaPasienTujuan = m.NamaPasienTujuan,
                        NoRegAsal = m.NoReg_Asal,
                        NoRegTujuan = m.NoReg_Tujuan,
                        NRMAsal = m.NRMPasienAsal,
                        NRMTujuan = m.NRMPasienTujuan,
                        Tanggal = m.Tanggal,
                        TglKeluarAsal = m.TglKeluar?.ToString("dd/MM/yyyy"),
                        TglRegTujuan = m.TglReg.ToString("dd/MM/yyyy")
                    };

                    var d = s.VW_Kasir_SplitBill_Detail_GetList.Where(x => x.NoBukti == id).ToList();
                    foreach (var x in d)
                    {
                        item.Detail_List.Add(false, new SplitBillDetailViewModel() 
                        {
                            Check = x.Pindahkan ?? false,
                            NoBukti = x.NoBukti,
                            NoReg = x.NoBukti_Transaksi,
                            SectionName = x.SectionName,
                            Total = (decimal?)x.jml,
                            Total_View = ((decimal?)x.jml ?? 0).ToMoney()
                        });
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion
    }
}