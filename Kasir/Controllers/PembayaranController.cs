﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Kasir.Controllers
{
    [Authorize(Roles = "Kasir")]
    public class PembayaranController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoNumber_Kasir_Billing().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_ListKasir> proses = s.Kasir_ListKasir;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListKasir.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_ListKasir.NoReg)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4]))
                    {
                        proses = proses.Where("TglReg = @0", DateTime.Parse(filter[4]));
                    }
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_ListKasir.NRM)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Kasir_ListKasir.NamaPasien)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(Kasir_ListKasir.JenisKerjasama)}.Contains(@0)", filter[7]);
                    if (!string.IsNullOrEmpty(filter[8])) proses = proses.Where($"{nameof(Kasir_ListKasir.Nama_Customer)}.Contains(@0)", filter[8]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PembayaranKasirViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Jam_View = x.Jam.ToString("HH:mm");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg = null)
        {
            var model = new PembayaranInsertViewModel();
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;
            try
            {
                using (var s = new SIMEntities())
                {
                    if (noreg != null)
                    {
                        var dtreg = s.Kasir_GetBillingPasien.Where(x => x.NoReg == noreg).FirstOrDefault();
                        if (dtreg != null)
                        {
                            model.NoReg = dtreg.NoReg;
                            model.TglReg = dtreg.TglReg.ToString("yyyy-MM-dd");
                            model.NRM = dtreg.NRM;
                            model.NamaPasien = dtreg.NamaPasien;
                            model.Alamat = dtreg.Alamat;
                            model.TipePelayanan = dtreg.TipePelayanan;
                            model.KelasID = dtreg.KdKelasPelayanan;
                            model.JenisKerjasama = dtreg.JenisKerjasama;
                            var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == dtreg.NoReg);
                            if(reg != null)
                            {
                                model.NamaPerusahaan = reg.NoReg;
                            }
                        }
                    }
                    var d = s.mJenisBayar.Where(x => x.Active == true).ToList();
                    model.Detail_List = new ListDetail<PembayaranInsertDetailViewModel>();
                    foreach (var x in d)
                    {
                        var y = new PembayaranInsertDetailViewModel()
                        {
                            IDBayar = x.IDBayar,
                            NamaBayar = x.Description
                        };
                        model.Detail_List.Add(false, y);
                    }
                }
                TimeSpan time = DateTime.Now.TimeOfDay;
                var pagi = new {
                    time_in = new TimeSpan(00, 00, 00),
                    time_out = new TimeSpan(13, 30, 00)
                };

                var sore = new
                {
                    time_in = new TimeSpan(13, 30, 00),
                    time_out = new TimeSpan(19, 30, 00)
                };

                var malem = new
                {
                    time_in = new TimeSpan(19, 30, 00),
                    time_out = new TimeSpan(23, 59, 00)
                };

                var pagi_1 = TimeSpan.Compare(time, pagi.time_in);
                var pagi_2 = TimeSpan.Compare(time, pagi.time_out);

                var sore_1 = TimeSpan.Compare(time, sore.time_in);
                var sore_2 = TimeSpan.Compare(time, sore.time_out);

                var malem_1 = TimeSpan.Compare(time, malem.time_in);
                var malem_2 = TimeSpan.Compare(time, malem.time_out);

                if ((pagi_1 == 1) && (pagi_2 == -1))
                {
                    model.Shift = "Pagi";
                }else
                    if ((sore_1 == 1) && (sore_2 == -1))
                {
                    model.Shift = "Sore";
                }
                else
                    if ((malem_1 == 1) && (malem_2 == -1))
                {
                    model.Shift = "Malem";
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new PembayaranInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Validation
                                if (item.Discount_Detail_List == null)
                                    item.Discount_Detail_List = new ListDetail<DiscountInsertViewModel>();
                                item.Discount_Detail_List.RemoveAll(x => x.Remove);
                                #endregion

                                if (item.Sisa != "0" && !string.IsNullOrEmpty(item.Sisa) && item.Sisa != "-")
                                {
                                    return JsonHelper.JsonMsgInfo("Sisa harus 0");
                                }

                                if (item.OutStanding == true || item.Lost == true)
                                {
                                    if (item.Detail_List[3].Model.NilaiBayar_View.ToDecimal() == 0 || item.Detail_List[3].Model.NilaiBayar_View.ToDecimal() < 0)
                                    {
                                        return JsonHelper.JsonMsgInfo("Jenis pembayaran pasien OutStanding atau Pasien Lost harus KREDIT / BON");
                                    }
                                }

                                if (item.Detail_List[3].Model.NilaiBayar_View.ToDecimal() != 0)
                                {
                                    if (item.OutStanding == false && item.Lost == false) {
                                        return JsonHelper.JsonMsgInfo("Saat tipe pembayaran KREDIT / BON, OutStanding atau Pasien Lost salah satu harus dicentang");
                                    }
                                }

                                var m = IConverter.Cast<SIMtrKasir>(item);
                                m.NoBukti = s.AutoNumber_Kasir_Billing().FirstOrDefault();
                                m.Jam = item.Jam.AddSeconds(double.Parse(DateTime.Now.ToString("ss")));
;                               m.NilaiInvoice = (double)item.NilaiInvoice_View.ToDecimal();
                                m.NilaiDeposit = (double)item.NilaiDeposit_View.ToDecimal();
                                m.NilaiAddCharge = (double)item.NilaiAddCharge_View.ToDecimal();
                                m.NilaiDiscTdkLangsung = (double)item.NilaiDiscTdkLangsung_View.ToDecimal();
                                m.NilaiDiscount = (double)item.NilaiDiscount_View.ToDecimal();

                                if (item.InvoiceGabungNoReg != null)
                                {
                                    var nilaiInvoiceGabung1 = s.Kasir_GetNilaiInvoice(item.InvoiceGabungNoReg).FirstOrDefault() ?? 0;
                                    m.NilaiInvoiceGabung = (decimal)nilaiInvoiceGabung1;
                                }
                                else
                                {
                                    m.NilaiInvoiceGabung = 0;
                                }
                                if (item.InvoiceGabungNoReg2 != null)
                                {
                                    var nilaiInvoiceGabung2 = s.Kasir_GetNilaiInvoice(item.InvoiceGabungNoReg2).FirstOrDefault() ?? 0;
                                    m.NilaiInvoiceGabung2 = (decimal)nilaiInvoiceGabung2;
                                }
                                else
                                {
                                    m.NilaiInvoiceGabung2 = 0;
                                }
                                if (item.InvoiceGabungNoReg3 != null)
                                {
                                    var nilaiInvoiceGabung3 = s.Kasir_GetNilaiInvoice(item.InvoiceGabungNoReg3).FirstOrDefault() ?? 0;
                                    m.NilaiInvoiceGabung3 = (decimal)nilaiInvoiceGabung3;
                                }
                                else
                                {
                                    m.NilaiInvoiceGabung3 = 0;
                                }
                                
                                m.AddCharge = item.AddCharge_View.ToDecimal();
                                m.AddCharge_Persen = (double)item.AddCharge_Persen_View.ToDecimal();
                                m.TarifBPJS = item.TarifBPJS_View.ToDecimal();
                                m.BiayaLainBPJS = item.TambahanBPJS_View.ToDecimal();
                                m.Batal = false;
                                m.Audit = false;
                                m.MunculkanCaraBayar = item.MunculkanCaraBayar;
                                m.UserIDWeb = User.Identity.GetUserId();
                                if (Request.Cookies["Shift"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Request.Cookies["Shift"].Value))
                                    {
                                        m.Shift = Request.Cookies["Shift"].Value;
                                    }
                                }

                                if (Request.Cookies["SectionID"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                                    {
                                        m.SectionID = Request.Cookies["SectionID"].Value;
                                    }
                                }


                                #region Detail Pembayaran
                                foreach (var x in item.Detail_List.Where(x => x.Model.NilaiBayar_View != "0"))
                                {
                                    x.Model.NoBukti = m.NoBukti;
                                    x.Model.NilaiBayar = x.Model.NilaiBayar_View.ToDecimal();
                                }
                                var d_pembayaran = item.Detail_List.ConvertAll(x => IConverter.Cast<SIMtrKasirDetail>(x.Model)).ToArray();
                                foreach (var x in d_pembayaran.Where(z => z.NilaiBayar != 0)) {
                                    if (x.IDBayar == 7)
                                    {
                                        m.NilaiPembayaranKKAwal = x.NilaiBayar;
                                    }
                                    s.SIMtrKasirDetail.Add(x); 
                                }
                                #endregion

                                #region Detail Discount
                                if (item.Discount_Detail_List.Count() > 0)
                                {
                                    foreach (var x in item.Discount_Detail_List)
                                    {
                                        x.Model.NoBukti = m.NoBukti;
                                        x.Model.NilaiDiscount = x.Model.NilaiDiscount_View.ToDecimal();
                                    }
                                    var d_discount = item.Discount_Detail_List.ConvertAll(x => IConverter.Cast<SIMtrKasirDiscount>(x.Model)).ToArray();
                                    foreach (var x in d_discount) { s.SIMtrKasirDiscount.Add(x); }
                                }
                                #endregion

                                #region Update Registrasi
                                var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == m.NoReg);
                                if (reg != null)
                                {
                                    reg.StatusBayar = "Sudah Bayar";
                                    reg.TglKeluar = m.Tanggal;
                                    reg.JamKeluar = m.Jam;
                                }

                                if (!string.IsNullOrEmpty(m.InvoiceGabungNoReg))
                                {
                                    var reg1 = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == m.InvoiceGabungNoReg);
                                    if (reg1 != null)
                                    {
                                        reg1.StatusBayar = "Sudah Bayar";
                                        reg1.TglKeluar = m.Tanggal;
                                        reg1.JamKeluar = m.Jam;
                                    }
                                }

                                if (!string.IsNullOrEmpty(m.InvoiceGabungNoReg2))
                                {
                                    var reg2 = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == m.InvoiceGabungNoReg2);
                                    if (reg2 != null)
                                    {
                                        reg2.StatusBayar = "Sudah Bayar";
                                        reg2.TglKeluar = m.Tanggal;
                                        reg2.JamKeluar = m.Jam;
                                    }
                                }

                                if (!string.IsNullOrEmpty(m.InvoiceGabungNoReg3))
                                {
                                    var reg3 = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == m.InvoiceGabungNoReg3);
                                    if (reg3 != null)
                                    {
                                        reg3.StatusBayar = "Sudah Bayar";
                                        reg3.TglKeluar = m.Tanggal;
                                        reg3.JamKeluar = m.Jam;
                                    }
                                }
                                #endregion

                                #region Update Pasien
                                var pasien = s.mPasien.FirstOrDefault(x => x.NRM == item.NRM);
                                if(pasien != null)
                                {
                                    pasien.SedangDirawat = false;
                                }
                                #endregion

                                #region Update Kamar
                                if(item.TipePelayanan == "RI")
                                {
                                    var kamar = s.mKamarDetail.FirstOrDefault(x => x.NoKamar == item.NoKamar && x.NoBed == item.NoBed);
                                    if (kamar != null)
                                    {
                                        kamar.Status = "AV";
                                    }
                                }
                                #endregion

                                s.SIMtrKasir.Add(m);

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"SIMtrKasir Cerate {m.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            PembayaranInsertViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {

                    var m = s.SIMtrKasir.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    var reg = s.VW_Registrasi.FirstOrDefault(x => x.NoReg == m.NoReg);
                    var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KelasID);
                    var kelasHak = new SIMmKelas();
                    if (reg.KdKelasPertanggungan != null)
                    {
                        kelasHak = s.SIMmKelas.FirstOrDefault(x => x.KelasID == reg.KdKelasPertanggungan);
                    }
                    var secPerawatan = new SIMmSection();
                    if (m.SectionPerawatanID != null)
                    {
                        secPerawatan = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionPerawatanID);
                    }
                    var dokter = new mDokter();
                    if (m.DokterID != null)
                    {
                         dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                    }

                    item = IConverter.Cast<PembayaranInsertViewModel>(m);
                    if(reg.COmpanyID != null)
                    {
                        var getCompany = s.mCustomer.FirstOrDefault(x => x.Kode_Customer == reg.COmpanyID);
                        if (getCompany != null)
                        {
                            item.NamaPerusahaan = getCompany.Nama_Customer;
                        }
                    }
                    if (secPerawatan != null)
                    {
                        item.SectionPerawatanName = secPerawatan.SectionName;
                    }
                    if (kelas != null)
                    {
                        item.NamaKelas = kelas.NamaKelas;
                    }
                    item.MunculkanCaraBayar = item.MunculkanCaraBayar;
                    item.NilaiInvoice_View = ((decimal)m.NilaiInvoice).ToMoney();
                    item.NilaiDiscTdkLangsung_View = ((decimal)m.NilaiDiscTdkLangsung).ToMoney();
                    item.NilaiDiscount_View = ((decimal)m.NilaiDiscount).ToMoney();
                    item.NilaiDeposit_View = ((decimal)m.NilaiDeposit).ToMoney();
                    item.NilaiAddCharge_View = ((decimal)m.NilaiAddCharge).ToMoney();
                    item.AddCharge_View = m.AddCharge == null ? "0" : m.AddCharge.Value.ToMoney();
                    item.AddCharge_Persen_View = ((decimal)(m.AddCharge_Persen == null ? 0 : m.AddCharge_Persen)).ToMoney();
                    item.TarifBPJS_View = ((decimal)m.TarifBPJS).ToMoney();
                    item.TipePelayananNama = m.TipePelayanan == "RJ" ? "Rawat Jalan" : m.TipePelayanan == "RI" ? "Rawat Inap" : m.TipePelayanan == "ODC" ? "ODC" : "";
                    if(dokter != null)
                    {
                        item.NamaDokter = dokter.NamaDOkter;
                    }
                    item.NoKamar = m.NoKamar;
                    item.IDBank = m.IDBank;
                    item.FromDate = (DateTime)m.FromDate;
                    item.ToDate = (DateTime)m.ToDate;
                    var bank = s.SIMmMerchan.FirstOrDefault(x => x.ID == m.IDBank);
                    if( bank != null)
                    {
                        item.NamaBank = bank.NamaBank;
                    }
                    var nilaiInvoiceGabung1 = m.NilaiInvoiceGabung ?? 0;
                    var nilaiInvoiceGabung2 = m.NilaiInvoiceGabung2 ?? 0;
                    var nilaiInvoiceGabung3 = m.NilaiInvoiceGabung3 ?? 0;
                    item.NilaiInvoiceGabungSemua = (nilaiInvoiceGabung1 + nilaiInvoiceGabung2 + nilaiInvoiceGabung3).ToMoney();
                    if (reg != null)
                    {
                        item.NamaPasien = reg.NamaPasien;
                        item.NRM = reg.NRM;
                        item.Alamat = reg.Alamat;
                        item.JenisKerjasama = reg.JenisKerjasama;
                        item.TglReg = reg.TglReg.ToString("dd/MM/yyyy");
                        if (kelasHak != null)
                        {
                            item.KelasHak = kelasHak == null ? "XX" : kelasHak.NamaKelas;
                        }
                    }
                    var d = s.Kasir_ListJenisBayar(id).ToList();
                    var d_disc = s.SIMtrKasirDiscount.Where(x => x.NoBukti == id).ToList();
                    item.Detail_List = new ListDetail<PembayaranInsertDetailViewModel>();
                    item.Discount_Detail_List = new ListDetail<DiscountInsertViewModel>();
                    foreach (var x in d)
                    {
                        if (true)
                        {
                            var y = IConverter.Cast<PembayaranInsertDetailViewModel>(x);
                            y.NamaBayar = x.Description;
                            y.NilaiBayar_View = x.NilaiBayar.Value.ToMoney();
                            item.Detail_List.Add(false, y);
                        }
                    }


                    foreach (var x in d_disc)
                    {
                        var y = IConverter.Cast<DiscountInsertViewModel>(x);
                        y.NilaiDiscount_View = x.NilaiDiscount.ToMoney();
                        var disc = s.Kasir_Discount(m.NoReg).FirstOrDefault(z=> z.IDDiscount == x.IDDiscount);
                        if(disc != null)
                        {
                            y.NamaDiscount = disc.NamaDiscount;
                            y.NamaDokter = disc.NamaDokter;
                            y.NamaJasa = disc.Jasa;
                            y.NamaKelas = disc.Kelas;
                            y.NilaiDiscountOriginal = ((decimal)disc.NilaiDiscont).ToMoney();
                        }
                        item.Discount_Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new PembayaranInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Validation
                                var model = s.SIMtrKasir.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                                if (model == null) throw new Exception("Data Tidak ditemukan");
                                if (item.Detail_List == null)
                                    item.Detail_List = new ListDetail<PembayaranInsertDetailViewModel>();
                                if (item.Discount_Detail_List == null)
                                    item.Discount_Detail_List = new ListDetail<DiscountInsertViewModel>();

                                if (item.Sisa != "0" && !string.IsNullOrEmpty(item.Sisa) && item.Sisa != "-")
                                {
                                    return JsonHelper.JsonMsgInfo("Sisa harus 0");
                                }
                                #endregion

                                #region Header
                                model.NilaiInvoice = (double)item.NilaiInvoice_View.ToDecimal();
                                model.NilaiDeposit = (double)item.NilaiDeposit_View.ToDecimal();
                                model.NilaiAddCharge = (double)item.NilaiAddCharge_View.ToDecimal();
                                model.NilaiDiscTdkLangsung = (double)item.NilaiDiscTdkLangsung_View.ToDecimal();
                                model.NilaiDiscount = (double)item.NilaiDiscount_View.ToDecimal();
                                model.TarifBPJS = item.TarifBPJS_View.ToDecimal();
                                model.AddCharge = item.AddCharge_View.ToDecimal();
                                model.AddCharge_Persen = (double)item.AddCharge_Persen_View.ToDecimal();
                                model.IDBank = item.IDBank;
                                var nilaiInvoiceGabung1 = s.Kasir_GetNilaiInvoice(item.InvoiceGabungNoReg).FirstOrDefault() ?? 0;
                                var nilaiInvoiceGabung2 = s.Kasir_GetNilaiInvoice(item.InvoiceGabungNoReg2).FirstOrDefault() ?? 0;
                                var nilaiInvoiceGabung3 = s.Kasir_GetNilaiInvoice(item.InvoiceGabungNoReg3).FirstOrDefault() ?? 0;
                                model.NilaiInvoiceGabung = (decimal)nilaiInvoiceGabung1;
                                model.NilaiInvoiceGabung2 = (decimal)nilaiInvoiceGabung2;
                                model.NilaiInvoiceGabung3 = (decimal)nilaiInvoiceGabung3;
                                model.InvoiceGabungNoReg = item.InvoiceGabungNoReg;
                                model.InvoiceGabungNoReg2 = item.InvoiceGabungNoReg2;
                                model.InvoiceGabungNoReg3 = item.InvoiceGabungNoReg3;
                                model.FromDate = item.FromDate;
                                model.ToDate = item.ToDate;
                                model.MunculkanCaraBayar = item.MunculkanCaraBayar;

                                model.TipePelayanan = item.TipePelayanan;
                                model.SectionPerawatanID = item.SectionPerawatanID;
                                model.DokterID = item.DokterID;
                                model.NoKamar = item.NoKamar;

                                //model.NilaiPembayaranLOG = item.NilaiPembayaranLOG;
                                model.KodeCustomerPenjamin = item.KodeCustomerPenjamin;
                                model.NamaCustomerPenjamin = item.NamaCustomerPenjamin;
                                model.NoPenjaminan = item.NoPenjaminan;
                                model.KeteranganPenjaminan = item.KeteranganPenjaminan;
                                #endregion

                                #region Detail Pembayaran
                                foreach (var x in item.Detail_List.Where(x => x.Model.NilaiBayar_View != "0"))
                                {
                                    x.Model.NoBukti = item.NoBukti;
                                    x.Model.NilaiBayar = x.Model.NilaiBayar_View.ToDecimal();
                                }
                                var new_list = item.Detail_List.Where(x => x.Model.NilaiBayar != 0);
                                var real_list = s.SIMtrKasirDetail.Where(x => x.NoBukti == item.NoBukti).ToList();
                                // delete | delete where (real_list not_in new_list)
                                foreach (var x in real_list)
                                {
                                    var m = new_list.FirstOrDefault(y => y.Model.IDBayar == x.IDBayar);
                                    if (m == null) s.SIMtrKasirDetail.Remove(x);
                                }

                                foreach (var x in new_list)
                                {
                                    var _m = real_list.FirstOrDefault(y => y.IDBayar == x.Model.IDBayar);
                                    // add | add where (new_list not_in raal_list)
                                    if (_m == null)
                                    {
                                        if (x.Model.IDBayar == 7)
                                        {
                                            model.NilaiPembayaranKKAwal = x.Model.NilaiBayar;
                                        }
                                        s.SIMtrKasirDetail.Add(new SIMtrKasirDetail()
                                        {
                                            NoBukti = x.Model.NoBukti,
                                            IDBayar = x.Model.IDBayar,
                                            NilaiBayar = x.Model.NilaiBayar,
                                        });
                                    }
                                    // edit | where (new_list in raal_list)
                                    else
                                    {
                                        _m.NilaiBayar = x.Model.NilaiBayar;
                                    }
                                }
                                #endregion

                                #region Detail Discount
                                foreach (var x in item.Discount_Detail_List)
                                {
                                    x.Model.NoBukti = item.NoBukti;
                                    x.Model.NilaiDiscount = x.Model.NilaiDiscount_View.ToDecimal();
                                }
                                var new_list_disc = item.Discount_Detail_List;
                                var real_list_disc = s.SIMtrKasirDiscount.Where(x => x.NoBukti == item.NoBukti).ToList();
                                // delete | delete where (real_list not_in new_list)
                                foreach (var x in real_list_disc)
                                {
                                    var m = new_list_disc.FirstOrDefault(y => y.Model.IDDiscount == x.IDDiscount);
                                    if (m == null) s.SIMtrKasirDiscount.Remove(x);
                                }

                                foreach (var x in new_list_disc)
                                {
                                    var _m = real_list_disc.FirstOrDefault(y => y.IDDiscount == x.Model.IDDiscount);
                                    // add | add where (new_list not_in raal_list)
                                    if (_m == null)
                                    {
                                        var d_discount = item.Discount_Detail_List.ConvertAll(d => IConverter.Cast<SIMtrKasirDiscount>(x.Model)).ToArray();
                                        foreach (var d in d_discount) { s.SIMtrKasirDiscount.Add(d); }
                                    }
                                    // edit | where (new_list in raal_list)
                                    else
                                    {
                                        _m.Persen = x.Model.Persen;
                                        _m.NilaiDiscount = x.Model.NilaiDiscount;
                                    }
                                }
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"SIMtrKasir Edit {model.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                if (ex.InnerException != null)
                                {
                                    if (ex.InnerException.Message == "Conflicting changes detected. This may happen when trying to insert multiple entities with the same key.")
                                    {
                                        throw new Exception("Detail tidak boleh sama");
                                    }
                                }
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            PembayaranInsertViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {

                    var m = s.SIMtrKasir.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) return HttpNotFound();
                    var reg = s.VW_Registrasi.FirstOrDefault(x => x.NoReg == m.NoReg);
                    var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KelasID);
                    var kelasHak = new SIMmKelas();
                    if (reg.KdKelasPertanggungan != null)
                    {
                        kelasHak = s.SIMmKelas.FirstOrDefault(x => x.KelasID == reg.KdKelasPertanggungan);
                    }
                    var secPerawatan = new SIMmSection();
                    if (m.SectionPerawatanID != null)
                    {
                        secPerawatan = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionPerawatanID);
                    }
                    var dokter = new mDokter();
                    if (m.DokterID != null)
                    {
                        dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                    }

                    item = IConverter.Cast<PembayaranInsertViewModel>(m);
                    if (reg.COmpanyID != null)
                    {
                        var getCompany = s.mCustomer.FirstOrDefault(x => x.Kode_Customer == reg.COmpanyID);
                        if (getCompany != null)
                        {
                            item.NamaPerusahaan = getCompany.Nama_Customer;
                        }
                    }
                    if (secPerawatan != null)
                    {
                        item.SectionPerawatanName = secPerawatan.SectionName;
                    }
                    if (kelas != null)
                    {
                        item.NamaKelas = kelas.NamaKelas;
                    }
                    item.NilaiInvoice_View = ((decimal)m.NilaiInvoice).ToMoney();
                    item.NilaiDiscTdkLangsung_View = ((decimal)m.NilaiDiscTdkLangsung).ToMoney();
                    item.NilaiDiscount_View = ((decimal)m.NilaiDiscount).ToMoney();
                    item.NilaiDeposit_View = ((decimal)m.NilaiDeposit).ToMoney();
                    item.NilaiAddCharge_View = ((decimal)m.NilaiAddCharge).ToMoney();
                    item.AddCharge_View = m.AddCharge == null ? "0" : m.AddCharge.Value.ToMoney();
                    item.AddCharge_Persen_View = ((decimal)(m.AddCharge_Persen == null ? 0 : m.AddCharge_Persen)).ToMoney();
                    item.TarifBPJS_View = ((decimal)m.TarifBPJS).ToMoney();
                    item.TipePelayananNama = m.TipePelayanan == "RJ" ? "Rawat Jalan" : m.TipePelayanan == "RI" ? "Rawat Inap" : m.TipePelayanan == "ODC" ? "ODC" : "";
                    if (dokter != null)
                    {
                        item.NamaDokter = dokter.NamaDOkter;
                    }
                    item.NoKamar = m.NoKamar;
                    item.IDBank = m.IDBank;
                    item.FromDate = (DateTime)m.FromDate;
                    item.ToDate = (DateTime)m.ToDate;
                    var bank = s.SIMmMerchan.FirstOrDefault(x => x.ID == m.IDBank);
                    if (bank != null)
                    {
                        item.NamaBank = bank.NamaBank;
                    }
                    var nilaiInvoiceGabung1 = m.NilaiInvoiceGabung ?? 0;
                    var nilaiInvoiceGabung2 = m.NilaiInvoiceGabung2 ?? 0;
                    var nilaiInvoiceGabung3 = m.NilaiInvoiceGabung3 ?? 0;
                    item.NilaiInvoiceGabungSemua = (nilaiInvoiceGabung1 + nilaiInvoiceGabung2 + nilaiInvoiceGabung3).ToMoney();
                    if (reg != null)
                    {
                        item.NamaPasien = reg.NamaPasien;
                        item.NRM = reg.NRM;
                        item.Alamat = reg.Alamat;
                        item.JenisKerjasama = reg.JenisKerjasama;
                        item.TglReg = reg.TglReg.ToString("dd/MM/yyyy");
                        if (kelasHak != null)
                        {
                            item.KelasHak = kelasHak == null ? "XX" : kelasHak.NamaKelas;
                        }
                    }
                    var d = s.Kasir_ListJenisBayar(id).ToList();
                    var d_disc = s.SIMtrKasirDiscount.Where(x => x.NoBukti == id).ToList();
                    item.Detail_List = new ListDetail<PembayaranInsertDetailViewModel>();
                    item.Discount_Detail_List = new ListDetail<DiscountInsertViewModel>();
                    foreach (var x in d)
                    {
                        var y = IConverter.Cast<PembayaranInsertDetailViewModel>(x);
                        y.NamaBayar = x.Description;
                        y.NilaiBayar_View = x.NilaiBayar.Value.ToMoney();
                        item.Detail_List.Add(false, y);
                    }


                    foreach (var x in d_disc)
                    {
                        var y = IConverter.Cast<DiscountInsertViewModel>(x);
                        y.NilaiDiscount_View = x.NilaiDiscount.ToMoney();
                        var disc = s.Kasir_Discount(m.NoReg).FirstOrDefault(z => z.IDDiscount == x.IDDiscount);
                        if (disc != null)
                        {
                            y.NamaDiscount = disc.NamaDiscount;
                            y.NamaDokter = disc.NamaDokter;
                            y.NamaJasa = disc.Jasa;
                            y.NamaKelas = disc.Kelas;
                            y.NilaiDiscountOriginal = ((decimal)disc.NilaiDiscont).ToMoney();
                        }
                        item.Discount_Detail_List.Add(false, y);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id, string alasan)
        {
            try
            {
                ResultSS result;

                using (var s = new SIMEntities())
                {
                    var batal = s.BatalKasirDenganAsalan(id, true, alasan);
                    result = new ResultSS(1, batal);
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"SIMtrKasir Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== R I N C I A N - B I A Y A

        [HttpGet]
        public ActionResult DetailBiaya(string noreg)
        {
            ViewBag.NoReg = noreg;
            return PartialView();
        }

        [HttpPost]
        public string ListDetailBiaya(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<GetDetailRincianBiaya_Result> proses = s.GetDetailRincianBiaya(filter[11], (int?)null);
                    if (!string.IsNullOrEmpty(filter[12]))
                        proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.KategoriBiaya)}=@0", filter[12]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.NoBukti)}.Contains(@0)", filter[0]);
                    if (IFilter.F_DateTime(filter[1]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Tanggal)}=@0", IFilter.F_DateTime(filter[1]));
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.JenisBiaya)}.Contains(@0)", filter[2]);
                    if (IFilter.F_Decimal(filter[3]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Qty)}=@0", (double)IFilter.F_Decimal(filter[3]));
                    if (IFilter.F_Decimal(filter[4]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Nilai)}=@0", IFilter.F_Decimal(filter[4]));
                    if (IFilter.F_Decimal(filter[5]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Disc)}=@0", (double)IFilter.F_Decimal(filter[5]));
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.SectionName)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.DokterName)}.Contains(@0)", filter[7]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DetailBiayaViewModel>(x));
                    var totaldiskon = proses.Sum(x => x.Disc);
                    var totalqty = proses.Sum(x => x.Qty);
                    foreach (var x in m)
                    {
                        x.TotalDisk = totaldiskon ?? 0;
                        x.TotalQty = totalqty ?? 0;
                        x.NilaiRumus = (x.Nilai * (decimal)x.Qty) + x.HExt;
                        x.Nilai_View = ((decimal)(x.Nilai ?? 0)).ToMoney();
                        x.Tanggal_View = x.Tanggal == null ? "" : x.Tanggal.Value.ToString("dd/MM/yyyy");
                    }
                    if(m.Count() > 0)
                    {
                        m[0].GrandTotal_View = ((decimal)m.Sum(x => x.NilaiRumus ?? 0)).ToMoney();
                        m[0].GrandTotal = (decimal)m.Sum(x => x.NilaiRumus ?? 0);
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string getTotalDetailBiaya(string noreg)
        {
            try
            {
                decimal total = 0;
                using (var s = new SIMEntities())
                {
                    var l = s.GetDetailRincianBiaya(noreg, (int?)null).ToList();

                    foreach (var x in l)
                    {
                        total += (decimal) x.Nilai * (decimal) x.Qty;
                    }
                }
                return JsonConvert.SerializeObject(new {
                    IsSuccess = true,
                    Data = total.ToMoney(),
                    Message = "Success.."
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== G E T  D A T A  D I A G N O S A
        [HttpPost]
        public string GetDataDiagnosa(string noreg)
        {
            try
            {
                var item = new PembayaranInsertViewModel();
                using (var s = new SIMEntities())
                {
                    var detail = s.Kasir_GetDiagnosa.Where(x => x.NoReg == noreg).ToList();

                    item.Diagnosa_Detail_List = new ListDetail<DiagnosaViewModel>();
                    foreach (var x in detail)
                    {
                        var y = new DiagnosaViewModel()
                        {
                            Descriptions = x.Descriptions,
                            KodeICD = x.KodeICD,
                            Nomor = x.Nomor,
                            NoReg = x.NoReg,
                            SectionName = x.SectionName,
                            TipeDiagnosa = x.TipeDiagnosa
                        };
                        item.Diagnosa_Detail_List.Add(false, y);
                    }
                }
                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== G E T  M E M O
        [HttpPost]
        public string GetMemo(string noreg)
        {
            try
            {
                var item = new PembayaranInsertViewModel();
                using (var s = new SIMEntities())
                {
                    var detail = s.Kasir_GetMemo.Where(x => x.NoReg == noreg).ToList();

                    item.Memo_Detail_List = new ListDetail<MemoViewModel>();
                    foreach (var x in detail)
                    {
                        var y = new MemoViewModel()
                        {
                            NoReg = x.NoReg,
                            SectionName = x.SectionName,
                            Memo = x.Memo,
                            NoUrut = x.NoUrut
                        };
                        item.Memo_Detail_List.Add(false, y);
                    }
                }
                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== G E T  N I L A I  I N V O I C E 
        [HttpGet]
        public string GetNilaiInvoice(string id)
        {
            double result;
            using (var s = new SIMEntities())
            {
                var m = s.Kasir_GetNilaiInvoice(id);
                if (m == null) throw new Exception("Data NIlai Invoice tidak ditemukan");
                result = m.FirstOrDefault() ?? 0;
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result,
            });
        }
        #endregion

        #region ==== U P D A T E  P R O S E S  P A Y M E N T
        [HttpPost]
        public string UpdateProsesPayment(string id, bool value)
        {
            ResultSS result;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == id);
                if (m != null)
                {
                    m.ProsesPayment = value;
                    result = new ResultSS(s.SaveChanges());
                }
                else
                {
                    result = new ResultSS();
                }
            }
            return JsonHelper.JsonMsgEdit(result);
        }
        #endregion

        #region ==== GET JENIS PEMBAYARAN
        [HttpPost]
        public string GetJenispembayaran(string jeniskerjasama)
        {
            using (var s = new SIMEntities())
            {
                var m = s.SIMmJenisKerjasama.FirstOrDefault(x => x.JenisKerjasama == jeniskerjasama);
                if (m == null)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = "11",
                    });
                }
                else
                {
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = m.JenisPembayaran.ToString(),
                    });
                }
            }
        }

        [HttpPost]
        public string getNilaiINAiurBayar(string noreg)
        {
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                if (m != null)
                {
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            iurbayar = m.IURBayar == null ? 0 : m.IURBayar,
                            tarifINA = m.TarifINA == null ? 0 : m.TarifINA
                        },
                    });
                }
                else
                {
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            iurbayar = 0,
                            tarifINA = 0
                        },
                    });
                }
            }
        }
        #endregion
    }
}