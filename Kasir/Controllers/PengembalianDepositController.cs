﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Kasir.Controllers
{
    public class PengembalianDepositController : Controller
    {
        // GET: PengembalianDeposit

        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_ListPengembalianDeposit> proses = s.Kasir_ListPengembalianDeposit;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListDeposit.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_ListDeposit.NOReg)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_ListDeposit.NRM)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_ListDeposit.NamaPasien)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Kasir_ListDeposit.Alamat)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7]))
                    {
                        string nilai = filter[7];
                        proses = proses.Where(x => x.Nilai.ToString().Contains(nilai));
                    }
                    if (!string.IsNullOrEmpty(filter[3]))
                    {
                        proses = proses.Where("TglReg = @0", DateTime.Parse(filter[3]));
                    }
                    var sectionID = "";
                    if (Request.Cookies["SectionID"] != null)
                    {
                        if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                        {
                            sectionID = Request.Cookies["SectionID"].Value;
                        }
                    }
                    if (!string.IsNullOrEmpty(sectionID)) proses = proses.Where($"{nameof(Kasir_ListDeposit.SectionID)}.Contains(@0)", sectionID);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PembayaranDepositViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        x.NilaiDeposit_View = x.NilaiDeposit.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}