﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Kasir.CrystalReports
{
    public class ManualPostingController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    //var m = s.AutoNumber_Kasir_ManualPosting().FirstOrDefault();
                    //if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    var m = "";
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg = null, int pembayaran = 0, string nilaiinvoice11 = "0")
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var id = user.GetUserId();

            var model = new ManualPostingViewModel();
            using (var s = new SIMEntities())
            {
                if (noreg != null)
                {
                    var dtreg = s.Kasir_GetBillingPasien.Where(x => x.NoReg == noreg).FirstOrDefault();
                    if (dtreg != null)
                    {
                        model.NoReg = dtreg.NoReg;
                        model.TglReg = dtreg.TglReg.ToString("yyyy-MM-dd");
                        model.NRM = dtreg.NRM;
                        model.NamaPasien = dtreg.NamaPasien;
                        model.Alamat = dtreg.Alamat;
                        model.Registrasi_JenisKerjasama = dtreg.JenisKerjasama;
                        model.Registrasi_CompanyName = dtreg.Nama_Customer;
                    }
                }
            }
            model.NilaiInvoice_View = (nilaiinvoice11 == null ? "0" : nilaiinvoice11);
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;
            model.Pembayaran = pembayaran;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new ManualPostingViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var userid = User.Identity.GetUserId();
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                                var nobukti = s.AutoNumber_Kasir_ManualPosting().FirstOrDefault();
                                //var nobuktisave = "";
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");
                                //var ceknobukti = s.SIMtrRJ.Where(x => x.NoBukti == nobukti).FirstOrDefault();
                                //if (ceknobukti != null)
                                //{
                                //    nobuktisave = s.AutoNumber_Kasir_ManualPosting().FirstOrDefault();
                                //}
                                //else {
                                //    nobuktisave = nobukti;
                                //}
                                //nobuktisave = 
                                checkDetailKomponen(item);

                                var m = new SIMtrRJ()
                                {
                                    NoBukti = nobukti,
                                    RawatInap = simtrregistrasi.RawatInap,
                                    RegNo = item.NoReg,
                                    JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID,
                                    Tanggal = item.Tanggal,
                                    Jam = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                    SectionID = item.SectionID,
                                    DokterID = item.DokterID,
                                    KdKelas = simtrregistrasi.KdKelas,
                                    UserID = 1103,
                                    NRM = simtrregistrasi.NRM,
                                    Status = "O",
                                    MCU = false,
                                    Batal = false,
                                    AdaObat = false,
                                    Audit = false,
                                    ManualPosting = true,
                                    UserIDWeb = userid
                                };
                                s.SIMtrRJ.Add(m);
                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<ManualPostingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");

                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        //NamaJasa = x.jasanama,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = x.tarif,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon,
                                            Pajak = 0
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {m.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true,
                                    Data = nobukti
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nobukti)
        {
            ManualPostingViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();

                #region ====  G E T  A L L  D A T A
                var section = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                var pasien = s.Kasir_GetBillingPasien_all.Where(x => x.NoReg == m.RegNo).FirstOrDefault();
                #endregion

                model = new ManualPostingViewModel()
                {
                    Tanggal = m.Tanggal,
                    Jam = m.Jam,
                    DokterID = dokter.DokterID ?? dokter.DokterID,
                    NamaDokter = dokter.NamaDOkter ?? dokter.NamaDOkter,
                    SectionID = section.SectionID ?? section.SectionID,
                    Nama_asli = section.SectionName ?? section.SectionName,
                    KdKelas = kelas.KelasID ?? kelas.KelasID,
                    NoReg = pasien.NoReg ?? pasien.NoReg,
                    TglReg = pasien.TglReg.ToString("dd-MM-yyyy"),
                    NRM = pasien.NRM ?? pasien.NRM,
                    NamaPasien = pasien.NamaPasien ?? pasien.NamaPasien,
                    Alamat = pasien.Alamat ?? pasien.Alamat,
                    Registrasi_JenisKerjasama = pasien.JenisKerjasama ?? pasien.JenisKerjasama,
                    Registrasi_CompanyName = pasien.Nama_Customer ?? pasien.Nama_Customer,
                    AdaObat = m.AdaObat,
                    Audit = m.Audit,
                    detail = new List<ManualPostingInsertDetailViewModel>()
                };
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new ManualPostingInsertDetailViewModel()
                    {
                        id = x.Nomor,
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.NoReg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.SectionID,
                        tarif = x.Tarif,
                        detail = new List<ManualPostingInsertDetailJasaViewModel>()
                    };
                    model.Jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new ManualPostingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new ManualPostingViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var nobukti = item.NoBukti;
                                var userid = User.Identity.GetUserId();
                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat di ubah");

                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");
                                if (simtrregistrasi.StatusBayar == "Sudah Bayar") throw new Exception("Pasien sudah melakukan pembayaran");

                                checkDetailKomponen(item);

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.NoBukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.NoBukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                #endregion

                                SIMtrRJ.Tanggal = item.Tanggal;
                                SIMtrRJ.Jam = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay;
                                SIMtrRJ.SectionID = item.SectionID;
                                SIMtrRJ.DokterID = item.DokterID;
                                SIMtrRJ.KdKelas = simtrregistrasi.KdKelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.MCU = false;
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.ManualPosting = true;
                                SIMtrRJ.UserIDWeb = userid;
                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<ManualPostingInsertDetailViewModel>();
                                if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrRJTransaksi()
                                    {
                                        NoBukti = nobukti,
                                        Nomor = nomorbukti,
                                        JasaID = x.jasaid,
                                        KelasID = simtrregistrasi.KdKelas,
                                        Qty = (double)x.qty,
                                        Tarif = x.tarif,
                                        Jam = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                        UserID = 1103,
                                        NRM = simtrregistrasi.NRM,
                                        Waktu = item.Tanggal.Value.Date + item.Jam.Value.TimeOfDay,
                                        PasienKTP = simtrregistrasi.PasienKTP,
                                        DokterID = x.dokter,
                                        Disc = (double)x.diskon
                                    };
                                    s.SIMtrRJTransaksi.Add(i);
                                    foreach (var y in x.detail)
                                    {
                                        var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                        var j = new SIMtrRJTransaksiDetail()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KomponenID = y.komponenid,
                                            Harga = y.harga,
                                            KelompokAkun = komponenbiaya.KelompokAkun,
                                            PostinganKe = "GL",
                                            HargaOrig = y.harga,
                                            Disc = (double)y.diskon
                                        };
                                        s.SIMtrRJTransaksiDetail.Add(j);
                                    }
                                    nomorbukti++;
                                }

                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {item.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== D E T A I L
        [HttpGet]
        public ActionResult Detail(string nobukti, int pembayaran = 0)
        {
            ManualPostingViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var r = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == m.RegNo);
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();

                #region ====  G E T  A L L  D A T A
                var section = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                var pasien = s.Kasir_GetBillingPasien_all.Where(x => x.NoReg == m.RegNo).FirstOrDefault();
                #endregion


                var nilai = s.Kasir_GetNilaiInvoice(m.RegNo);
                var nilaiinv = "";
                if (nilai != null)
                {
                    decimal nilaiInvoice = Convert.ToDecimal(nilai.FirstOrDefault());
                    nilaiinv = IConverter.ToMoney(nilaiInvoice);
                }
                model = new ManualPostingViewModel()
                {
                    NilaiInvoice_View = nilaiinv,
                    Tanggal = m.Tanggal,
                    Jam = m.Jam,
                    DokterID = dokter.NamaDOkter,
                    SectionID = section.SectionName ?? section.SectionName,
                    KdKelas = kelas.KelasID ?? kelas.KelasID,
                    NoReg = pasien.NoReg ?? pasien.NoReg,
                    TglReg = pasien.TglReg.ToString("dd-MM-yyyy"),
                    NRM = pasien.NRM ?? pasien.NRM,
                    NamaPasien = pasien.NamaPasien ?? pasien.NamaPasien,
                    Alamat = pasien.Alamat ?? pasien.Alamat,
                    Registrasi_JenisKerjasama = pasien.JenisKerjasama ?? pasien.JenisKerjasama,
                    Registrasi_CompanyName = pasien.Nama_Customer ?? pasien.Nama_Customer,
                    AdaObat = m.AdaObat,
                    Audit = m.Audit,
                    Pulang = (r.StatusBayar == "Sudah Bayar") ? 1 : 0,
                    detail = new List<ManualPostingInsertDetailViewModel>()
                };

                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new ManualPostingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.NoReg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.SectionID,
                        tarif = x.Tarif,
                        detail = new List<ManualPostingInsertDetailJasaViewModel>()
                    };
                    model.Jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new ManualPostingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }
                decimal hasildetail = model.NilaiInvoice_View.ToDecimal() - model.Jumlah;
                decimal calc = model.Jumlah - hasildetail;
                model.TotalNilai = calc;
            }

            model.Pembayaran = pembayaran;
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }
        #endregion

        #region === T A B L E
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_ListManualPosting> proses = s.Kasir_ListManualPosting;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }
                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.Registrasi_NoReg)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.NamaDokter)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Kasir_ListManualPosting.Registrasi_JenisKerjasama)}.Contains(@0)", filter[5]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ManualPostingViewModel>(x));
                    foreach (var x in m)
                    {
                        var r = s.SIMtrRegistrasi.FirstOrDefault(yx => yx.NoReg == x.Registrasi_NoReg);
                        if (r != null)
                        {
                            x.Pulang = (r.StatusBayar == "Sudah Bayar") ? 1 : 0;
                        }

                        x.Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<mDokter> proses = s.mDokter;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(mDokter.Active)}=@0", true);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListKelas(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMmKelas> proses = s.SIMmKelas;
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmKelas.KelasID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmKelas.NamaKelas)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(SIMmKelas.Active)}=@0", true);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmKelas>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string GetTarif(string jasa, string dokter, string section, string noreg, int? kategorioperasi, int? cito, bool tarifmanual)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var p = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var c = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                    var h = s.GetTarifBiaya_Global(jasa, dokter, cito, kategorioperasi, section, noreg, "1").FirstOrDefault();
                    if (h == null) throw new Exception("GetTarifBiaya_Global tidak ditemukan");
                    var d = s.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, p.JenisKerjasamaID, "1", c.UnitBisnisID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d,
                        Tarif = h.Harga ?? 0,
                        tarifmanual = tarifmanual
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListJasa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Pelayanan_LookupTarif_Result> proses = s.Pelayanan_LookupTarif(filter[10] == "True" ? "" : Request.Cookies["SectionID"].Value);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<Pelayanan_LookupTarif_Result>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id)
        {
            try
            {
                ResultSS result;

                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == id);
                    m.Batal = true;
                    result = new ResultSS(1, s.SaveChanges());
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"SIMtrRJ Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        private bool checkDetailKomponen(ManualPostingViewModel item)
        {

            int error = 0;
            if (item.detail == null) item.detail = new List<ManualPostingInsertDetailViewModel>();
            if (item.detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
            foreach (var x in item.detail)
            {
                decimal sumHarga = 0;
                if (x.detail == null) throw new Exception("Detail komponen masih kosong");

                foreach (var y in x.detail)
                {
                    sumHarga += y.harga;
                }
                error += (x.tarif == sumHarga) ? 0 : 1;
            }
            if (error > 0)
            {
                throw new Exception("Harga detail tidak sama dengan harga header");
            }
            return true;
        }

        #region ===== C R E A T E  J A S A
        [HttpGet]
        [ActionName("CreateJasa")]
        public ActionResult CreateJasa_Get(string noreg = null, int pembayaran = 0)
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var id = user.GetUserId();

            var model = new ManualPostingViewModel();
            using (var s = new SIMEntities())
            {
                if (noreg != null)
                {
                    var dtreg = s.Kasir_GetBillingPasien.Where(x => x.NoReg == noreg).FirstOrDefault();
                    if (dtreg != null)
                    {
                        model.NoReg = dtreg.NoReg;
                        model.TglReg = dtreg.TglReg.ToString("yyyy-MM-dd");
                        model.NRM = dtreg.NRM;
                        model.NamaPasien = dtreg.NamaPasien;
                        model.Alamat = dtreg.Alamat;
                        model.Registrasi_JenisKerjasama = dtreg.JenisKerjasama;
                        model.Registrasi_CompanyName = dtreg.Nama_Customer;
                    }
                }
            }
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;
            model.Pembayaran = pembayaran;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("CreateJasa")]
        public string CreateJasa_Post()
        {
            try
            {
                var item = new ManualPostingViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                item.HargaJasa = item.HargaJasa_View.ToDecimal();
                                var m = s.SIM_InsertJasaManual(item.NamaJasa, item.HargaJasa);
                                s.SaveChanges();

                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Jasa Manual Create {item.NamaJasa}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true,
                                    Data = 0
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        [HttpPost]
        public string LastResep(string idjasa, string nobukti)
        {
            try
            {
                ManualPostingViewModel model;
                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                    var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                    var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();

                    #region ====  G E T  A L L  D A T A
                    var section = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionID);
                    var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                    var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                    var pasien = s.Kasir_GetBillingPasien_all.Where(x => x.NoReg == m.RegNo).FirstOrDefault();
                    #endregion

                    model = new ManualPostingViewModel()
                    {
                        Tanggal = m.Tanggal,
                        Jam = m.Jam,
                        DokterID = dokter.DokterID ?? dokter.DokterID,
                        NamaDokter = dokter.NamaDOkter ?? dokter.NamaDOkter,
                        SectionID = section.SectionID ?? section.SectionID,
                        Nama_asli = section.SectionName ?? section.SectionName,
                        KdKelas = kelas.KelasID ?? kelas.KelasID,
                        NoReg = pasien.NoReg ?? pasien.NoReg,
                        TglReg = pasien.TglReg.ToString("dd-MM-yyyy"),
                        NRM = pasien.NRM ?? pasien.NRM,
                        NamaPasien = pasien.NamaPasien ?? pasien.NamaPasien,
                        Alamat = pasien.Alamat ?? pasien.Alamat,
                        Registrasi_JenisKerjasama = pasien.JenisKerjasama ?? pasien.JenisKerjasama,
                        Registrasi_CompanyName = pasien.Nama_Customer ?? pasien.Nama_Customer,
                        AdaObat = m.AdaObat,
                        Audit = m.Audit,
                        detail = new List<ManualPostingInsertDetailViewModel>(),
                        detailtarif = new List<ManualPostingInsertDetailJasaViewModel>()
                    };
                    foreach (var x in d)
                    {
                        dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                        var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                        var detail = new ManualPostingInsertDetailViewModel()
                        {
                            id = x.Nomor,
                            diskon = (decimal)(x.Disc),
                            dokter = x.DokterID,
                            namadokter = dokter == null ? "" : dokter.NamaDOkter,
                            jasaid = x.JasaID,
                            jasanama = jasa.JasaName,
                            noreg = model.NoReg,
                            qty = (decimal)(x.Qty ?? 0),
                            section = model.SectionID,
                            tarif = x.Tarif,
                            detail = new List<ManualPostingInsertDetailJasaViewModel>()
                        };
                        model.Jumlah += detail.qty * (detail.tarif - detail.diskon);
                        foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                        {
                            var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                            var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                            detail.detail.Add(new ManualPostingInsertDetailJasaViewModel()
                            {
                                diskon = (decimal)y.Disc,
                                harga = y.Harga,
                                komponenid = y.KomponenID,
                                diskonnilai = diskonnilai,
                                komponennama = komponen.KomponenName,
                                jumlah = y.Harga = diskonnilai
                            });
                        }
                        model.detail.Add(detail);
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = model
                        //Data = d.ConvertAll(x => new
                        //{
                        ////Kode = x.KodePaket,
                        ////NamaPaket = x.NamaPaket,
                        //Kode_Barang = x.JasaID,
                        //    Barang_ID = x.KomponenID,
                        //    Nama_Barang = x.Nama_Barang,
                        //    AturanPakai = x.AturanPakai,
                        //    Satuan_Stok = x.Satuan,
                        //    Qty = x.Qty,
                        //    Dosis = x.Dosis,
                        //    KetRacik = x.KetRacik,
                        //    StatusRacikan = x.StatusRacikan,
                        //    JmlRacikan = x.JmlRacikan,
                        //    //Harga_Satuan = x.Harga_Satuan ?? 0,
                        //    //Harga_Jual_View = (x.Harga_Satuan ?? 0).ToMoney(),
                        //    //Jumlah_View = ((x.Harga ?? 0) * (decimal)(x.Qty)).ToMoney(),
                        //    //Qty_Stok = x.Qty ?? 0
                        //})
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #region ==== G E T  N I L A I  I N V O I C E 
        [HttpGet]
        public string GetNilaiInvoice(string id)
        {
            double result;
            using (var s = new SIMEntities())
            {
                var m = s.Kasir_GetNilaiInvoice(id);
                if (m == null) throw new Exception("Data NIlai Invoice tidak ditemukan");
                result = m.FirstOrDefault() ?? 0;
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = result,
            });
        }
        #endregion

        #region ===== R I N C I A N - B I A Y A 
        [HttpGet]
        public ActionResult DetailBiaya(string noreg)
        {
            ViewBag.NoReg = noreg;
            return PartialView();
        }

        [HttpPost]
        public string ListDetailBiaya(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<GetDetailRincianBiaya_Result> proses = s.GetDetailRincianBiaya(filter[11], (int?)null);
                    if (!string.IsNullOrEmpty(filter[12]))
                        proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.KategoriBiaya)}=@0", filter[12]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.NoBukti)}.Contains(@0)", filter[0]);
                    if (IFilter.F_DateTime(filter[1]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Tanggal)}=@0", IFilter.F_DateTime(filter[1]));
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.JenisBiaya)}.Contains(@0)", filter[2]);
                    if (IFilter.F_Decimal(filter[3]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Qty)}=@0", (double)IFilter.F_Decimal(filter[3]));
                    if (IFilter.F_Decimal(filter[4]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Nilai)}=@0", IFilter.F_Decimal(filter[4]));
                    if (IFilter.F_Decimal(filter[5]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Disc)}=@0", (double)IFilter.F_Decimal(filter[5]));
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.SectionName)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.DokterName)}.Contains(@0)", filter[7]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DetailBiayaViewModel>(x));
                    var totaldiskon = proses.Sum(x => x.Disc);
                    var totalqty = proses.Sum(x => x.Qty);
                    foreach (var x in m)
                    {
                        x.TotalDisk = totaldiskon ?? 0;
                        x.TotalQty = totalqty ?? 0;
                        x.NilaiRumus = (x.Nilai * (decimal)x.Qty) + x.HExt;
                        x.Nilai_View = ((decimal)(x.Nilai ?? 0)).ToMoney();
                        x.Tanggal_View = x.Tanggal == null ? "" : x.Tanggal.Value.ToString("dd/MM/yyyy");
                    }
                    if (m.Count() > 0)
                    {
                        m[0].GrandTotal_View = ((decimal)m.Sum(x => x.NilaiRumus ?? 0)).ToMoney();
                        m[0].GrandTotal = (decimal)m.Sum(x => x.NilaiRumus ?? 0);
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

    }
}