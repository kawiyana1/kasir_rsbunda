﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
namespace Kasir.Controllers
{
    public class MutasiKasBankController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<SIMtrKasirMutasiKeBank> proses = s.SIMtrKasirMutasiKeBank;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }
                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(SIMtrKasirMutasiKeBank.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(SIMtrKasirMutasiKeBank.NamaBank)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3]))
                    {
                        string nilai = filter[3];
                        proses = proses.Where(x => x.Total.ToString().Contains(nilai));

                    }
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(SIMtrKasirMutasiKeBank.Shift)}.Contains(@0)", filter[4]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<MutasiKasBankViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Jam_View = x.Jam.ToString("HH:mm");
                        x.Total_View = x.Total.Value.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoNumber_Kasir_SIMtrKasirMutasiKeBank().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var username = user.GetUserId();

            var model = new MutasiKasBankViewModel();
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;
            model.UserIDWeb = username;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new MutasiKasBankViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        #region ==== V A L I D A S I

                        //if (item.l_TipePembayaran == null) return JsonHelper.JsonMsgInfo("Tipe Bayar tidak boleh kosong.");
                        //if (item.l_TipeTransaksi == null) return JsonHelper.JsonMsgInfo("Transaksi tidak boleh kosong.");
                        //if (item.Keterangan == null) return JsonHelper.JsonMsgInfo("Keterangan tidak boleh kosong.");
                        //if (item.Nilai_View == null) return JsonHelper.JsonMsgInfo("Nilai Penerimaan tidak boleh kosong.");
                        #endregion

                        //var m = IConverter.Cast<SIMtrPenerimaanNonPasien>(item);
                        //var transaction = s.KASIR_mTypeTransaksiKasir.FirstOrDefault(xx => xx.NamaType == item.l_TipeTransaksi);
                        //if (transaction != null)
                        //{
                        //    m.AkunID = transaction.akun_id;
                        //    m.JenisTransaksi = item.l_TipeTransaksi;
                        //}
                        //var paymen = s.KASIR_mTypeBayarKasir.FirstOrDefault(yy => yy.NamaJenisBayar == item.l_TipePembayaran);
                        //if (paymen != null)
                        //{
                        //    m.TipeTransaksi = paymen.Tipe;
                        //    m.MerchanID = paymen.MerchanID;
                        //    m.AkunBayarID = paymen.Akun_ID;
                        //}
                        //m.Nilai = item.Nilai_View.ToDecimal();
                        //m.Batal = false;
                        //if (Request.Cookies["Shift"] != null)
                        //{
                        //    if (!string.IsNullOrEmpty(Request.Cookies["Shift"].Value))
                        //    {
                        //        var shift = Request.Cookies["Shift"].Value;
                        //        m.Shift = shift;
                        //    }
                        //}
                        //if (Request.Cookies["SectionID"] != null)
                        //{
                        //    if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                        //    {
                        //        m.SectionID = Request.Cookies["SectionID"].Value;
                        //    }
                        //}
                        //m.UserID = 490;
                        //m.UserIDWeb = User.Identity.GetUserId();
                        //m.UsernameWeb = User.Identity.GetUserName();

                        //m.Tipe = "PNP";
                        //m.TglHonor = null;
                        //m.HonorBruto = null;
                        //m.Closing = false;
                        //m.Posting = false;
                        //s.SIMtrPenerimaanNonPasien.Add(m);
                        result = new ResultSS("");

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrPenerimaanNonPasien Create "
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== U P D A T E
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            var model = new MutasiKasBankViewModel();

            using (var s = new SIMEntities())
            {
                var mutasi = s.SIMtrKasirMutasiKeBank.FirstOrDefault(x => x.NoBukti == id);
                if (mutasi == null) HttpNotFound();

                model = IConverter.Cast<MutasiKasBankViewModel>(mutasi);
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new PenerimaanNonInvoiceViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        #region ==== V A L I D A S I

                        if (item.l_TipePembayaran == null) return JsonHelper.JsonMsgInfo("Tipe Bayar tidak boleh kosong.");
                        if (item.l_TipeTransaksi == null) return JsonHelper.JsonMsgInfo("Transaksi tidak boleh kosong.");
                        if (item.Keterangan == null) return JsonHelper.JsonMsgInfo("Keterangan tidak boleh kosong.");
                        if (item.Nilai_View == null) return JsonHelper.JsonMsgInfo("Nilai Penerimaan tidak boleh kosong.");
                        #endregion

                        var m = s.SIMtrPenerimaanNonPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        if (m == null) HttpNotFound();
                        m.DIterimaDari = item.DIterimaDari;
                        m.Keterangan = item.Keterangan;

                        var transaction = s.KASIR_mTypeTransaksiKasir.FirstOrDefault(xx => xx.NamaType == item.l_TipeTransaksi);
                        if (transaction != null)
                        {
                            m.AkunID = transaction.akun_id;
                            m.JenisTransaksi = item.l_TipeTransaksi;
                        }
                        var paymen = s.KASIR_mTypeBayarKasir.FirstOrDefault(yy => yy.NamaJenisBayar == item.l_TipePembayaran);
                        if (paymen != null)
                        {
                            m.TipeTransaksi = paymen.Tipe;
                            m.MerchanID = paymen.MerchanID;
                            m.AkunBayarID = paymen.Akun_ID;
                        }
                        m.Nilai = item.Nilai_View.ToDecimal();
                        m.Batal = false;
                        if (Request.Cookies["Shift"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["Shift"].Value))
                            {
                                var shift = Request.Cookies["Shift"].Value;
                                m.Shift = shift;
                            }
                        }
                        if (Request.Cookies["SectionID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                            {
                                m.SectionID = Request.Cookies["SectionID"].Value;
                            }
                        }
                        m.UserID = 490;
                        m.UserIDWeb = User.Identity.GetUserId();
                        m.UsernameWeb = User.Identity.GetUserName();
                        m.Tipe = "PNP";

                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrPenerimaanNonPasien Create {m.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id)
        {
            try
            {
                ResultSS result;

                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrPenerimaanNonPasien.FirstOrDefault(x => x.NoBukti == id);
                    m.Batal = true;
                    result = new ResultSS(1, s.SaveChanges());
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"SIMtrPenerimaanNonPasien Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        [HttpGet]
        public string get_detailkasir(string nobukti, DateTime fromdate, DateTime todate, bool all , int val)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var model = new List<MutasiKasBankDetailViewModel>();
                    if (!all)
                    {
                        if(val == 0)
                        {
                            var m = s.VW_trKasirTransaksiDetail.Where(x => x.MutasiKasKeBank == val && (x.Tanggal >= fromdate && x.Tanggal <= todate)).ToList();
                            foreach (var x in m)
                            {
                                model.Add(new MutasiKasBankDetailViewModel()
                                {
                                    Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"),
                                    NoBukti = x.NoBukti,
                                    Keterangan = x.Keterangan,
                                    Total_View = Convert.ToDecimal(x.NilaiBayar).ToMoney()
                                });
                            }
                        }
                        else
                        {
                            var m = s.SIMtrKasirMutasiKeBankDetail.Where(x => x.NoBukti == nobukti).ToList();
                            foreach (var y in m)
                            {
                                var x = s.VW_trKasirTransaksiDetail.FirstOrDefault(e => e.NoBukti == y.NoBuktiReferensi);
                                model.Add(new MutasiKasBankDetailViewModel()
                                {
                                    Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"),
                                    NoBukti = x.NoBukti,
                                    Keterangan = x.Keterangan,
                                    Total_View = Convert.ToDecimal(x.NilaiBayar).ToMoney()
                                });
                            }
                        }
                    }
                    else
                    {
                        if (val == 0)
                        {
                            var m = s.VW_trKasirTransaksiDetail.Where(x => x.MutasiKasKeBank == val).ToList();
                            foreach (var x in m)
                            {
                                model.Add(new MutasiKasBankDetailViewModel()
                                {
                                    Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"),
                                    NoBukti = x.NoBukti,
                                    Keterangan = x.Keterangan,
                                    Total_View = Convert.ToDecimal(x.NilaiBayar).ToMoney()
                                });
                            }
                        }
                        else
                        {
                            var m = s.SIMtrKasirMutasiKeBankDetail.Where(x => x.NoBukti == nobukti).ToList();
                            foreach (var y in m)
                            {
                                var x = s.VW_trKasirTransaksiDetail.FirstOrDefault(e => e.NoBukti == y.NoBuktiReferensi);
                                model.Add(new MutasiKasBankDetailViewModel()
                                {
                                    Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy"),
                                    NoBukti = x.NoBukti,
                                    Keterangan = x.Keterangan,
                                    Total_View = Convert.ToDecimal(x.NilaiBayar).ToMoney()
                                });
                            }
                        }
                    }
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            data = model,
                            countAll = model.Count()
                        },
                        Message = ""
                    });
                }

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string update_kasir(List<UpdateKasir> id, MutasiKasBankViewModel header)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = new SIMtrKasirMutasiKeBank();
                    m.NoBukti = header.NoBukti;
                    m.Tanggal = header.Tanggal;
                    m.Jam = header.Jam;
                    m.IDBank = header.IDBank;
                    m.NamaBank = header.NamaBank;
                    m.AkunIDBank = header.AkunIDBank;
                    m.SectionID = Request.Cookies["SectionID"].Value;
                    m.Batal = false;
                    m.Audit = false;
                    m.UserIDWeb = User.Identity.GetUserId();
                    m.Shift = Request.Cookies["Shift"].Value;

                    decimal get_total = 0;
                    foreach (var x in id)
                    {
                        if (x.val == true)
                        {
                            var detail = s.VW_trKasirTransaksiDetail.FirstOrDefault(o => o.NoBukti == x.NoBukti);
                            if (detail != null)
                            {
                                var SIMtrKasir = s.SIMtrKasirDetail.FirstOrDefault(e => e.NoBukti == x.NoBukti && e.IDBayar == 13);
                                SIMtrKasir.MutasiKasKeBank = x.val;

                                var d = new SIMtrKasirMutasiKeBankDetail();
                                d.NoBukti = header.NoBukti;
                                d.NoBuktiReferensi = detail.NoBukti;
                                d.IdJenisBayar = detail.IDBayar.ToString();
                                d.NamaJenisBayar = detail.Description;
                                d.NilaiBayar = detail.NilaiBayar;
                                s.SIMtrKasirMutasiKeBankDetail.Add(d);

                                get_total += detail.NilaiBayar;
                            }
                        }
                        else
                        {
                            var detail = s.SIMtrKasirMutasiKeBankDetail.FirstOrDefault(o => o.NoBuktiReferensi == x.NoBukti);
                            if (detail != null)
                            {
                                var SIMtrKasir = s.SIMtrKasirDetail.FirstOrDefault(e => e.NoBukti == x.NoBukti && e.IDBayar == 13);
                                SIMtrKasir.MutasiKasKeBank = x.val;

                                get_total -= detail.NilaiBayar.Value;

                                s.SIMtrKasirMutasiKeBankDetail.Remove(detail);
                            }
                        }

                    }

                    var mutasi = s.SIMtrKasirMutasiKeBank.FirstOrDefault(e => e.NoBukti == m.NoBukti);
                    if (mutasi == null)
                    {
                        m.Total = get_total;
                        s.SIMtrKasirMutasiKeBank.Add(m);
                    }
                    else
                    {
                        mutasi.Total = mutasi.Total + get_total;
                    }
                    s.SaveChanges();

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = "",
                        Message = ""
                    });
                }

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}