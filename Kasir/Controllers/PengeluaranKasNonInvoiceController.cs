﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Kasir.Entities;
using Kasir.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Kasir.Controllers
{
    public class PengeluaranKasNonInvoiceController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Kasir_ListPengeluaranKasNonInvoice> proses = s.Kasir_ListPengeluaranKasNonInvoice;
                    if (filter[14] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[12]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[12]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[13]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[13]));
                        }
                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Kasir_ListPengeluaranKasNonInvoice.NoBukti)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Kasir_ListPengeluaranKasNonInvoice.Keterangan)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3]))
                    {
                        string nilai = filter[3];
                        proses = proses.Where(x => x.Nilai.ToString().Contains(nilai));

                    }
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Kasir_ListPengeluaranKasNonInvoice.Akun_Akun_Name)}.Contains(@0)", filter[4]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PengeluaranKasNonInvoiceViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.Jam_View = x.Jam.ToString("HH:mm");
                        x.Nilai_View = x.Nilai.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoNumber_Kasir_PengeluaranKasNonInvoice().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            var user = (System.Security.Claims.ClaimsIdentity)User.Identity;
            var username = user.GetUserName();

            var model = new PengeluaranKasNonInvoiceViewModel();
            model.Tanggal = DateTime.Today;
            model.Jam = DateTime.Now;
            model.UsernameWeb = username;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new PengeluaranKasNonInvoiceViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        #region ==== V A L I D A S I
                        if (item.Keterangan == null) return JsonHelper.JsonMsgInfo("Keterangan tidak boleh kosong.");
                        if (item.l_TipeTransaksi == null) return JsonHelper.JsonMsgInfo("Transaksi tidak boleh kosong.");
                        if (item.l_TipePembayaran == null) return JsonHelper.JsonMsgInfo("Pembayaran tidak boleh kosong.");
                        if (item.Nilai_View == null || item.Nilai_View == "0") return JsonHelper.JsonMsgInfo("Nilai Penerimaan tidak boleh kosong.");
                        #endregion

                        var m = IConverter.Cast<SIMtrPenerimaanNonPasien>(item);
                        var transaction = s.KASIR_mTypeTransaksiKasir.FirstOrDefault(x => x.NamaType == item.l_TipeTransaksi);
                        if(transaction != null)
                        {
                            m.AkunID = transaction.akun_id;
                            m.JenisTransaksi = item.l_TipeTransaksi;
                        }
                        var paymen = s.KASIR_mTypeBayarKasir.FirstOrDefault(yy => yy.NamaJenisBayar == item.l_TipePembayaran);
                        if (paymen != null)
                        {
                            m.TipeTransaksi = paymen.Tipe;
                            m.MerchanID = paymen.MerchanID;
                            m.AkunBayarID = paymen.Akun_ID;
                        }
                        m.Nilai = item.Nilai_View.ToDecimal();
                        m.Batal = false;
                        if (Request.Cookies["Shift"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["Shift"].Value))
                            {
                                var shift = Request.Cookies["Shift"].Value;
                                m.Shift = shift;
                            }
                        }
                        if (Request.Cookies["SectionID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                            {
                                m.SectionID = Request.Cookies["SectionID"].Value;
                            }
                        }
                        m.UserID = 490;
                        m.UserIDWeb = User.Identity.GetUserId();
                        m.UsernameWeb = User.Identity.GetUserName();

                        m.Tipe = "KNI";
                        m.TglHonor = null;
                        m.HonorBruto = null;
                        m.Closing = false;
                        m.Posting = false;
                        s.SIMtrPenerimaanNonPasien.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrPenerimaanNonPasien Create {m.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== U P D A T E
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            var model = new PengeluaranKasNonInvoiceViewModel();
            using (var s = new SIMEntities())
            {
                var e = s.SIMtrPenerimaanNonPasien.FirstOrDefault(x => x.NoBukti == id);
                if (e == null) return HttpNotFound();
                model = IConverter.Cast<PengeluaranKasNonInvoiceViewModel>(e);
                var t = s.KASIR_mTypeTransaksiKasir.FirstOrDefault(x => x.akun_id == e.AkunID && x.NamaType == e.JenisTransaksi);
                if (t != null)
                {
                    model.l_TipeTransaksi = t.NamaType;
                }
                var p = s.KASIR_mTypeBayarKasir.FirstOrDefault(x => x.Tipe == e.TipeTransaksi && x.Akun_ID == e.AkunBayarID && x.MerchanID == e.MerchanID);
                if (p != null)
                {
                    model.l_TipePembayaran = p.NamaJenisBayar;
                }
                model.Nilai_View = e.Nilai.ToMoney();
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new PengeluaranKasNonInvoiceViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        #region ==== V A L I D A S I

                        if (item.l_TipePembayaran == null) return JsonHelper.JsonMsgInfo("Tipe Bayar tidak boleh kosong.");
                        if (item.l_TipeTransaksi == null) return JsonHelper.JsonMsgInfo("Transaksi tidak boleh kosong.");
                        if (item.Keterangan == null) return JsonHelper.JsonMsgInfo("Keterangan tidak boleh kosong.");
                        if (item.Nilai_View == null) return JsonHelper.JsonMsgInfo("Nilai Penerimaan tidak boleh kosong.");
                        #endregion

                        var m = s.SIMtrPenerimaanNonPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                        if (m == null) HttpNotFound();
                        m.Keterangan = item.Keterangan;

                        var transaction = s.KASIR_mTypeTransaksiKasir.FirstOrDefault(xx => xx.NamaType == item.l_TipeTransaksi);
                        if (transaction != null)
                        {
                            m.AkunID = transaction.akun_id;
                            m.JenisTransaksi = item.l_TipeTransaksi;
                        }
                        var paymen = s.KASIR_mTypeBayarKasir.FirstOrDefault(yy => yy.NamaJenisBayar == item.l_TipePembayaran);
                        if (paymen != null)
                        {
                            m.TipeTransaksi = paymen.Tipe;
                            m.MerchanID = paymen.MerchanID;
                            m.AkunBayarID = paymen.Akun_ID;
                        }
                        m.Nilai = item.Nilai_View.ToDecimal();
                        m.Batal = false;
                        if (Request.Cookies["Shift"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["Shift"].Value))
                            {
                                var shift = Request.Cookies["Shift"].Value;
                                m.Shift = shift;
                            }
                        }
                        if (Request.Cookies["SectionID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Request.Cookies["SectionID"].Value))
                            {
                                m.SectionID = Request.Cookies["SectionID"].Value;
                            }
                        }
                        m.UserID = 490;
                        m.UserIDWeb = User.Identity.GetUserId();
                        m.UsernameWeb = User.Identity.GetUserName();
                        m.Tipe = "KNI";

                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SIMtrPenerimaanNonPasien Create {m.NoBukti}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id)
        {
            try
            {
                ResultSS result;

                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrPenerimaanNonPasien.FirstOrDefault(x => x.NoBukti == id);
                    m.Batal = true;
                    result = new ResultSS(1, s.SaveChanges());
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"SIMtrPenerimaanNonPasien Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}