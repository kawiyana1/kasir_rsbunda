
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Kasir.Entities
{

using System;
    using System.Collections.Generic;
    
public partial class Pelayanan_GetObatNonPasien
{

    public string Kode_Barang { get; set; }

    public string Nama_Barang { get; set; }

    public string Nama_Kategori { get; set; }

    public string Satuan_Stok { get; set; }

    public Nullable<decimal> Harga_Jual { get; set; }

    public short Lokasi_ID { get; set; }

    public string SectionID { get; set; }

    public int Barang_ID { get; set; }

    public Nullable<short> KodeSatuan { get; set; }

    public double Qty_Stok { get; set; }

}

}
