
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Kasir.Entities
{

using System;
    using System.Collections.Generic;
    
public partial class Kasir_GetObatBebas
{

    public string NoBukti { get; set; }

    public System.DateTime Tanggal { get; set; }

    public string DokterID { get; set; }

    public string NamaDOkter { get; set; }

    public string TipePasien { get; set; }

    public string NamaPasien { get; set; }

    public Nullable<decimal> BiayaRacik { get; set; }

    public Nullable<double> Disc_Persen { get; set; }

    public Nullable<decimal> Total { get; set; }

    public string SectionID { get; set; }

    public string DeskripsiObat { get; set; }

    public Nullable<bool> ClosePayment { get; set; }

}

}
