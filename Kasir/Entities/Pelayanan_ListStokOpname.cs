
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Kasir.Entities
{

using System;
    using System.Collections.Generic;
    
public partial class Pelayanan_ListStokOpname
{

    public string No_Bukti { get; set; }

    public System.DateTime Tgl_Opname { get; set; }

    public string KelompokJenis { get; set; }

    public short Lokasi_ID { get; set; }

    public string SectionID { get; set; }

    public string SectionName { get; set; }

    public bool Status_Batal { get; set; }

    public bool Posted { get; set; }

}

}
