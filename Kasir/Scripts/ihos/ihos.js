﻿function togglecheckbox(id) {
    var i = $(id);
    if (i.css('display') === 'none') {
        i.show(100);
    } else {
        i.hide(100);
    }
}

function checkall(_this, index) {
    index = index || 1;
    var j_this = $(_this);
    j_this.closest('table').find('tbody tr td:nth-child(' + index + ') input').prop('checked', _this.checked);
}

function modallookuodismiss(id) {
    var j_modal = $(id);
    if (j_modal.data('header-ajax').toLowerCase() === "true") {
        j_modal.on('hidden.bs.modal', function () {
            if ($('.modal.in').length > 0) $('body').addClass('modal-open');
        });
    }
    j_modal.modal('hide');
}

function disabletarget(value, target) {
    if (value) {
        $(target).attr('disabled', 'disabled');
    } else {
        $(target).removeAttr('disabled');
    }
}

function removetr(_this) {
    var reset = false;
    var j_tr = $(_this).closest('tr');
    if (j_tr.next().length === 0 && j_tr.find('[id$=0__Remove]').length > 0)
        reset = true;
    if (reset) {
        j_tr.closest('.detail').data('index-detail', 0);
        j_tr.remove();
    } else {
        j_tr.find('[id$=__Remove]').first().prop('checked', true).closest('td').addClass('state');
        j_tr.find('td:not(.state)').remove();
        j_tr.hide();
    }
}

function removetrselected(t, index) {
    index = index || 1;
    var target = $(t).find('tbody tr td:nth-child(' + index + ') input');
    $.each(target, function (index, value) {
        var j_val = $(value);
        if (j_val.prop('checked')) {
            var j_tr = j_val.closest('tr');
            j_tr.find('[id$=__Remove]').first().prop('checked', true).closest('td').addClass('state');
            j_tr.find('td:not(.state)').remove();
            j_tr.hide();
        }
    });
}

function loadingform(_this) {
    if ($(_this).valid()) $('#_loading').css('display', '');
}

function loading() {
    $('#_loading').css('display', '');
}

function autoid(_this, url, target) {
    var j_this = $(_this);
    myajax({
        url: url,
        method: 'GET',
        dataType: null,
        success: function (data) {
            try {
                var json = JSON.parse(data);
                if (json.IsSuccess) j_this.closest('.wall').find(target).val(json.Message);
                else alertpopup(json.Message, json.Status);
            } catch (ex) { alertpopup(data); }
        },
        beforeSend: function () {
            j_this.attr('disabled', 'disabled');
            j_this.closest('.wall').find(target).attr('disabled', 'disabled');
        },
        complete: function () {
            j_this.removeAttr('disabled');
            j_this.closest('.wall').find(target).removeAttr('disabled');
        }

    });
}

function alertpopup(text, status) {
    var newText = '';
    text = text || 'danger';
    if (status === 'danger') {
        if (text.length > 1000) text = 'Internal Server Error';
    }
    $.each(text.split("|"), function (index, value) {
        newText += "<li>" + value + "</li>";
    });
    var icon = status === 'success' ? 'icon-checkmark3' :
        status === 'danger' ? 'icon-blocked' :
            status === 'info' ? 'icon-info22' :
                status === 'warning' ? 'icon-warning22' :
                    '';
    var myStack = { "dir1": "down", "dir2": "right", "push": "top" };
    new PNotify({
        title: status,
        icon: icon,
        text: newText,
        type: status,
        stack: myStack
    });
}

function myajax(option) {
    var setting = {
        'url': '',
        'method': 'POST',
        'dataType': 'json',
        'beforeSend': function () {
            $('#_loading').css('display', '');
        },
        'success': '',
        'data': '',
        'error': function (xhr, status, error) {
            alertpopup(xhr.responseText || 'Server Error', 'danger');
        },
        'complete': function () {
            $('#_loading').css('display', 'none');
        }
    };
    $.extend(setting, option);
    try {
        $.ajax({
            url: setting.url,
            method: setting.method,
            data: setting.data,
            dataType: setting.dataType,
            beforeSend: setting.beforeSend,
            success: setting.success,
            error: setting.error,
            complete: setting.complete
        });
    } catch (ex) {
        $('#_loading').css('display', 'none');
        alertpopup(ex.message, 'danger');
    }
}

function oncompletecreate(xhr, formname, isajax, x_this) {
    try {
        var result = JSON.parse(xhr.responseText);
        alertpopup(result.Message, result.Status);
        if (result.IsSuccess) {
            if (isajax.toUpperCase() === "TRUE") {
                var j_modal = $(x_this.closest('.modal'));
                var table = j_modal.data('target-auto-refresh');
                j_modal.modal('hide');
                if (table || undefined !== undefined)
                    $('#' + table).closest('.wall').find('[ng-click="getdatas()"]').click();
            }
            else inputclear(formname);
        }
    } catch (ex) {
        alertpopup(xhr.responseText || 'Server Error', 'danger');
    }
}

function inputclear(_this) {
    var form = typeof _this === 'string' ? $(_this) : $(_this).closest('form');
    form.find('input[type=text]:not(.disable-clear), input[type=number]:not(.disable-clear), textarea:not(.disable-clear)').val('');
    form.find('input[type=checkbox]:not(.disable-clear)').prop('checked', false);
    $.each(form.find('.input-radio:not(.disable-clear)'), function (index, value) {
        $(value).find('input[type=radio]').first().prop('checked', true);
    });
    $.each(form.find('select:not(.disable-clear)'), function (index, value) {
        var option = $(value).find('option');
        option.removeAttr('selected');
        option.first().attr('selected', 'selected');
    });

    var j_detail = form.find('.detail');
    j_detail.data('index-detail', '');
    j_detail.find('tbody').html('');
}

function oncompletecreate_target(xhr, formname, x_this, target) {
    try {
        var result = JSON.parse(xhr.responseText);
        alertpopup(result.Message, result.Status);
        if (result.IsSuccess) {
            var j_modal = $(x_this.closest('.modal'));
            j_modal.modal('hide');
            var j_target = $('#' + target);
            j_target.val(result.Data);
            j_target.blur();
        }
    } catch (ex) {
        alertpopup(xhr.responseText || 'Server Error', 'danger');
    }
}

function oncompleteedit(xhr, formname, isajax, x_this) {
    try {
        var result = JSON.parse(xhr.responseText);
        alertpopup(result.Message, result.Status);
        if (result.IsSuccess) {
            if (isajax.toUpperCase() === "TRUE") {
                var j_modal = $(x_this.closest('.modal'));
                var table = j_modal.data('target-auto-refresh');
                j_modal.modal('hide');
                if (table || undefined !== undefined)
                    $('#' + table).closest('.wall').find('[ng-click="getdatas()"]').click();
            }
            else {
                var j_this = $(x_this);
                $.each(j_this.find('[name]'), function (index, value) {
                    var j_val = $(value);
                    if (j_val.prop("tagName") === "SELECT") {
                        j_val.data('value', j_val.val());
                    } else if (j_val.prop("tagName") === "TEXTAREA") {
                        j_val.attr('value', value.value);
                    } else if (j_val.prop("tagName") === "INPUT") {
                        if (j_val.attr('type') === 'checkbox') {
                            j_val.data('value', value.checked ? 'true' : 'false');
                        } else if (j_val.attr('type') === 'radio') {
                            if (value.checked) {
                                j_val.closest('[data-radio-value]').data('radio-value', j_val.val());
                            }
                        } else
                            j_val.attr('value', value.value);
                    }
                });
            }
        }
    } catch (ex) {
        alertpopup(xhr.responseText || 'Server Error', 'danger');
    }
}

function add_modal(option) {
    var setting = {
        id: 'modal_' + Math.floor(Math.random() * 100000 + 1),
        size: 'md',
        target_auto_refresh: null,
        header: null,
        body: null,
        footer: null,
        allbody: null,
        status: 'default',
        backdrop: 'static',
        onclose: null
    };
    $.extend(setting, option);

    var stringModal = ' <div class="modal" id="' + setting.id + '" role="dialog" ' +
        (setting.target_auto_refresh || undefined !== undefined ?
            'data-target-auto-refresh="' + setting.target_auto_refresh + '"'
            : '') + '>';
    stringModal += '        <div class="modal-dialog modal-' + setting.size + '">';

    if (setting.allbody === null) {
        var label_header = '';
        if (setting.status === 'danger')
            label_header = 'bg-danger';
        else if (setting.status === 'warning')
            label_header = 'bg-warning';
        else if (setting.status === 'success')
            label_header = 'bg-success';
        else if (setting.status === 'info')
            label_header = 'bg-info';
        else if (setting.status === 'primary')
            label_header = 'bg-primary';

        stringModal += '            <div class="modal-content">';
        stringModal += '                <div class="modal-header ' + label_header + '">';
        stringModal += '                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>';
        stringModal += '                    <h4 class="modal-title">' + setting.header + '</h4>';
        stringModal += '                </div>';
        stringModal += '                <div class="modal-body">' + setting.body + '</div>';
        if (setting.footer) {
            stringModal += '            <div class="modal-footer">' + setting.footer + '</div>';
        }
        stringModal += '            </div>';
    } else {
        stringModal += setting.allbody;
    }

    stringModal += '        </div>';
    stringModal += '    </div>';

    $('#_modal').prepend(stringModal);
    var modal = $('#' + setting.id);

    modal.css("zIndex", getlash_Z_indexmodal() + 1);
    modal.modal({ backdrop: setting.backdrop }).on('hidden.bs.modal', function () {
        if (typeof setting.onclose === 'function') {
            setting.onclose();
        }
        modal.remove();
        if ($('.modal.in').length > 0) $('body').addClass('modal-open');
    });

    var result_id = new Array();
    result_id.push(setting.id);
    var _modal = $('#_modal');
    $.each(modal.find('.modal'), function (index, value) {
        var j_value = $(value);
        var _id = j_value.attr('id');
        var j_removemodal = _modal.children('#' + _id);
        if (j_removemodal.length > 0) j_removemodal.remove();
        j_value.css("zIndex", getlash_Z_indexmodal() + 1);
        j_value.appendTo('#_modal');
        result_id.push(_id);
    });

    return result_id;
}

function getlash_Z_indexmodal() {
    var result = 0;
    $(".modal").each(function () {
        var index_current = parseInt($(this).css("zIndex"), 10);
        if (index_current > result)
            result = index_current;
    });
    return result;
}

function load_angular() {

    try {
        app = angular.module('ihos', ['ngSanitize', 'angular.filter']);
    } catch (e) { return; }

    app.directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(
                function (scope) { return scope.$eval(attrs.compile); },
                function (value) { element.html(value); $compile(element.contents())(scope); }
            );
        };
    }]);

    app.controller('body', function ($scope, $http, $sce, $compile) {
        $scope.ajaxmodal = function (option) {
            var setting = {
                url: null,
                header: null,
                this: null,
                param: null,
                target_auto_refresh: null,
                size : null
            };
            $.extend(setting, option);
            if (setting.this || undefined !== undefined) if ($(setting.this.target).attr('disabled') === 'disabled') return;
            $('#_loading').css('display', '');
            $http({
                method: 'GET',
                url: setting.url,
                params: setting.param,
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function successCallback(response) {
                var modal_id = add_modal({
                    header: setting.header,
                    body: response.data,
                    size: setting.size || 'full',
                    target_auto_refresh: setting.target_auto_refresh
                });
                $.each(modal_id, function (index, value) {
                    $.validator.unobtrusive.parse('#' + value);
                    $compile($("#" + value))($scope);
                });
                all_load();
                $('#_loading').css('display', 'none');
            }, function errorCallback(response) {
                alertpopup(response.statusText || 'Service Error', 'danger');
                $('#_loading').css('display', 'none');
            });
        };
    });

    app.controller('table', function ($scope, $http, $sce, $compile) {
        $scope.pagesizes = [5, 10, 20, 50, 100, 200];

        $scope.update = function () {
            var startdata;
            if ($scope.totalrow === 0) startdata = 0;
            else startdata = $scope.currentpage * $scope.pagesize + 1;
            var enddata = startdata + $scope.pagesize - 1;
            if (enddata > $scope.totalrow) enddata = $scope.totalrow;
            $scope.datainfo = 'Data ' + startdata + '-' + enddata +
                ' of ' + $scope.totalrow +
                ' Page (' + ($scope.totalrow === 0 ? 0 : $scope.currentpage + 1) + '/' + $scope.maxpage + ')';

            // page
            $scope.pagination = "";
            if ($scope.totalrow > 0) {
                var start_page = $scope.currentpage > 0 ? $scope.currentpage - 1 : 0;
                var end_page = $scope.currentpage + 1;
                if ($scope.currentpage === 0) end_page++;
                end_page = end_page > $scope.maxpage - 1 ? $scope.maxpage - 1 : end_page;
                if ($scope.currentpage >= end_page && start_page > 0) start_page--;
                var disable = ' class="disabled"';
                var string_page = '<li' + ($scope.currentpage === 0 ? disable : '') + '><a' + ($scope.currentpage === 0 ? '' : ' ng-click="paging(0)"') + '><span>&laquo;</span></a></li>';
                string_page += '<li' + ($scope.currentpage === 0 ? disable : '') + '><a' + ($scope.currentpage === 0 ? '' : ' ng-click="paging(' + ($scope.currentpage - 1) + ')"') + '><span>&lsaquo;</span></a></li>';
                if (start_page > 0)
                    string_page += '<li><a ng-click="paging(' + (start_page - 1) + ')">..</a></li>';
                for (var i = start_page; i <= end_page; i++) {
                    var active = i === $scope.currentpage;
                    string_page += '<li' + (active ? ' class="active"' : '') + '><a' + (active ? '' : ' ng-click=paging("' + i + '")') + '>' + (i + 1) + '</a></li>';
                }
                if (end_page + 1 < $scope.maxpage)
                    string_page += '<li><a ng-click="paging(' + (end_page + 1) + ')">..</a></li>';
                string_page += '<li' + ($scope.currentpage + 1 === $scope.maxpage ? disable : '') + '><a' + ($scope.currentpage + 1 === $scope.maxpage ? '' : ' ng-click="paging(' + ($scope.currentpage + 1) + ')"') + '><span>&rsaquo;</span></a></li>';
                string_page += '<li' + ($scope.currentpage + 1 === $scope.maxpage ? disable : '') + '><a' + ($scope.currentpage + 1 === $scope.maxpage ? '' : ' ng-click="paging(' + ($scope.maxpage - 1) + ')"') + '><span>&raquo;</span></a></li>';
                $scope.pagination = $sce.trustAsHtml(string_page);
                $scope.pagination = string_page;
            }
        };

        $scope.pagesize = $scope.pagesizes[0];
        $scope.currentpage = 0;
        $scope.totalrow = 0;
        $scope.datas = new Array;
        $scope.maxpage = 0;
        $scope.update();

        $scope.init = function () {
            $scope.filter = Array.apply(null, Array($scope.filterlength)).map(String.prototype.valueOf, "");
        };

        $scope.getdatas = function () {
            $scope.loading = true;

            var dt;
            if ($scope.customdata || undefined !== undefined) {
                dt = $scope.customdata;
            } else {
                dt = {
                    sortBy: $scope.sort,
                    sortByType: $scope.sorttype,
                    pageSize: $scope.pagesize,
                    pageIndex: $scope.currentpage,
                    filter: $scope.filter
                };
            }

            $scope.datas = $http.post($scope.url, dt).then(function successCallback(success) {
                if (!success.data.IsSuccess) {
                    alertpopup(success.data.Message, 'danger');
                }
                else {
                    $scope.datas = success.data.Table || [];
                    $scope.currentpage = success.data.Pagger.PageIndex;
                    $scope.totalrow = success.data.Pagger.TotalRowCount;
                    $scope.maxpage = Math.ceil($scope.totalrow / $scope.pagesize);
                }
                $scope.loading = false;
                $scope.update();
                if (typeof customgetdatascomplate === 'function') {
                    customgetdatascomplate(success, $scope.url, dt);
                }
            }, function errorCallback(response) {
                alertpopup(response.statusText || 'Server Error', 'danger');
                $scope.loading = false;
            });
        };

        $scope.getnumber = function (n) {
            return new Array(n);
        };

        $scope.arraySort = function () {
            $scope.sort = new Array;
            $scope.sorttype = new Array;
        };

        $scope.ordering = function (_this, by) {
            var j_this = $(_this.target);
            j_this.parent('tr').find('.sorting_asc,.sorting_desc').removeClass('sorting_asc').removeClass('sorting_desc').addClass('sorting');
            j_this.removeClass('sorting');
            j_this.addClass($scope.sorttype === 'asc' ? 'sorting_asc' : 'sorting_desc');
            $scope.sorttype = $scope.sorttype === 'asc' && $scope.sort === by ? 'desc' : 'asc';
            $scope.sort = by;
            $scope.getdatas();
        };

        $scope.paging = function (page) {
            $scope.currentpage = page;
            $scope.getdatas();
        };

        $scope.edit = function (id, url, setting) {
            url = url === undefined ? id : url + '/' + encodeURI(id + ''.trim());
            $('#_loading').css('display', '');
            $http({
                method: 'GET',
                url: url,
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function successCallback(response) {
                var modal_id = add_modal({
                    header: "Ubah",
                    body: response.data,
                    size: setting !== undefined ? setting.size : undefined || 'full',
                    target_auto_refresh: $scope.target_auto_refresh_edit
                });
                $.each(modal_id, function (index, value) {
                    $.validator.unobtrusive.parse('#' + value);
                    $compile($("#" + value))($scope);
                });
                all_load();
                $('#_loading').css('display', 'none');
            }, function errorCallback(response) {
                alertpopup(response.statusText || 'Server Error', 'danger');
                $('#_loading').css('display', 'none');
            });
        };

        $scope.edit_param = function (param, url) {
            $('#_loading').css('display', '');
            var id = $.map(param, function (value, index) {
                return index + '=' + encodeURI(value + ''.trim());
            });
            $http({
                method: 'GET',
                url: url + '?' + id.join('&'),
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function successCallback(response) {
                var modal_id = add_modal({
                    header: "Ubah",
                    body: response.data,
                    size: 'full',
                    target_auto_refresh: $scope.target_auto_refresh_edit
                });
                $.each(modal_id, function (index, value) {
                    $.validator.unobtrusive.parse('#' + value);
                    $compile($("#" + value))($scope);
                });
                all_load();
                $('#_loading').css('display', 'none');
            }, function errorCallback(response) {
                alertpopup(response.statusText || 'Server Error', 'danger');
                $('#_loading').css('display', 'none');
            });
        };

        $scope.detail = function (id, url, setting) {
            $('#_loading').css('display', '');
            $http({
                method: 'GET',
                url: url + '/' + encodeURI(id + ''.trim()),
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function successCallback(response) {
                var modal_id = add_modal({
                    header: "Detail",
                    body: response.data,
                    size: setting !== undefined ? setting.size : undefined || 'full'
                });
                $.each(modal_id, function (index, value) {
                    $.validator.unobtrusive.parse('#' + value);
                    $compile($("#" + value))($scope);
                });
                all_load();
                $('#_loading').css('display', 'none');
            }, function errorCallback(response) {
                alertpopup(response.statusText || 'Server Error', 'danger');
                $('#_loading').css('display', 'none');
            });
        };

        $scope.detail_param = function (param, url) {
            $('#_loading').css('display', '');
            var id = $.map(param, function (value, index) {
                return index + '=' + encodeURI(value + ''.trim());
            });
            $http({
                method: 'GET',
                url: url + '?' + id.join('&'),
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function successCallback(response) {
                var modal_id = add_modal({
                    header: "Detail",
                    body: response.data,
                    size: 'full'
                });
                $.each(modal_id, function (index, value) {
                    $.validator.unobtrusive.parse('#' + value);
                    $compile($("#" + value))($scope);
                });
                all_load();
                $('#_loading').css('display', 'none');
            }, function errorCallback(response) {
                alertpopup(response.statusText || 'Server Error', 'danger');
                $('#_loading').css('display', 'none');
            });
        };

        $scope.delete = function (id, url) {
            var isDeleted = false;
            var modal_id = add_modal({
                header: 'Hapus',
                backdrop: true,
                status: 'danger',
                size: 'sm',
                body: '<p>Anda yakin akan menghapus data ini</p>',
                footer: '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>' +
                    '<button type="button" class="btn btn-danger delete-click">HAPUS</button>',
                onclose: function () {
                    if (isDeleted) $scope.getdatas();
                }
            });
            $('#' + modal_id).find('.delete-click').on('click', function () {
                myajax({
                    url: url,
                    data: { id: id },
                    success: function (data) {
                        alertpopup(data.Message, data.Status);
                        if (data.IsSuccess) isDeleted = true;
                    },
                    complete: function () {
                        $('#' + modal_id).modal('hide');
                        $('#_loading').css('display', 'none');
                    }
                });
            });
        };

        $scope.batal = function (id, url) {
            var isDeleted = false;
            var modal_id = add_modal({
                header: 'Batal',
                backdrop: true,
                status: 'danger',
                size: 'sm',
                body: '<p>Anda yakin akan membatalkan data ini</p>',
                footer: '<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>' +
                    '<button type="button" class="btn btn-danger delete-click">Ya</button>',
                onclose: function () {
                    if (isDeleted) $scope.getdatas();
                }
            });
            $('#' + modal_id).find('.delete-click').on('click', function () {
                myajax({
                    url: url,
                    data: { id: id },
                    success: function (data) {
                        alertpopup(data.Message, data.Status);
                        if (data.IsSuccess) isDeleted = true;
                    },
                    complete: function () {
                        $('#' + modal_id).modal('hide');
                        $('#_loading').css('display', 'none');
                    }
                });
            });
        };

        $scope.modalpost = function (param, url, text, status) {
            var isDeleted = false;
            var modal_id = add_modal({
                header: text,
                backdrop: true,
                status: status,
                size: 'sm',
                body: '<p>Anda yakin akan ' + text + '</p>',
                footer: '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>' +
                    '<button type="button" class="btn btn-' + status + ' delete-click">' + text + '</button>',
                onclose: function () {
                    if (isDeleted) $scope.getdatas();
                }
            });
            $('#' + modal_id).find('.delete-click').on('click', function () {
                myajax({
                    url: url,
                    data: param,
                    success: function (data) {
                        alertpopup(data.Message, data.Status);
                        if (data.IsSuccess) isDeleted = true;
                    },
                    complete: function () {
                        $('#' + modal_id).modal('hide');
                        $('#_loading').css('display', 'none');
                    }
                });
            });
        };

        $scope.delete_param = function (param, url) {
            var isDeleted = false;
            var modal_id = add_modal({
                header: 'Hapus',
                backdrop: true,
                status: 'danger',
                size: 'sm',
                body: '<p>Anda yakin akan menghapus data ini</p>',
                footer: '<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>' +
                    '<button type="button" class="btn btn-danger delete-click">HAPUS</button>',
                onclose: function () {
                    if (isDeleted) $scope.getdatas();
                }
            });
            $('#' + modal_id).find('.delete-click').on('click', function () {
                myajax({
                    url: url,
                    data: param,
                    success: function (data) {
                        alertpopup(data.Message, data.Status);
                        if (data.IsSuccess) isDeleted = true;
                    },
                    complete: function () {
                        $('#' + modal_id).modal('hide');
                        $('#_loading').css('display', 'none');
                    }
                });
            });
        };

        $scope.loadfunction = function (func, data) {
            var fn = window[func];
            if (typeof fn === 'function') {
                fn(data);
            }
        };

        $scope.newtab = function (url) {
            newtab(url);
        };

        $scope.getcurrencyfromfloat = function (data) {
            return getcurrencyfromfloat(data);
        };

        $scope.getfloatfromcurrency = function (data) {
            return getfloatfromcurrency(daga);
        };
    });

    app.controller('tablelookup', function ($scope, $http, $sce, $compile) {
        $scope.pagesizes = [5, 10, 20, 50, 100, 200];

        $scope.update = function () {
            var startdata;
            if ($scope.totalrow === 0) startdata = 0;
            else startdata = $scope.currentpage * $scope.pagesize + 1;
            var enddata = startdata + $scope.pagesize - 1;
            if (enddata > $scope.totalrow) enddata = $scope.totalrow;
            $scope.datainfo = 'Data ' + startdata + '-' + enddata +
                ' of ' + $scope.totalrow +
                ' Page (' + ($scope.totalrow === 0 ? 0 : $scope.currentpage + 1) + '/' + $scope.maxpage + ')';

            // page
            $scope.pagination = "";
            if ($scope.totalrow > 0) {
                var start_page = $scope.currentpage > 0 ? $scope.currentpage - 1 : 0;
                var end_page = $scope.currentpage + 1;
                if ($scope.currentpage === 0) end_page++;
                end_page = end_page > $scope.maxpage - 1 ? $scope.maxpage - 1 : end_page;
                if ($scope.currentpage >= end_page && start_page > 0) start_page--;
                var disable = ' class="disabled"';
                var string_page = '<li' + ($scope.currentpage === 0 ? disable : '') + '><a ' + ($scope.currentpage === 0 ? '' : 'ng-click="paging(0)') + '"><span>&laquo;</span></a></li>';
                string_page += '<li' + ($scope.currentpage === 0 ? disable : '') + '><a ' + ($scope.currentpage === 0 ? '' : 'ng-click="paging(' + ($scope.currentpage - 1) + ')') + '"><span>&lsaquo;</span></a></li>';
                if (start_page > 0)
                    string_page += '<li><a ng-click="paging(' + (start_page - 1) + ')">..</a></li>';
                for (var i = start_page; i <= end_page; i++) {
                    var active = i === $scope.currentpage;
                    string_page += '<li' + (active ? ' class="active"' : '') + '><a' + (active ? '' : ' ng-click=paging("' + i + '")') + '>' + (i + 1) + '</a></li>';
                }
                if (end_page + 1 < $scope.maxpage)
                    string_page += '<li><a ng-click="paging(' + (end_page + 1) + ')">..</a></li>';
                string_page += '<li' + ($scope.currentpage + 1 === $scope.maxpage ? disable : '') + '><a ' + ($scope.currentpage + 1 === $scope.maxpage ? '' : 'ng-click="paging(' + ($scope.currentpage + 1) + ')') + ' "><span>&rsaquo;</span></a></li>';
                string_page += '<li' + ($scope.currentpage + 1 === $scope.maxpage ? disable : '') + '><a ' + ($scope.currentpage + 1 === $scope.maxpage ? '' : 'ng-click="paging(' + ($scope.maxpage - 1) + ')') + '"><span>&raquo;</span></a></li>';
                $scope.pagination = $sce.trustAsHtml(string_page);
                $scope.pagination = string_page;
            }
        };

        $scope.pagesize = $scope.pagesizes[0];
        $scope.currentpage = 0;
        $scope.totalrow = 0;
        $scope.filter = new Array;
        $scope.datas = new Array;
        $scope.maxpage = 0;
        $scope.update();
        $scope.blurfirsttarget = false;
        $scope.blurfirsttarget2 = false;

        // for detail
        $scope.values = new Array();

        $scope.getdatas = function () {
            $scope.loading = true;
            $scope.datas = $http.post($scope.url, {
                sortBy: $scope.sort,
                sortByType: $scope.sorttype,
                pageSize: $scope.pagesize,
                pageIndex: $scope.currentpage,
                filter: $scope.filter
            }).then(function successCallback(success) {
                if (!success.data.IsSuccess) {
                    alertpopup(success.data.Message, 'danger');
                }
                else {
                    $scope.datas = success.data.Table || [];
                    $scope.currentpage = success.data.Pagger.PageIndex;
                    $scope.totalrow = success.data.Pagger.TotalRowCount;
                    $scope.maxpage = Math.ceil($scope.totalrow / $scope.pagesize);
                }
                $scope.loading = false;
                $scope.update();
                $scope.values = new Array();
            }, function errorCallback(response) {
                alertpopup(response.statusText || 'Server Error', 'danger');
                $scope.loading = false;
            });
        };

        $scope.getnumber = function (n) {
            return new Array(n);
        };

        $scope.init = function () {
            $scope.filter = Array.apply(null, Array($scope.filterlength)).map(String.prototype.valueOf, "");
        };

        $scope.ordering = function (_this, by) {
            var j_this = $(_this.target);
            j_this.parent('tr').find('.sorting_asc,.sorting_desc').removeClass('sorting_asc').removeClass('sorting_desc').addClass('sorting');
            j_this.removeClass('sorting');
            j_this.addClass($scope.sorttype === 'asc' ? 'sorting_asc' : 'sorting_desc');
            $scope.sorttype = $scope.sorttype === 'asc' && $scope.sort === by ? 'desc' : 'asc';
            $scope.sort = by;
            $scope.getdatas();
        };

        $scope.paging = function (page) {
            $scope.currentpage = page;
            $scope.getdatas();
        };

        $scope.select = function (event, target) {
            var j_this = $(event.target).parent('tr');
            if (j_this.hasClass('active-info'))
                $scope.selected(event);
            else {
                $scope.value = target;
                j_this.closest('tbody').find('tr.active-info').removeClass('active-info');
                j_this.addClass('active-info');
            }
        };

        $scope.selected = function (event) {
            var modal_content = $(event.target).closest('.modal-content');
            var tr_active = modal_content.find('table tbody tr.active-info');
            if (tr_active.length === 0)
                alertpopup('silahkan klik pilihan', 'info');
            else {
                var target_form = $('#' + $scope.targetform);
                var firstobj;
                var target_column = modal_content.closest('.modal')
                    .data('modal-target') || $scope.target;
                $.each(target_column, function (index, value) {
                    var target = target_form.find(value);
                    target.val($scope.value[index.replace('t', '')]);
                    if (index === 't0') firstobj = value;
                });
                modal_content.find('button.close').click();
                if ($scope.blurfirsttarget) {
                    var j_firstobj = target_form.find(firstobj);
                    j_firstobj[0].onblur();
                } else if ($scope.blurfirsttarget2) {
                    var j_firstobj2 = target_form.find(firstobj);
                    j_firstobj2.focus();
                }
                if ($scope.targetenterevent) {
                    var e = $.Event("keypress", { which: 13, keyCode: 13 });
                    $($scope.targetenterevent).trigger(e);
                }
            }
        };

        $scope.multipleselect = function (event, target) {
            var j_this = $(event.target).parent('tr');
            if (j_this.hasClass('active-info')) {
                $scope.values[j_this.data('detail-index')] = undefined;
                j_this.removeClass('active-info');
            } else {
                $scope.values[j_this.data('detail-index')] = target;
                j_this.addClass('active-info');
            }
        };

        $scope.multipleselected = function (event) {
            var modal_content = $(event.target).closest('.modal-content');
            var tr_active = modal_content.find('table tbody tr.active-info');
            if (tr_active.length === 0)
                alertpopup('silahkan klik pilihan', 'info');
            else {
                var target_form = $('#' + $scope.targetform);
                var target_table = target_form.find('#' + $scope.targetTable);
                $.each($scope.values, function (index, value) {
                    if (value || undefined !== undefined) {
                        var firstobj;
                        $scope.addrowdetaillookup();
                        var last_tr = target_table.find('tbody tr').last();
                        $.each($scope.target, function (index_, value_) {
                            var j_targetvalue = last_tr.find('[id$=' + value_.split('_-1_')[1] + ']');
                            var ind = index_.replace('t', '');
                            j_targetvalue.val(value[ind]);
                            if (ind === "0") firstobj = j_targetvalue;
                        });
                        if ($scope.blurfirsttarget) {
                            var trigger_onblur = last_tr.find('.trigger-onblur');
                            if (trigger_onblur.length > 0) {
                                try { trigger_onblur.trigger("blur"); } catch (ex) { console.log(ex); }
                            }

                            var j_firstobj = target_form.find(firstobj);
                            try { j_firstobj[0].onblur(); } catch (ex) { console.log(ex); }
                        } else if ($scope.blurfirsttarget2) {
                            var j_firstobj2 = target_form.find(firstobj);
                            j_firstobj2.focus();
                        }
                    }
                });
                tr_active.removeClass("active-info");
                $scope.values = new Array();
                modal_content.find('button.close').click();
            }
        };

        $scope.detail = function (id, url) {
            $('#_loading').css('display', '');
            $http({
                method: 'GET',
                url: url + '/' + encodeURI(id + ''.trim()),
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function successCallback(response) {
                var modal_id = add_modal({
                    header: "Detail",
                    body: response.data,
                    size: 'full'
                });
                $.each(modal_id, function (index, value) {
                    $.validator.unobtrusive.parse('#' + value);
                    $compile($("#" + value))($scope);
                });
                all_load();
                $('#_loading').css('display', 'none');
            }, function errorCallback(response) {
                alertpopup(response.statusText || 'Server Error', 'danger');
                $('#_loading').css('display', 'none');
            });
        };

        $scope.addrowdetaillookup = function () {
            var j_this = $('#' + $scope.targetform);
            var j_detail = $scope.targetTable ?
                j_this.find('#' + $scope.targetTable).closest('.detail') :
                j_this.find('.detail');
            var formid = $scope.targetform;
            var index_detail = j_detail.data('index-detail') || 0;
            var result = '<tr>';
            $.each($('#' + $scope.targetInputTable + ' .input-detail'), function (index, value) {
                result += '<td>';
                result += $(value).html().replace(/List\[-1]./g, 'List[' + index_detail + '].').replace(/List_-1__/g, 'List_' + index_detail + '__');
                result += '</td>';
            });
            result += '</tr>';
            result = result.replace(/input-date-loaded/g, '')
                .replace(/input-time-loaded/g, '')
                .replace(/input-money-loaded/g, '')
                .replace(/hasDatepicker/g, '');
            j_detail.data('index-detail', index_detail + 1);
            var j_tr = j_detail.find('tbody').append(result).find('tr').last();
            $compile(j_tr)($scope);
            all_load();
            var j_form = $('#' + formid);
            j_form.removeData('validator');
            j_form.removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse(j_form);
        };

        $scope.getcurrencyfromfloat = function (data) {
            return getcurrencyfromfloat(data);
        };

        $scope.getfloatfromcurrency = function (data) {
            return getfloatfromcurrency(daga);
        };
    });

    app.controller('form', function ($scope, $http, $sce, $compile) {

        $scope.addrowdetail = function (_this, inputtarget, autofocus) {
            var j_this = $(_this.target);
            var j_detail = j_this.closest('.detail');
            var formid = j_this.closest('form').attr('id');
            var index_detail = j_detail.data('index-detail') || 0;
            var result = '<tr>';
            $.each($(inputtarget).find('.input-detail'), function (index, value) {
                result += '<td>';
                var j_value = $(value);
                result += j_value.html().replace(/List\[-1]./g, 'List[' + index_detail + '].').replace(/List_-1__/g, 'List_' + index_detail + '__');
                result += '</td>';
            });
            result += '</tr>';
            result = result.replace(/input-date-loaded/g, '')
                .replace(/input-time-loaded/g, '')
                .replace(/input-money-loaded/g, '')
                .replace(/hasDatepicker/g, '');
            j_detail.data('index-detail', index_detail + 1);
            var j_tr = j_detail.find('table tbody').append(result).find('tr').last();
            var trigger_onblur = j_tr.find('.trigger-onblur');
            $compile(j_tr)($scope);
            all_load();
            var j_form = $('#' + formid);
            j_form.removeData('validator');
            j_form.removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse(j_form);
            $.each(trigger_onblur, function (index, value) {
                $(value).blur();
            });
            if (autofocus || undefined !== undefined) {
                j_tr.find(autofocus).focus();
            }
            if (typeof customaddrowdetail === 'function') {
                customaddrowdetail(index_detail, _this, inputtarget, autofocus);
            }
        };
    });
}

function getcurrencyfromfloat(text) {
    if (text === '0') return '0';
    if (text === '') return '';
    if (text === -0) return '-';
    if (!text) return '';
    text = text.toString().replace(/\,/g, '');
    var point = text.split('.')[1] || '';
    var number = parseFloat(text.toString().replace(/\./g, '.'));
    var arrResult = number.toString().replace(/\,/g, '.').split('.');
    var result = arrResult[0].substr(0, 16).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    result = result === "NaN" ? "" : result;
    if (arrResult.length > 1) return result + '.' + point;
    else return result;
}

function getfloatfromcurrency(currency) {
    if (!currency) { return ""; }
    if (currency === "-") currency = "-0";
    return parseFloat(currency.toString().replace(/[^0-9\.-]+/g, ""));
}

function all_load() {
    $('.input-money:not(.input-money-loaded)').inputmoney();
    load_angular();

    $.each($('.auto-uncheck'), function (index, value) {
        value.checked = false;
        $(value).removeClass('auto-uncheck');
    });
}

$.fn.inputmoney = function () {
    this.on('blur', function (e) {
        var result = getcurrencyfromfloat(this.value);
        this.value = result === '' ? '' : result;
    });

    this.on('focus', function (e) {
        this.value = getfloatfromcurrency(this.value.replace(/Rp. /g, '')) || "";
    });

    this.addClass('input-money-loaded');
    return this;
};

all_load();
$('#_loading').css('display', 'none');

$($('.sidebar.sidebar-main li a[href]').get().reverse()).each(function (index, value) {
    if ($(value).attr('href').split(urlroot || '/')[1] === window.location.pathname.split(urlroot || '/')[1]) {
        var v = $(value);
        var li1 = v.closest('li');
        if (li1.length > 0) {
            li1.addClass('active');
            var ul = li1.closest('ul');
            if (ul.length > 0) {
                ul.css('display', 'block');
                var li2 = ul.closest('li');
                if (li2.length > 0) {
                    li2.addClass('active');
                }
            }
        }
        return false;
    }
});

// ========================= C L A S S - O B J E C T

class iPainter {
    constructor(canvas, img, ximg, color, line, width, height) {
        this.canvas = document.getElementById(canvas);
        this.img = document.getElementById(img);
        this.canvas.setAttribute('width', width || this.img.width);
        this.canvas.setAttribute('height', height || this.img.height);
        this.context = this.canvas.getContext('2d');
        this.width = width;
        this.height = height;
        if (this.width || undefined !== undefined && this.height || undefined !== undefined)
            this.context.drawImage(ximg || this.img, 0, 0, this.width, this.height);
        else
            this.context.drawImage(ximg || this.img, 0, 0);
        var t = this;
        this.clickX = new Array();
        this.clickY = new Array();
        this.clickDrag = new Array();
        var _paint;
        this.canvas.addEventListener('mousedown', function (e) {
            var _mouseX = e.pageX - this.offsetLeft;
            var _mouseY = e.pageY - this.offsetTop;
            _paint = true;
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
            redraw();
        });
        this.canvas.addEventListener('mousemove', function (e) {
            if (_paint) {
                addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
                redraw();
            }
        });
        this.canvas.addEventListener('mouseup', function (e) {
            _paint = false;
        });
        this.canvas.addEventListener('mouseleave', function (e) {
            _paint = false;
        });
        function addClick(x, y, dragging) {
            t.clickX.push(x);
            t.clickY.push(y);
            t.clickDrag.push(dragging);
        }
        function redraw() {
            t.context.strokeStyle = color || "#df4b26";
            t.context.lineJoin = "round";
            t.context.lineWidth = line || 3;
            for (var i = 0; i < t.clickX.length; i++) {
                t.context.beginPath();
                if (t.clickDrag[i] && i) {
                    t.context.moveTo(t.clickX[i - 1], t.clickY[i - 1]);
                } else {
                    t.context.moveTo(t.clickX[i] - 1, t.clickY[i]);
                }
                t.context.lineTo(t.clickX[i], t.clickY[i]);
                t.context.closePath();
                t.context.stroke();
            }
        }
    }
    clear() {
        this.clickX = new Array();
        this.clickY = new Array();
        this.clickDrag = new Array();
        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
        if (this.width || undefined !== undefined && this.height || undefined !== undefined)
            this.context.drawImage(this.img, 0, 0, this.width, this.height);
        else
            this.context.drawImage(this.img, 0, 0);
    }
    convert(image) {
        var ImageData = this.canvas.toDataURL("image/png");
        document.getElementById(image).value = ImageData.replace(/^data:image\/[a-z]+;base64,/, "");
    }
}