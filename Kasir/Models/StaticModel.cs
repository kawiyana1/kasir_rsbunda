﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kasir.Entities;

namespace Kasir.Models
{
    public class StaticModel
    {
        public static string DbEntityValidationExceptionToString(DbEntityValidationException ex)
        {
            var msgs = new List<string>();
            foreach (var eve in ex.EntityValidationErrors)
            {
                var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                msgs.Add(msg);
            }
            return string.Join("\n", msgs.ToArray());
        }

        public static List<SelectListItem> ListSection
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true && x.TipePelayanan == "KASIR").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID,
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionName
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true && x.TipePelayanan == "KASIR").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionName,
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListDokter
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.mDokter.Where(x => x.Active == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.DokterID,
                            Text = item.NamaDOkter
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListKerjaSama
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmJenisKerjasama.ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.JenisKerjasamaID.ToString(),
                            Text = item.JenisKerjasama
                        });
                    }
                    r.Insert(0, new SelectListItem() {
                        Value = "ALL",
                        Text = "ALL"
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListShif
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "All", Value = "0"},
                    new SelectListItem() { Text = "Pagi", Value = "1"},
                    new SelectListItem() { Text = "Sore", Value = "2"},
                    new SelectListItem() { Text = "Malam", Value = "3"}
                };
            }
        }

        public static List<SelectListItem> ListTipeTransaksi
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "KAS", Value = "KAS"},
                    new SelectListItem() { Text = "KARTU KREDIT", Value = "KARTU KREDIT"},
                    new SelectListItem() { Text = "BANK", Value = "BANK"},
                };
            }
        }

        public static List<SelectListItem> SelectListFormatReport
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "PDF", Value = "PDF"},
                    new SelectListItem() { Text = "Excel", Value = "Excel" },
                    new SelectListItem() { Text = "Word", Value = "Word" }
                };
            }
        }

        public static List<SelectListItem> SelectListShift
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "Pagi", Value = "Pagi"},
                    new SelectListItem() { Text = "Sore", Value = "Sore"},
                    new SelectListItem() { Text = "Malam", Value = "Malam"}
                };
            }
        }

        public static List<SelectListItem> SelectListTipePerawatan
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "Rawat Jalan", Value = "RJ"},
                    new SelectListItem() { Text = "Rawat Inap", Value = "RI"},
                    new SelectListItem() { Text = "ODC", Value = "ODC"}
                };
            }
        }

        public static List<SelectListItem> SelectListKelas
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.SIMmKelas.Where(x => x.Active == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaKelas,
                        Value = x.KelasID.ToString(),
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListLokasi
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.mLokasi.Where(x => x.Lokasi_ID == 1366 || x.Lokasi_ID == 1368 || x.Lokasi_ID == 296).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama_Lokasi,
                        Value = x.Lokasi_ID.ToString()
                    });
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListLokasiAll
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.mLokasi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama_Lokasi,
                        Value = x.Lokasi_ID.ToString()
                    });
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKelompokJenis
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIMEntities())
                {
                    result = s.SIMmKelompokJenisObat.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.KelompokJenis,
                        Value = x.KelompokJenis,
                    });
                    result.Insert(0, new SelectListItem() {
                        Text = "All",
                        Value = " "
                    });
                }
                return result;
            }
        }

        public static List<SelectListItem> ListSectionStok
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "FARMASI" || x.TipePelayanan == "GUDANG").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListJenisKelompokObat
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmKelompokJenisObat.ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.KelompokJenis,
                            Text = item.KelompokJenis
                        });
                    }
                }
                return r;
            }
        }

    }
}