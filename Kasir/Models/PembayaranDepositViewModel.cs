﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PembayaranDepositViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NOReg { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public decimal NilaiDeposit { get; set; }
        public string NilaiDeposit_View { get; set; }
        public Nullable<bool> AUdit { get; set; }
        public Nullable<bool> Batal { get; set; }
        public string SectionID { get; set; }
    }
}