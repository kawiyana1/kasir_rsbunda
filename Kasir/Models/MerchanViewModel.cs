﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class MerchanViewModel
    {
        public string ID { get; set; }
        public string NamaBank { get; set; }
        public int Akun_ID_Tujuan { get; set; }
        public Nullable<double> AddCharge_Debet { get; set; }
        public Nullable<double> AddCharge_Kredit { get; set; }
        public Nullable<double> Diskon { get; set; }
        public string Akun_Name { get; set; }
    }
}