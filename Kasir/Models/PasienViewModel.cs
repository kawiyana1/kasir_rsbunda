﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PasienViewModel
    {
        public string Status_Pulang { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public string SectionPerawatan { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisPasien { get; set; }
        public string SisaResep { get; set; }
        public string SisaPenunjang { get; set; }
        public string SisaRJ { get; set; }
        public string NoReg { get; set; }
        public bool RawatInap { get; set; }
        public string SectionPerawatanID { get; set; }
        public string Alamat { get; set; }
        public string TipePelayanan { get; set; }
        public string KelasPelayanan { get; set; }
        public string JenisKerjasama { get; set; }
        public string nilai { get; set; }
        public Nullable<bool> ProsesPayment { get; set; }
        public string KelasID { get; set; }
    }
}