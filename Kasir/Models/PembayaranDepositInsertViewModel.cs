﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PembayaranDepositInsertViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        [DataType(DataType.Time)]
        public System.DateTime Jam { get; set; }
        [Required]
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string TipePasien { get; set; }
        public string Perusahaan { get; set; }
        public string NoKartu { get; set; }
        public string Kamar { get; set; }
        public decimal NilaiDeposit { get; set; }
        public string NilaiDeposit_View { get; set; }
        public string Keterangan { get; set; }
        public Nullable<short> UserID { get; set; }
        public string UserIDWeb { get; set; }
        public string SectionID { get; set; }
        public Nullable<bool> Audit { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<double> AddCharge_Persen { get; set; }
        public string AddCharge_Persen_View { get; set; }
        public Nullable<decimal> AddCharge { get; set; }
        public string AddCharge_View { get; set; }
        public string IDBank { get; set; }
        public string NamaBank { get; set; }
        public bool Tunai { get; set; }
        public bool KartuKredit { get; set; }
        public string Shift { get; set; }
        public string Total { get; set; }
    }
}