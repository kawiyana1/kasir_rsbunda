﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class InvoiceLostViewModel
    {
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKerjasama { get; set; }
        public decimal NilaiAwal { get; set; }
        public string NilaiAwal_View { get; set; }
        public Nullable<decimal> NilaiAkumulasi { get; set; }
        public string NilaiAkumulasi_View { get; set; }
        public Nullable<decimal> NilaiKewajiban { get; set; }
        public string NilaiKewajiban_View { get; set; }
    }
}