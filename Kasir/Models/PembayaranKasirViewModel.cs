﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PembayaranKasirViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string NoReg { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public Nullable<double> NilaiInvoice { get; set; }
        public bool Batal { get; set; }
        public bool Audit { get; set; }
        public string SectionID { get; set; }
        public string TipePelayanan { get; set; }
        public string UserKasir { get; set; }
    }
}