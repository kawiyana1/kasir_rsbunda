﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class TipePembayaranViewModel
    {
        public string Tipe { get; set; }
        public string NamaJenisBayar { get; set; }
        public Nullable<int> Akun_ID { get; set; }
        public string MerchanID { get; set; }
    }
}