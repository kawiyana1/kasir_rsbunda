﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class RegistrasiViewModel
    {
        public string NoReg { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public string NoKartu { get; set; }
        public bool RawatInap { get; set; }
        public string TipePelayanan { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string KdKelasHak { get; set; }
        public string KelasHak { get; set; }
        public string KdKelasPelayanan { get; set; }
        public string KelasPelayanan { get; set; }
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public string DokterRawatID { get; set; }
        public string NamaDokter { get; set; }
        public Nullable<decimal> Deposit { get; set; }
        public string Deposit_View { get; set; }
        public int JenisPembayaran { get; set; }
        public string nilai { get; set; }
    }
}