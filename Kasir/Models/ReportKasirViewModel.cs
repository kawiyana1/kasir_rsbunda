﻿using Kasir.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class ReportKasirViewModel
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime Start { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public DateTime End { get; set; }
        public int BarangID { get; set; }
        [Required]
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }

        public string JenisReport { get; set; }
        public string DokterID { get; set; }

        public string SPName { get; set; }
    }

    public class ReportHelperViewModel
    {
        public string Name { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public int LokasiID { get; set; }
        public List<HReportModel> Reports { get; set; }
    }
}