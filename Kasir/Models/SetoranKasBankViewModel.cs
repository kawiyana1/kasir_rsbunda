﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class SetoranKasBankViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        [DataType(DataType.Time)]
        public System.DateTime Jam { get; set; }
        public string Tanggal_View { get; set; }
        public string Jam_View { get; set; }
        public string Keterangan { get; set; }
        public short UserID { get; set; }
        public decimal Nilai { get; set; }
        public string Nilai_View { get; set; }
        public int AkunID { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<bool> Posting { get; set; }
        public string Tipe { get; set; }
        public Nullable<bool> Closing { get; set; }
        public string TipeTransaksi { get; set; }
        public string MerchanID { get; set; }
        public Nullable<int> AkunMerchanID { get; set; }
        public string DokterID { get; set; }
        public string SectionID { get; set; }
        public string Shift { get; set; }
        public Nullable<System.DateTime> TglHonor { get; set; }
        public Nullable<decimal> HonorBruto { get; set; }
        public Nullable<decimal> Pajak { get; set; }
        public string DIterimaDari { get; set; }
        public string NoRegPasien { get; set; }
        public Nullable<decimal> NilaiTunaiOrig { get; set; }
        public string NoRev { get; set; }
        public string UserIDWeb { get; set; }
        public string UsernameWeb { get; set; }


        public string Akun_Akun_No { get; set; }
        public string Akun_Akun_Name { get; set; }

    }
}