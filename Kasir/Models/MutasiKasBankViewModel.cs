﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class MutasiKasBankViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View{ get; set; }

        [DataType(DataType.Time)]
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string IDBank { get; set; }
        public string NamaBank { get; set; }
        public int AkunIDBank { get; set; }
        public string Keterangan { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string Total_View { get; set; }
        public string Shift { get; set; }
        public Nullable<bool> Audit { get; set; }
        public bool Batal { get; set; }
        public string AlasanBatal { get; set; }
        public string SectionID { get; set; }
        public string UserIDWeb { get; set; }
    }

    public class MutasiKasBankDetailViewModel
    {
        public string NoBukti { get; set; }
        public string Tanggal_View { get; set; }
        public string Keterangan { get; set; }
        public string Total_View { get; set; }
        public string NoBuktiReferensi { get; set; }
        public string IdJenisBayar { get; set; }
        public string NamaJenisBayar { get; set; }
        public Nullable<decimal> NilaiBayar { get; set; }
    }

    public class UpdateKasir
    {
        public string NoBukti { get; set; }
        public bool val { get; set; }
    }
}