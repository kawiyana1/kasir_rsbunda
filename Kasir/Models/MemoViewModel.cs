﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class MemoViewModel
    {
        public string NoUrut { get; set; }
        public string NoReg { get; set; }
        public string SectionName { get; set; }
        public string Memo { get; set; }
    }
}