﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kasir.Models
{
    public class ManualPostingViewModel
    {
        public string NoBukti { get; set; }
        public string RegNo { get; set; }
        public int Pembayaran { get; set; }
        public Nullable<byte> NoUrut { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Tanggal { get; set; }
        [DataType(DataType.Time)]
        public Nullable<System.DateTime> Jam { get; set; }
        public string SectionID { get; set; }
        public string SectionAsalID { get; set; }
        public string DokterID { get; set; }
        public string Symptom { get; set; }
        public string Therapi { get; set; }
        public string Kecelakaan { get; set; }
        public Nullable<bool> Emergency { get; set; }
        public string RujukDr { get; set; }
        public string NoRujuk { get; set; }
        public Nullable<bool> Meninggal { get; set; }
        public Nullable<System.DateTime> Meninggal_Jam { get; set; }
        public Nullable<bool> DOA { get; set; }
        public string IDICD { get; set; }
        public string IDICD2 { get; set; }
        public Nullable<bool> KasusPertama { get; set; }
        public Nullable<bool> TindakLanjut_Pulang { get; set; }
        public Nullable<bool> TindakLanjut_KonsulMedik { get; set; }
        public string Konsul_DOkterID { get; set; }
        public string Konsul_SectionID { get; set; }
        public Nullable<bool> TindakLanjut_RI { get; set; }
        public string RI_SectionID { get; set; }
        public string KelasAsalID { get; set; }
        public string KdKelas { get; set; }
        public Nullable<bool> TitipKelas { get; set; }
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public Nullable<int> RI_JenisPasienID { get; set; }
        public Nullable<bool> TindakLanjut_ODC { get; set; }
        public Nullable<bool> TindakLanjut_TransferOK { get; set; }
        public Nullable<bool> TindakLanjut_TransferVK { get; set; }
        public Nullable<bool> TindakLanjutCekUpUlang { get; set; }
        public Nullable<bool> TindakLanjut_PulangPaksa { get; set; }
        public Nullable<System.DateTime> TglCekUp { get; set; }
        public string Status { get; set; }
        public Nullable<short> UserID { get; set; }
        public Nullable<decimal> KomisiDokter { get; set; }
        public Nullable<decimal> THT { get; set; }
        public Nullable<decimal> Pajak { get; set; }
        public Nullable<decimal> KomisiObat { get; set; }
        public Nullable<bool> RawatInap { get; set; }
        public decimal Jumlah { get; set; }
        public Nullable<bool> Audit { get; set; }
        public Nullable<double> PPN { get; set; }
        public Nullable<int> Umur_Th { get; set; }
        public Nullable<int> Umur_Bln { get; set; }
        public Nullable<int> Umur_Hr { get; set; }
        public Nullable<double> PersenAnastesi { get; set; }
        public Nullable<double> PersenAsisten { get; set; }
        public Nullable<decimal> KomisiDokterAnak { get; set; }
        public Nullable<decimal> KomisiAssistenBedah { get; set; }
        public Nullable<bool> Batal { get; set; }
        public string KategoriPlafon { get; set; }
        public string NamaKasus { get; set; }
        public Nullable<decimal> NilaiPlafon { get; set; }
        public Nullable<decimal> SudahTerpakai { get; set; }
        public string NRM { get; set; }
        public Nullable<int> TerpakaiRIPerTahun { get; set; }
        public Nullable<int> TerpakaiRIPerOpname { get; set; }
        public Nullable<decimal> TerpakaiNominalRI { get; set; }
        public Nullable<int> PlafonRIPerTahun { get; set; }
        public Nullable<int> PlafonRIPerOpname { get; set; }
        public Nullable<decimal> PlafonRINominal { get; set; }
        public string NoKamarPerawatan { get; set; }
        public Nullable<int> TerpakaiICUPerOpname { get; set; }
        public Nullable<int> TerpakaiICUPerTahun { get; set; }
        public Nullable<int> PlafonICUPerOpname { get; set; }
        public Nullable<int> PlafonICUPerTahun { get; set; }
        public Nullable<bool> TindakLanjutReferal { get; set; }
        public string KeteranganReferal { get; set; }
        public string Triage { get; set; }
        public Nullable<bool> DC { get; set; }
        public Nullable<int> DC_Hari { get; set; }
        public Nullable<bool> ManualPosting { get; set; }
        public bool MCU { get; set; }
        public Nullable<bool> ClosedTransaksi { get; set; }
        public Nullable<short> ClosedOleh { get; set; }
        public Nullable<System.DateTime> ClosedTgl { get; set; }
        public Nullable<System.DateTime> ClosedJam { get; set; }
        public Nullable<bool> ProsesAudit { get; set; }
        public string Gender { get; set; }
        public Nullable<byte> KelompokUmur { get; set; }
        public Nullable<bool> ODC { get; set; }
        public Nullable<bool> AdaPaket { get; set; }
        public Nullable<decimal> NilaiPaket { get; set; }
        public Nullable<bool> AdaPlafon { get; set; }
        public Nullable<decimal> NilaiPlafonObat { get; set; }
        public Nullable<decimal> CustomerKerjasamaID { get; set; }
        public string KdKelasKamar { get; set; }
        public string Shift { get; set; }
        public string ICUSkoring { get; set; }
        public string ICUIndikasiMasuk { get; set; }
        public string OK_JenisAnastesi { get; set; }
        public Nullable<System.DateTime> OK_JamAnasMulai { get; set; }
        public Nullable<System.DateTime> OK_JamAnasSelesai { get; set; }
        public Nullable<System.DateTime> OK_JamOperasiMulai { get; set; }
        public Nullable<System.DateTime> OK_JamOperasiSelesai { get; set; }
        public Nullable<byte> OK_KategoriOperasiID { get; set; }
        public string OK_PenanggungNama { get; set; }
        public string OK_PenanggungAlamat { get; set; }
        public string OK_PenanggungID { get; set; }
        public string OK_PenanggungHub { get; set; }
        public string OK_DokterBedahID { get; set; }
        public string OK_DokterBedahID2 { get; set; }
        public string OK_DOkterAssBedah { get; set; }
        public string OK_DOkterAssBedah2 { get; set; }
        public string OK_DokterANak { get; set; }
        public string OK_DokterAnasID { get; set; }
        public Nullable<double> PersenAnak { get; set; }
        public Nullable<double> PersenBedah { get; set; }
        public Nullable<double> PersenPerawatOK { get; set; }
        public Nullable<bool> Cyto { get; set; }
        public string Keterangan { get; set; }
        public string DokterPengirimID { get; set; }
        public Nullable<decimal> DokterOperator_Komisi { get; set; }
        public Nullable<decimal> DokterOperator_Pajak { get; set; }
        public Nullable<decimal> DokterOPerator_THT { get; set; }
        public Nullable<decimal> DokterOperatorAss_Komisi { get; set; }
        public Nullable<decimal> DokterOperatorAss_Pajak { get; set; }
        public Nullable<decimal> DokterOPeratorAss_THT { get; set; }
        public Nullable<decimal> DokterAnas_Komisi { get; set; }
        public Nullable<decimal> DokterAnas_Pajak { get; set; }
        public Nullable<decimal> DokterAnas_THT { get; set; }
        public Nullable<decimal> Penata_Honor { get; set; }
        public Nullable<decimal> Penata_Pajak { get; set; }
        public Nullable<decimal> Penata_THT { get; set; }
        public Nullable<bool> AdaDataBayi { get; set; }
        public string SupplierPengirimID { get; set; }
        public string KriteriaOperasi { get; set; }
        public Nullable<bool> Sectio { get; set; }
        public string SpesialisasiID { get; set; }
        public string JenisLukaID { get; set; }
        public string RuangOK { get; set; }
        public string IndikasiVKID { get; set; }
        public Nullable<short> UmurKehamilan { get; set; }
        public Nullable<bool> Pribadi { get; set; }
        public string TransferToSectionID { get; set; }
        public string TransferKelasID { get; set; }
        public Nullable<bool> TransferTitipKelas { get; set; }
        public string TransferToKamar { get; set; }
        public string TransferToNoBed { get; set; }
        public Nullable<bool> Closed { get; set; }
        public Nullable<bool> TransferKeOK { get; set; }
        public Nullable<bool> ClosedPemakaian { get; set; }
        public string KeteranganPemeriksaan { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<decimal> Pengirim { get; set; }
        public Nullable<decimal> TarifHD_USD { get; set; }
        public string UnitBisnisID { get; set; }
        public string NoMCU { get; set; }
        public string TransferKelasKamarID { get; set; }
        public string Diagnosa { get; set; }
        public Nullable<short> UmurKehamilanHari { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string AnalisID { get; set; }
        public string JenisPasien { get; set; }
        public string LokasiPasien { get; set; }
        public string NoReg { get; set; }
        public Nullable<bool> PakeReg { get; set; }
        public string KamarNo { get; set; }
        public Nullable<bool> adahasil { get; set; }
        public Nullable<bool> SIH { get; set; }
        public Nullable<bool> Rujukan { get; set; }
        public Nullable<bool> Accept { get; set; }
        public Nullable<int> Nomor { get; set; }
        public string TransferSectionID { get; set; }
        public string TransferDokterID { get; set; }
        public Nullable<bool> Dirujuk { get; set; }
        public string DirujukVendorID { get; set; }
        public Nullable<bool> CITO { get; set; }
        public string RujukanDariVendorID { get; set; }
        public Nullable<short> CUstomer_ID { get; set; }
        public Nullable<short> Supplier_ID { get; set; }
        public Nullable<bool> PAPSmear { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public Nullable<System.DateTime> TglBacaHasil { get; set; }
        public string CatatanHasil { get; set; }
        public Nullable<bool> PublishHasil { get; set; }
        public Nullable<short> UserIDPublish { get; set; }
        public Nullable<System.DateTime> TglPublish { get; set; }
        public string DokterPengirim { get; set; }
        public Nullable<int> WaktuID { get; set; }
        public string Memo { get; set; }
        public Nullable<bool> ILO { get; set; }
        public Nullable<bool> Plebitis { get; set; }
        public Nullable<System.DateTime> TglPenerimaan { get; set; }
        public Nullable<short> UserIDPenerimaan { get; set; }
        public string KeteranganPenerimaan { get; set; }
        public Nullable<bool> Repeater { get; set; }
        public string KodeGapah { get; set; }
        public string AlasanDirujukID { get; set; }
        public string JenisPenolong { get; set; }
        public Nullable<bool> VaksinDokter { get; set; }
        public Nullable<bool> FeeVaksinDokter { get; set; }
        public string OK_DokterDuranteID { get; set; }
        public Nullable<System.DateTime> TglKirim { get; set; }
        public Nullable<System.DateTime> TglTerima { get; set; }
        public Nullable<System.DateTime> TglInput { get; set; }
        public Nullable<System.DateTime> TglBatal { get; set; }
        public string AlasanBatal { get; set; }
        public Nullable<short> UserIDBatal { get; set; }
        public Nullable<bool> Dikunci { get; set; }
        public Nullable<int> AdaPengambilanHasil { get; set; }
        public string OnLoopID { get; set; }
        public string InstrumenId { get; set; }
        public string PenataID { get; set; }
        public string DokterAndrologiID { get; set; }
        public string DokterEmbriologiID { get; set; }
        public string RuangPreOp { get; set; }
        public string RuangRR { get; set; }
        public string ShyringPump { get; set; }
        public string SetAlat { get; set; }
        public Nullable<bool> ViDaStana { get; set; }
        public string noTelp { get; set; }
        public Nullable<bool> AdaObat { get; set; }
        public Nullable<bool> RealisasiAdaObat { get; set; }
        public Nullable<int> UserIDRealisasiAdaObat { get; set; }
        public string UserIDWeb { get; set; }

        //Model get pasien
        public string Tanggal_View { get; set; }
        public string TglReg { get; set; }
        public string Alamat { get; set; }
        public string Registrasi_NoReg { get; set; }
        public string Dokter_DokterID { get; set; }
        public string Registrasi_JenisKelamin { get; set; }
        public string NamaDokter { get; set; }
        public string Registrasi_JenisKerjasama { get; set; }
        public string Registrasi_CompanyName { get; set; }
        public string Registrasi_NoKartu { get; set; }
        public string Nama_asli { get; set; }
        public int Pulang { get; set; }
        public string NamaJasa { get; set; }
        public Nullable<decimal> HargaJasa { get; set; }
        public string HargaJasa_View { get; set; }
        public string Nilai_View { get; set; }
        public string NilaiInvoice_View { get; set; }
        public decimal TotalNilai { get; set; }


        public List<ManualPostingInsertDetailViewModel> detail { get; set; }
        public List<ManualPostingInsertDetailJasaViewModel> detailtarif { get; set; }
    }


    public class ManualPostingInsertDetailViewModel
    {
        public int id { get; set; }
        public string jasaid { get; set; }
        public string jasanama { get; set; }
        public string section { get; set; }
        public string noreg { get; set; }
        public decimal qty { get; set; }
        public decimal tarif { get; set; }
        public decimal diskon { get; set; }
        public string dokter { get; set; }
        public string namadokter { get; set; }
        public List<ManualPostingInsertDetailJasaViewModel> detail { get; set; }
    }

    public class ManualPostingInsertDetailJasaViewModel
    {
        public string komponenid { get; set; }
        public string komponennama { get; set; }
        public decimal harga { get; set; }
        public decimal diskon { get; set; }
        public decimal diskonnilai { get; set; }
        public decimal jumlah { get; set; }
    }
}