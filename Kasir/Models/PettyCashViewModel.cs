﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PettyCashViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        [DataType(DataType.Time)]
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string SectionName { get; set; }
        public string UserName_Username { get; set; }
        public byte Shift { get; set; }
        public string Shift_Name { get; set; }
        public decimal SaldoAwal { get; set; }
        public decimal Debet { get; set; }
        public string Debet_View { get; set; }
        public decimal Kredit { get; set; }
        public string Kredit_View { get; set; }
        [Required]
        public string Deskripsi { get; set; }
        public bool Batal { get; set; }
        public string akun_Name { get; set; }
        public short UserID { get; set; }
        public string SectionID { get; set; }
        [Required]
        public Nullable<int> Akun_ID_Tujuan { get; set; }
        public string UnitBisnisID { get; set; }
        public Nullable<bool> POsted { get; set; }
        [Required]
        public string Kepada { get; set; }
        public string UserIDWeb { get; set; }
        public string UsernameWeb { get; set; }
    }
}