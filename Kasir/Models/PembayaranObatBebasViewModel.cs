﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PembayaranObatBebasViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string NamaPasien { get; set; }
        public string TipePasien { get; set; }
        public decimal NilaiTransaksi { get; set; }
        public string NilaiTransaksi_View { get; set; }
        public bool Batal { get; set; }
        public bool Audit { get; set; }
        public string SectionID { get; set; }
    }
}