﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class DiscountViewModel
    {
        public string IDDiscount { get; set; }
        public string NamaDiscount { get; set; }
        public string IDDokter { get; set; }
        public string NamaDokter { get; set; }
        public string IDJasa { get; set; }
        public string Jasa { get; set; }
        public string KelasID { get; set; }
        public string Kelas { get; set; }
        public Nullable<double> NilaiDiscont { get; set; }
        public string NilaiDiscont_View { get; set; }
        public string TipeDiscount { get; set; }
        public string KomponenName { get; set; }
    }
}