﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class SplitBillInsertViewModel
    {
        public DateTime? Tanggal { get; set; }

        [Required]
        public string NoRegAsal { get; set; }
        public string TglKeluarAsal { get; set; }
        public string NRMAsal { get; set; }
        public string NamaPasienAsal { get; set; }
        [Required]
        public string NoRegTujuan { get; set; }
        public string NRMTujuan { get; set; }
        public string NamaPasienTujuan { get; set; }
        public string TglRegTujuan { get; set; }
        public ListDetail<SplitBillDetailViewModel> Detail_List { get; set; }
    }

    public partial class SplitBillDetailViewModel
    {
        public string SectionName { get; set; }
        public string NoReg { get; set; }
        public string NoBukti { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string Total_View { get; set; }
        public bool Check { get; set; }
    }
}