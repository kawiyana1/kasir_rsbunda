﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PembayaranOutStandingViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKerjasama { get; set; }
        public decimal NilaiPembayaran { get; set; }
        public string NilaiPembayaran_View { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<bool> Audit { get; set; }
        public string Jenis { get; set; }
        public string SectionID { get; set; }
    }
}