﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class ObatBebasViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string DokterID { get; set; }
        public string NamaDOkter { get; set; }
        public string TipePasien { get; set; }
        public string NamaPasien { get; set; }
        public Nullable<decimal> BiayaRacik { get; set; }
        public Nullable<double> Disc_Persen { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string Total_View { get; set; }
        public string SectionID { get; set; }
        public string DeskripsiObat { get; set; }
    }
}