﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PembayaranInsertViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public DateTime Tanggal { get; set; }
        [DataType(DataType.Time)]
        public DateTime Jam { get; set; }
        [Required]
        public string NoReg { get; set; }
        public string TglReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string KelasHak { get; set; }
        public string Shift { get; set; }
        public bool Audit { get; set; }
        public bool Batal { get; set; }
        public string TipePelayanan { get; set; }
        public string TipePelayananNama { get; set; }
        public string AlasanBatal { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string SectionPerawatanID { get; set; }
        public string SectionPerawatanName { get; set; }
        public string RJ { get; set; }
        public string KelasID { get; set; }
        public string JenisKerjasama { get; set; }
        public string NamaKelas { get; set; }
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }
        public string DokterID { get; set; }
        public string NamaDokter { get; set; }
        public Nullable<double> NilaiInvoice { get; set; }
        public string NilaiInvoice_View { get; set; }
        public Nullable<double> NilaiDiscount { get; set; }
        public string NilaiDiscount_View { get; set; }
        public Nullable<double> NilaiDiscTdkLangsung { get; set; }
        public string NilaiDiscTdkLangsung_View { get; set; }
        public Nullable<double> NilaiDeposit { get; set; }
        public string NilaiDeposit_View { get; set; }
        public Nullable<double> NilaiAddCharge { get; set; }
        public string NilaiAddCharge_View { get; set; }
        public string GrandTotal { get; set; }
        public string Pembayaran { get; set; }
        public string Sisa { get; set; }
        public string KategoriPlafon { get; set; }
        public bool KasusBaru { get; set; }
        public string NamaKasus { get; set; }
        public Nullable<decimal> NilaiPlafon { get; set; }
        public Nullable<decimal> KelebihanPlafonObat { get; set; }
        public Nullable<decimal> KelebihanPlafonKelas { get; set; }
        public Nullable<decimal> KelebihanPlafonKasus { get; set; }
        public Nullable<decimal> KelebihanPlafonRI { get; set; }
        public Nullable<decimal> NilaiSudahTerpakai { get; set; }
        public bool InvoiceGabung { get; set; }
        public string InvoiceGabungNoReg { get; set; }
        public Nullable<decimal> NilaiInvoiceGabung { get; set; }
        public string NilaiInvoiceGabung_View { get; set; }
        public string NilaiInvoiceGabungSemua{ get; set; }
        public string InvoiceGabungNoReg2 { get; set; }
        public Nullable<decimal> NilaiInvoiceGabung2 { get; set; }
        public string NilaiInvoiceGabung2_View { get; set; }
        public string InvoiceGabungNoReg3 { get; set; }
        public string NilaiInvoiceGabung3_View { get; set; }
        public Nullable<decimal> NilaiInvoiceGabung3 { get; set; }
        public string Diagnosa { get; set; }
        public Nullable<decimal> Tindakan { get; set; }
        public Nullable<decimal> PemeriksaanFisik { get; set; }
        public Nullable<decimal> SewaKamar { get; set; }
        public Nullable<decimal> Perawatan { get; set; }
        public Nullable<decimal> Obat { get; set; }
        public Nullable<double> AddCharge_Persen { get; set; }
        public string AddCharge_Persen_View { get; set; }
        public Nullable<decimal> AddCharge { get; set; }
        public string AddCharge_View { get; set; }
        public string IDBank { get; set; }
        public string NamaBank { get; set; }
        public Nullable<double> AddCharge_Persen2 { get; set; }
        public Nullable<decimal> AddCharge2 { get; set; }
        public string IDBank2 { get; set; }
        public Nullable<double> AddCharge_Persen3 { get; set; }
        public Nullable<decimal> AddCharge3 { get; set; }
        public string IDBank3 { get; set; }
        public bool OutStanding { get; set; }
        public bool Lost { get; set; }
        public bool Panjar { get; set; }
        public bool Lunas { get; set; }
        public Nullable<System.DateTime> TglLunas { get; set; }
        public bool RealisasiPanjar { get; set; }
        public string KodeCustomerPenjamin { get; set; }
        public string NamaCustomerPenjamin { get; set; }
        public string NoPenjaminan { get; set; }
        public string KeteranganPenjaminan { get; set; }
        public string KodeINACBG { get; set; }
        public string DeskripsiINACBG { get; set; }
        public Nullable<decimal> TarifINACBG { get; set; }
        public string TarifINACBG_View { get; set; }
        public Nullable<decimal> TarifBPJS { get; set; }
        public string TarifBPJS_View { get; set; }
        public Nullable<decimal> TambahanBPJS { get; set; }
        public string TambahanBPJS_View { get; set; }
        public Nullable<decimal> TarifBPJS_NaikKelas { get; set; }
        public string TarifBPJS_NaikKelas_View { get; set; }
        public Nullable<decimal> TarifINACBG_NaikKelas { get; set; }
        public string TarifINACBG_NaikKelas_View { get; set; }
        public string Selisih { get; set; }
        public string NamaPerusahaan { get; set; }
        public Nullable<decimal> NilaiPembayaranLOG { get; set; }
        public bool MunculkanCaraBayar { get; set; }

        public ListDetail<PembayaranInsertDetailViewModel> Detail_List { get; set; }
        public ListDetail<DiscountInsertViewModel> Discount_Detail_List { get; set; }
        public ListDetail<DiagnosaViewModel> Diagnosa_Detail_List { get; set; }
        public ListDetail<MemoViewModel> Memo_Detail_List { get; set; }
    }

    public class PembayaranInsertDetailViewModel
    {
        public string NoBukti { get; set; }
        public int IDBayar { get; set; }
        public string NamaBayar { get; set; }
        public decimal NilaiBayar { get; set; }
        public string NilaiBayar_View { get; set; }
    }
}