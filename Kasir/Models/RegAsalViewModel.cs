﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class RegAsalViewModel
    {
        public System.DateTime tglKeluar { get; set; }
        public string tglKeluar_View { get; set; }
        public string NoBukti { get; set; }
        public string NRM { get; set; }
        public string NoReg { get; set; }
        public string NamaPasien { get; set; }
    }
}