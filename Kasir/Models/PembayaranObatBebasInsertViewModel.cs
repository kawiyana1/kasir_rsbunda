﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PembayaranObatBebasInsertViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public DateTime Tanggal { get; set; }
        [DataType(DataType.Time)]
        public DateTime Jam { get; set; }
        [Required]
        public string NoBuktiFarmasi { get; set; }
        public string TglFarmasi { get; set; }
        public string NamaPasien { get; set; }
        public string TipePasien { get; set; }
        public string DetailObat { get; set; }
        public decimal NilaiTransaksi { get; set; }
        public string NilaiTransaksi_View { get; set; }
        public Nullable<decimal> NilaiPembayaranTunai { get; set; }
        public string NilaiPembayaranTunai_View { get; set; }
        public Nullable<decimal> NilaiPembayaranCC { get; set; }
        public string NilaiPembayaranCC_View { get; set; }
        public string IDBank { get; set; }
        public string NamaBank { get; set; }
        public Nullable<decimal> NilaiPembayaranKredit { get; set; }
        public string NilaiPembayaranKredit_View { get; set; }
        public Nullable<decimal> NilaiPembayaranBebanRS { get; set; }
        public string NilaiPembayaranBebanRS_View { get; set; }
        public Nullable<decimal> NilaiPembayaranTunaiBPD { get; set; }
        public string NilaiPembayaranTunaiBPD_View { get; set; }
        public string Total { get; set; }
        public string SectionID { get; set; }
        public bool Batal { get; set; }
        public bool Audit { get; set; }
        public Nullable<short> UserID { get; set; }
        public string UserIDWeb { get; set; }
    }
}