﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class INACBGViewModel
    {
        public string Kode { get; set; }
        public string Deskripsi { get; set; }
        public decimal Tarif { get; set; }
        public string TipePelayanan { get; set; }
        public string TarifBPJS { get; set; }
        public Nullable<decimal> TarifKelas2 { get; set; }
        public Nullable<decimal> tarifKelas3 { get; set; }
        public Nullable<decimal> Tarif_Lama { get; set; }
        public Nullable<decimal> TarifKelas2_Lama { get; set; }
        public Nullable<decimal> tarifKelas3_Lama { get; set; }
        public Nullable<int> BerlakuRumus { get; set; }
    }
}