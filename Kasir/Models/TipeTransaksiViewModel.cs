﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class TipeTransaksiViewModel
    {
        public string NamaType { get; set; }
        public int akun_id { get; set; }
        public string Tipe { get; set; }
    }
}