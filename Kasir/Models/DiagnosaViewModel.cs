﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class DiagnosaViewModel
    {
        public int Nomor { get; set; }
        public string NoReg { get; set; }
        public string SectionName { get; set; }
        public string KodeICD { get; set; }
        public string Descriptions { get; set; }
        public string TipeDiagnosa { get; set; }
    }
}