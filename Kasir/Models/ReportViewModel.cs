﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class ReportViewModel
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime Start { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public DateTime End { get; set; }
        public string JenisReport { get; set; }
        public short LokasiID { get; set; }
        public short LokasiIDAll { get; set; }
        public string SPName { get; set; }
        public string DokterID { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string Nama { get; set; }
        public string Kamar { get; set; }
        public string KelompokJenis { get; set; }
        public string FormatReport { get; set; }
    }
}