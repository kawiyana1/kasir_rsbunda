﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PengembalianDepositViewModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.DateTime Jam { get; set; }
        public string Keterangan { get; set; }
        public decimal Nilai { get; set; }
        public string Akun_Akun_No { get; set; }
        public string Akun_Akun_Name { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<bool> Posting { get; set; }
    }
}