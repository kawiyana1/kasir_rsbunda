﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class PembayaranLostInsertViewModel
    {
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        [DataType(DataType.Time)]
        public System.DateTime Jam { get; set; }
        public string NoInvoice { get; set; }
        public string NoReg { get; set; }
        public string TanggalInvoice { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string TipePasien { get; set; }
        public decimal NilaiAwal { get; set; }
        public string NilaiAwal_View { get; set; }
        public decimal NilaiAkumulaiPembayaran { get; set; }
        public string NilaiAkumulaiPembayaran_View { get; set; }
        public decimal NilaiPembayaran { get; set; }
        public string NilaiPembayaran_View { get; set; }
        public bool Tunai { get; set; }
        public bool KartuKredit { get; set; }
        public string IDBank { get; set; }
        public string NamaBank { get; set; }
        public Nullable<double> AddCharge_Persen { get; set; }
        public string AddCharge_Persen_View { get; set; }
        public Nullable<decimal> AddCharge { get; set; }
        public string AddCharge_View { get; set; }
        public string NoKartu { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<short> UserID { get; set; }
        public string UserIDWeb { get; set; }
        public Nullable<bool> Audit { get; set; }
        public string Shift { get; set; }
        public string Sisa { get; set; }
        public string SectionID { get; set; }
        public string Jenis { get; set; }
    }
}