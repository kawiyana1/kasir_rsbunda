﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class SplitBillViewModel
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NoReg_Asal { get; set; }
        public string NRMPasienAsal { get; set; }
        public string NamaPasienAsal { get; set; }
        public string NoReg_Tujuan { get; set; }
        public string NRMPasienTujuan { get; set; }
        public string NamaPasienTujuan { get; set; }
        public string UserIDWeb { get; set; }
    }
}