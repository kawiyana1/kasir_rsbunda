﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kasir.Models
{
    public class DiscountInsertViewModel
    {
        public string NoBukti { get; set; }
        public string IDDiscount { get; set; }
        public string NamaDiscount { get; set; }
        public string DokterID { get; set; }
        public string NamaDokter { get; set; }
        public double Persen { get; set; }
        public decimal NilaiDiscount { get; set; }
        public string NilaiDiscount_View { get; set; }
        public string NilaiDiscountOriginal { get; set; }
        public string Keterangan { get; set; }
        public string NoReg { get; set; }
        public string NoRegAnak { get; set; }
        public string JasaID { get; set; }
        public string NamaJasa { get; set; }
        public string KelasID { get; set; }
        public string NamaKelas { get; set; }
        public string KeteranganJasa { get; set; }
        public Nullable<bool> DiscountWithException { get; set; }
        public Nullable<double> PersenHiden { get; set; }
        public string NoRegAnak3 { get; set; }
        public string NoRegAnak2 { get; set; }
    }
}