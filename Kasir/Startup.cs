﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kasir.Startup))]
namespace Kasir
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
