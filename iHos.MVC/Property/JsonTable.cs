﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Property
{
    public class TableList
    {
        public TableList(object resultss)
        {
            var typ = resultss.GetType();
            Table = typ.GetProperty("Data").GetValue(resultss);
            Pagger = new Pagger((int)typ.GetProperty("Page").GetValue(resultss), 
                (int)typ.GetProperty("TotalCount").GetValue(resultss));
            IsSuccess = true;
        }

        public bool IsSuccess { get; set; } = false;
        public Pagger Pagger { get; set; }
        public object Table { get; set; }
        //public object ValueEvent { get; set; }
    }

    public class Pagger
    {
        public int PageIndex { get; }
        public int TotalRowCount { get; }
        public Pagger(int pageIndex, int totalRowCount)
        {
            this.PageIndex = pageIndex;
            this.TotalRowCount = totalRowCount;
        }
    }
}
