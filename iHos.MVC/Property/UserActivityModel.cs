﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace iHos.MVC.Property
{
    public class UserActivityModel
    {
        public UserActivityModel(HttpRequestBase RequestBase, string Id_User)
        {
            this.RequestBase = RequestBase;
            this.Id_User = Id_User;
        }

        public HttpRequestBase RequestBase { get; set; }
        public string Id_User { get; set; }

        public string IP { get { return RequestBase.UserHostName; } }
        public DateTime InputDate { get { return DateTime.Now; } }
        public string OS
        {
            get
            {
                var x = RequestBase.UserAgent;
                var x1 = x.IndexOf('(') + 1;
                var x2 = x.IndexOf(')') - x1;
                var userAgent = x.Substring(x1, x2);
                return userAgent;
            }
        }
        public string Browser { get { return RequestBase.Browser.Browser + " " + RequestBase.Browser.Version; } }
        public string Activity { get; set; }
    }
}
