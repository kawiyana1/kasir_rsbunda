﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Master
{
    public class MasterSqlCon : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public string ConString { get; set; }

        public string SP_Name { get; set; }

        public string TypeTable_Name { get; set; }

        public DataTable Table { get; set; }

        public ResultSS Execute()
        {
            ResultSS result;
            using (var conn = new SqlConnection(ConString))
            {
                conn.Open();
                int rst;
                var transaction = conn.BeginTransaction();
                SqlCommand cmd = new SqlCommand(SP_Name, conn)
                {
                    CommandType = CommandType.StoredProcedure,
                    Transaction = transaction
                };

                try
                {
                    SqlParameter param = new SqlParameter()
                    {
                        ParameterName = TypeTable_Name,
                        Value = Table
                    };
                    cmd.Parameters.Add(param);
                    rst = cmd.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch (SqlException ex)
                {
                    try
                    {
                        transaction.Rollback();
                        return new ResultSS(ex);
                    }
                    catch (Exception ex2) { return new ResultSS(ex2); }
                }
                catch (Exception ex)
                {
                    try
                    {
                        transaction.Rollback();
                        return new ResultSS(ex);
                    }
                    catch (Exception ex2) { return new ResultSS(ex2); }
                }
                conn.Close();
                result = new ResultSS(rst);
            };
            return result;
        }
    }
}
