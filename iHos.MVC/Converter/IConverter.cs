﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;

namespace iHos.MVC.Converter
{
    public static class IConverter
    {
        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            var dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                var colname = ((ColumnAttribute)info.GetCustomAttributes(typeof(ColumnAttribute), true).First()).Name;
                dataTable.Columns.Add(colname);
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

        public static decimal? ToDecimal(this string value, bool default0 = true)
        {
            if (value == null && default0 == false) return null;
            if (value == null) return 0;
            var newValue = value.Replace("Rp. ", "").Replace(",", "").Replace(".", ",");
            if (newValue == "-") return 0;
            return decimal.Parse(newValue == "" ? "0" : newValue);
        }

        public static decimal ToDecimal(this string value)
        {
            if (value == null) return 0;

            // US
            var newValue = value.Replace("Rp. ", "").Replace(",", "");
            // ID
            //var newValue = value.Replace("Rp. ", "").Replace(",", "").Replace(".", ",");

            if (newValue == "-") return 0;
            return decimal.Parse(newValue == "" ? "0" : newValue);
        }

        public static string ToMoney(this decimal value)
        {
            //return string.Format("{0:N}", value);
            var value_string = value.ToString();

            // US
            //var coma_split = value_string.Split('.');
            // ID
            var coma_split = value_string.Split('.');

            int coma_length = 0;
            if (coma_split.Length > 1)
                coma_length = coma_split[1].Replace("0", "").Length;
            value_string = Math.Round(value, coma_length).ToString();
            //value_string = value.ToString();

            decimal saveComa = 0;
            if (coma_split.Length > 1)
            {
                value_string = coma_split[0];
                saveComa = decimal.Parse("0," + coma_split[1]);
            }
            var newValue = "";
            var index = 1;
            for (var j = value_string.Length - 1; j >= 0; j--)
            {
                newValue = value_string[j] + newValue;
                if (index % 3 == 0 && j != 0)
                    newValue = "," + newValue;
                index++;
            }
            if (saveComa > 0) newValue += "." + (saveComa.ToString());
            return newValue == "" ? "0" : newValue;
        }

        public static DateTime? ToDateTime(this string value, bool defaultnow = true)
        {
            try
            {
                if (string.IsNullOrEmpty(value)) return (DateTime?)null;
                var _split = value.Split(' ');
                if (_split.Length > 1)
                {
                    var splitDate = _split[0].Split('/');
                    var splitTime = _split[1].Split(':');
                    if (splitDate.Length < 3 || splitTime.Length < 2)
                    {
                        if (defaultnow) return DateTime.Now;
                        else return null;
                    }
                    return new DateTime(
                        int.Parse(splitDate[2]),
                        int.Parse(splitDate[1]),
                        int.Parse(splitDate[0]),
                        int.Parse(splitTime[0]),
                        int.Parse(splitTime[1]),
                        int.Parse(splitTime[2]));
                }
                else
                {
                    var split = value.Split('/');
                    if (split.Length < 3)
                    {
                        if (defaultnow) return DateTime.Now;
                        else return null;
                    }
                    return new DateTime(int.Parse(split[2]), int.Parse(split[1]), int.Parse(split[0]));
                }
            }
            catch
            {
                if (defaultnow)
                    return DateTime.Now;
                else
                    throw new Exception("Format tanggal salah");
            }
        }

        public static string ToServerDateTimeFormat(this string value, bool defaultnow = true, bool time = false)
        {
            if (string.IsNullOrEmpty(value)) return "";
            var split = value.Split('/');
            if (split.Length < 2) return split[0];
            if (split.Length < 3) return split[1] + '-' + split[0];
            var val = value.ToDateTime(defaultnow);
            if (val == null)
            {
                if (defaultnow)
                    val = DateTime.Now;
                else
                    return "";
            }
            return val.Value.ToString(time ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd");
        }

        public static string ToStringDateTime(this DateTime value, bool time = false)
        {
            return value.ToString(time ? "dd/MM/yyyy HH:mm:ss" : "dd/MM/yyyy");
        }

        public static TimeSpan? ToTimeSpan(this string value, bool defaultnow = true)
        {
            try
            {
                var split = value.Split(':');
                if (split.Length < 2)
                {
                    if (defaultnow) return DateTime.Now.TimeOfDay;
                    else return null;
                }
                return new TimeSpan(int.Parse(split[0]), int.Parse(split[1]), split.Length > 2 ? int.Parse(split[2]) : 0);
            }
            catch
            {
                if (defaultnow)
                    return DateTime.Now.TimeOfDay;
                else
                    throw new Exception("Format tanggal salah");
            }
        }

        public static string ToStringTimeSpan(this TimeSpan value, bool seconds = false)
        {
            return (value.Hours < 10 ? "0" : "") + value.Hours + ":" +
                (value.Minutes < 10 ? "0" : "") + value.Minutes +
                (seconds ? (value.Seconds < 10 ? "0" : "") + value.Seconds : "");
        }

        public static Dictionary<string, object> JoinClass(object object1, object object2)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            // object 1
            if (object1 != null)
            {
                if (object1.GetType() == typeof(Dictionary<string, object>))
                    dictionary = ((Dictionary<string, object>)object1);
                else
                {
                    var object1_properties = object1.GetType().GetProperties();
                    for (int i = 0; i < object1_properties.Length; i++)
                        dictionary.Add(object1_properties[i].Name, object1_properties[i].GetValue(object1));
                }
            }
            // object 2
            var object2_properties = object2.GetType().GetProperties();
            for (int i = 0; i < object2_properties.Length; i++)
            {
                dictionary.Add(object2_properties[i].Name, object2_properties[i].GetValue(object2));
            }

            return dictionary;
        }

        public static T Cast<T>(this object myobj, string[] without = null)
        {
            Type objectType = myobj.GetType();
            Type target = typeof(T);
            var x = Activator.CreateInstance(target, false);
            var z = from source in objectType.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            var d = from source in target.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            List<MemberInfo> members = d.Where(memberInfo => d.Select(c => c.Name)
               .ToList().Contains(memberInfo.Name)).ToList();
            PropertyInfo propertyInfo;
            object value;
            foreach (var memberInfo in members)
            {
                try
                {
                    if (myobj.GetType().GetProperty(memberInfo.Name) == null) continue;
                    if (without != null)
                    {
                        if (without.Where(xyz => xyz == memberInfo.Name).Count() > 0) continue;
                    }
                    propertyInfo = typeof(T).GetProperty(memberInfo.Name);
                    value = myobj.GetType().GetProperty(memberInfo.Name).GetValue(myobj, null);
                    if ((
                            myobj.GetType().GetProperty(memberInfo.Name).PropertyType == typeof(TimeSpan?) ||
                            myobj.GetType().GetProperty(memberInfo.Name).PropertyType == typeof(TimeSpan)
                        )
                        &&
                        (
                            typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(DateTime?) ||
                            typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(DateTime)
                        ))
                    {
                        if (myobj.GetType().GetProperty(memberInfo.Name).PropertyType == typeof(TimeSpan?))
                            propertyInfo.SetValue(x, DateTime.Today + (TimeSpan?)value, null);
                        else
                            propertyInfo.SetValue(x, DateTime.Today + (TimeSpan)value, null);
                    }
                    else if ((
                            myobj.GetType().GetProperty(memberInfo.Name).PropertyType == typeof(DateTime?) ||
                            myobj.GetType().GetProperty(memberInfo.Name).PropertyType == typeof(DateTime)
                        )
                        &&
                        (
                            typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(TimeSpan?) ||
                            typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(TimeSpan)
                        ))
                    {
                        if (myobj.GetType().GetProperty(memberInfo.Name).PropertyType == typeof(DateTime?))
                        {
                            var v = (DateTime?)value;
                            propertyInfo.SetValue(x, v == null ? (TimeSpan?)null : v.Value.TimeOfDay, null);
                        }
                        else
                            propertyInfo.SetValue(x, ((DateTime)value).TimeOfDay, null);
                    }
                    else if ((
                        myobj.GetType().GetProperty(memberInfo.Name).PropertyType == typeof(byte[])
                    )
                    &&
                    (
                        typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(string)
                    ))
                    {
                        propertyInfo.SetValue(x, Convert.ToBase64String((byte[])value), null);
                    }
                    else if ((
                        myobj.GetType().GetProperty(memberInfo.Name).PropertyType == typeof(string)
                    )
                    &&
                    (
                        typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(byte[])
                    ))
                    {
                        propertyInfo.SetValue(x, Convert.FromBase64String((string)value), null);
                    }
                    else
                        propertyInfo.SetValue(x, value, null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return (T)x;
        }

        public static T CastDT<T>(this DataTable myobj, int rowIndex = 0)
        {
            if (myobj.Rows.Count < 1) return default(T);
            Type objectType = myobj.GetType();
            Type target = typeof(T);
            var x = Activator.CreateInstance(target, false);
            var z = from source in objectType.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            var d = from source in target.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            List<MemberInfo> members = d.Where(memberInfo => d.Select(c => c.Name)
               .ToList().Contains(memberInfo.Name)).ToList();
            PropertyInfo propertyInfo;
            object value;
            foreach (var memberInfo in members)
            {
                try
                {
                    var clm = myobj.Columns[memberInfo.Name];
                    if (clm == null) continue;
                    propertyInfo = typeof(T).GetProperty(memberInfo.Name);
                    value = myobj.Rows[rowIndex].ItemArray[clm.Ordinal];
                    if ((
                            clm.DataType == typeof(TimeSpan?) ||
                            clm.DataType == typeof(TimeSpan)
                        )
                        &&
                        (
                            typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(DateTime?) ||
                            typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(DateTime)
                        ))
                    {
                        if (clm.DataType == typeof(TimeSpan?))
                            propertyInfo.SetValue(x, DateTime.Today + (TimeSpan?)value, null);
                        else
                            propertyInfo.SetValue(x, DateTime.Today + (TimeSpan)value, null);
                    }
                    else if ((
                            clm.DataType == typeof(DateTime?) ||
                            clm.DataType == typeof(DateTime)
                        )
                        &&
                        (
                            typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(TimeSpan?) ||
                            typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(TimeSpan)
                        ))
                    {
                        if (clm.DataType == typeof(DateTime?))
                        {
                            var v = (DateTime?)value;
                            propertyInfo.SetValue(x, v == null ? (TimeSpan?)null : v.Value.TimeOfDay, null);
                        }
                        else
                            propertyInfo.SetValue(x, ((DateTime)value).TimeOfDay, null);
                    }
                    else if ((
                        clm.DataType == typeof(byte[])
                    )
                    &&
                    (
                        typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(string)
                    ))
                    {
                        propertyInfo.SetValue(x, Convert.ToBase64String((byte[])value), null);
                    }
                    else if ((
                        clm.DataType == typeof(string)
                    )
                    &&
                    (
                        typeof(T).GetProperty(memberInfo.Name).PropertyType == typeof(byte[])
                    ))
                    {
                        propertyInfo.SetValue(x, Convert.FromBase64String((string)value), null);
                    }
                    else
                        propertyInfo.SetValue(x, value, null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return (T)x;
        }

        public static bool PropertiesEqual<T>(this T self, T to, params string[] ignore) where T : class
        {
            if (self != null && to != null)
            {
                Type type = typeof(T);
                List<string> ignoreList = new List<string>(ignore);
                foreach (System.Reflection.PropertyInfo pi in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    if (!ignoreList.Contains(pi.Name))
                    {
                        object selfValue = type.GetProperty(pi.Name).GetValue(self, null);
                        object toValue = type.GetProperty(pi.Name).GetValue(to, null);

                        if (selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            return self == to;
        }

        public static ResultStatus JoinResutStatus(params ResultStatus[] resultstatus)
        {
            var isSuccess = true;
            var count = 0;
            var message = "";

            foreach (var model in resultstatus)
            {
                isSuccess = isSuccess && model.IsSuccess;
                count += model.Count;
                message += "|" + model.Message;
            }

            return new ResultStatus()
            {
                IsSuccess = isSuccess,
                Count = count,
                Message = message
            };
        }
    }
}